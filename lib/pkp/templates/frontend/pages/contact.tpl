{**
 * templates/frontend/pages/contact.tpl
 *
 * Copyright (c) 2014-2018 Simon Fraser University
 * Copyright (c) 2003-2018 John Willinsky
 * Distributed under the GNU GPL v2. For full terms see the file docs/COPYING.
 *
 * @brief Display the page to view the press's contact details.
 *
 * @uses $currentContext Journal|Press The current journal or press
 * @uses $mailingAddress string Mailing address for the journal/press
 * @uses $contactName string Primary contact name
 * @uses $contactTitle string Primary contact title
 * @uses $contactAffiliation string Primary contact affiliation
 * @uses $contactPhone string Primary contact phone number
 * @uses $contactEmail string Primary contact email address
 * @uses $supportName string Support contact name
 * @uses $supportPhone string Support contact phone number
 * @uses $supportEmail string Support contact email address
 *}
{include file="frontend/components/header.tpl" pageTitle="about.contact"}

<div class="page page_contact">
	{include file="frontend/components/breadcrumbs.tpl" currentTitleKey="about.contact"}
	{include file="frontend/components/editLink.tpl" page="management" op="settings" path="context" anchor="contact" sectionTitleKey="about.contact"}

	{* Contact section *}
	<div class="contact_section">

		{if $mailingAddress}
			<div class="address">
				{$mailingAddress|nl2br|strip_unsafe_html}
			</div>
		{/if}
		
		<div class="site-content container">
		<h1 class="page_title">Kontak</h1>
		<div class="table-responsive">
			<p><iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.317948877473!2d107.6434102142774!3d-6.971765494963383!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e9ca737f2fef%3A0xf1b3e1bbc36d01e3!2sSTAIPI+-+Sekolah+Tinggi+Agama+Islam+Persis!5e0!3m2!1sid!2sid!4v1551689794829" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>&nbsp;</p>
		<p><strong>Alamat Redaksi :</strong>&nbsp;<br>Jl. Ciganitri No.2, Cipagalo, Bojongsoang, Bandung, Jawa Barat 40287<br><strong>Pengelola :</strong>&nbsp;Haris Budiman</p>
		</div>

		{* Primary contact *}
		{if $contactTitle || $contactName || $contactAffiliation || $contactPhone || $contactEmail}
			<div class="contact primary">
				<h3>
					{translate key="about.contact.principalContact"}
				</h3>

				{if $contactName}
				<div class="name">
					{$contactName|escape}
				</div>
				{/if}

				{if $contactTitle}
				<div class="title">
					{$contactTitle|escape}
				</div>
				{/if}

				{if $contactAffiliation}
				<div class="affiliation">
					{$contactAffiliation|strip_unsafe_html}
				</div>
				{/if}

				{if $contactPhone}
				<div class="phone">
					<span class="label">
						{translate key="about.contact.phone"}
					</span>
					<span class="value">
						{$contactPhone|escape}
					</span>
				</div>
				{/if}

				{if $contactEmail}
				<div class="email">
					<a href="mailto:{$contactEmail|escape}">
						{$contactEmail|escape}
					</a>
				</div>
				{/if}
			</div>
		{/if}

		{* Technical contact *}
		{if $supportName || $supportPhone || $supportEmail}
			<div class="contact support">
				<h3>
					{translate key="about.contact.supportContact"}
				</h3>

				{if $supportName}
				<div class="name">
					{$supportName|escape}
				</div>
				{/if}

				{if $supportPhone}
				<div class="phone">
					<span class="label">
						{translate key="about.contact.phone"}
					</span>
					<span class="value">
						{$supportPhone|escape}
					</span>
				</div>
				{/if}

				{if $supportEmail}
				<div class="email">
					<a href="mailto:{$supportEmail|escape}">
						{$supportEmail|escape}
					</a>
				</div>
				{/if}
			</div>
		{/if}
	</div>

</div><!-- .page -->

{include file="frontend/components/footer.tpl"}
