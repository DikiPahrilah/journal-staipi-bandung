-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2019 at 02:42 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `journal`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_keys`
--

CREATE TABLE `access_keys` (
  `access_key_id` bigint(20) NOT NULL,
  `context` varchar(40) NOT NULL,
  `key_hash` varchar(40) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `expiry_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `announcement_id` bigint(20) NOT NULL,
  `assoc_type` smallint(6) DEFAULT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `date_expire` datetime DEFAULT NULL,
  `date_posted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`announcement_id`, `assoc_type`, `assoc_id`, `type_id`, `date_expire`, `date_posted`) VALUES
(1, 256, 1, 1, '2019-04-27 00:00:00', '2019-03-02 20:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_settings`
--

CREATE TABLE `announcement_settings` (
  `announcement_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement_settings`
--

INSERT INTO `announcement_settings` (`announcement_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'description', '', 'string'),
(1, 'en_US', 'descriptionShort', '<p>We currently open new submission for <strong>Tested </strong>Vol.4, Num 1, 2019.</p>', 'string'),
(1, 'en_US', 'title', 'Call for Paper, Tested Vol.4, Num 1, 2019', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_types`
--

CREATE TABLE `announcement_types` (
  `type_id` bigint(20) NOT NULL,
  `assoc_type` smallint(6) DEFAULT NULL,
  `assoc_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement_types`
--

INSERT INTO `announcement_types` (`type_id`, `assoc_type`, `assoc_id`) VALUES
(1, 256, 1);

-- --------------------------------------------------------

--
-- Table structure for table `announcement_type_settings`
--

CREATE TABLE `announcement_type_settings` (
  `type_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `announcement_type_settings`
--

INSERT INTO `announcement_type_settings` (`type_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'name', 'Call for Paper', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `authors`
--

CREATE TABLE `authors` (
  `author_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `primary_contact` tinyint(4) NOT NULL DEFAULT '0',
  `seq` double NOT NULL DEFAULT '0',
  `first_name` varchar(40) NOT NULL,
  `middle_name` varchar(40) DEFAULT NULL,
  `last_name` varchar(90) NOT NULL,
  `suffix` varchar(40) DEFAULT NULL,
  `country` varchar(90) DEFAULT NULL,
  `email` varchar(90) NOT NULL,
  `url` varchar(2047) DEFAULT NULL,
  `user_group_id` bigint(20) DEFAULT NULL,
  `include_in_browse` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `authors`
--

INSERT INTO `authors` (`author_id`, `submission_id`, `primary_contact`, `seq`, `first_name`, `middle_name`, `last_name`, `suffix`, `country`, `email`, `url`, `user_group_id`, `include_in_browse`) VALUES
(1, 1, 1, 1, 'Author', '', 'Author', '', 'ID', 'dikipahrilah29@gmail.com', NULL, 14, 1),
(2, 1, 0, 2, 'Diaz', 'Faizal', 'Malik', '', 'ID', 'diazfaizalmalik@gmail.com', '', 14, 1),
(3, 2, 1, 1, 'Author', '', 'Author', '', 'ID', 'dikipahrilah29@gmail.com', NULL, 14, 1),
(4, 2, 0, 2, 'Diaz', 'Faizal', 'Malik', '', 'ID', 'diazfaizalmalik@gmail.com', '', 14, 1);

-- --------------------------------------------------------

--
-- Table structure for table `author_settings`
--

CREATE TABLE `author_settings` (
  `author_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `author_settings`
--

INSERT INTO `author_settings` (`author_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'affiliation', 'Universitas STAIPI', 'string'),
(2, '', 'orcid', '', 'string'),
(2, 'en_US', 'affiliation', '', 'string'),
(2, 'en_US', 'biography', '', 'string'),
(3, 'en_US', 'affiliation', 'Universitas STAIPI', 'string'),
(4, '', 'orcid', '', 'string'),
(4, 'en_US', 'affiliation', '', 'string'),
(4, 'en_US', 'biography', '', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `auth_sources`
--

CREATE TABLE `auth_sources` (
  `auth_id` bigint(20) NOT NULL,
  `title` varchar(60) NOT NULL,
  `plugin` varchar(32) NOT NULL,
  `auth_default` tinyint(4) NOT NULL DEFAULT '0',
  `settings` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `citations`
--

CREATE TABLE `citations` (
  `citation_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL DEFAULT '0',
  `raw_citation` text,
  `seq` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `citation_settings`
--

CREATE TABLE `citation_settings` (
  `citation_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `completed_payments`
--

CREATE TABLE `completed_payments` (
  `completed_payment_id` bigint(20) NOT NULL,
  `timestamp` datetime NOT NULL,
  `payment_type` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `amount` double NOT NULL,
  `currency_code_alpha` varchar(3) DEFAULT NULL,
  `payment_method_plugin_name` varchar(80) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `controlled_vocabs`
--

CREATE TABLE `controlled_vocabs` (
  `controlled_vocab_id` bigint(20) NOT NULL,
  `symbolic` varchar(64) NOT NULL,
  `assoc_type` bigint(20) NOT NULL DEFAULT '0',
  `assoc_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `controlled_vocabs`
--

INSERT INTO `controlled_vocabs` (`controlled_vocab_id`, `symbolic`, `assoc_type`, `assoc_id`) VALUES
(8, 'interest', 0, 0),
(4, 'mods34-genre-marcgt', 0, 0),
(2, 'mods34-name-role-roleTerms-marcrelator', 0, 0),
(1, 'mods34-name-types', 0, 0),
(5, 'mods34-physicalDescription-form-marcform', 0, 0),
(3, 'mods34-typeOfResource', 0, 0),
(7, 'openurl10-book-genres', 0, 0),
(6, 'openurl10-journal-genres', 0, 0),
(15, 'submissionAgency', 1048585, 1),
(36, 'submissionAgency', 1048585, 2),
(14, 'submissionDiscipline', 1048585, 1),
(35, 'submissionDiscipline', 1048585, 2),
(13, 'submissionKeyword', 1048585, 1),
(34, 'submissionKeyword', 1048585, 2),
(16, 'submissionLanguage', 1048585, 1),
(37, 'submissionLanguage', 1048585, 2),
(12, 'submissionSubject', 1048585, 1),
(33, 'submissionSubject', 1048585, 2);

-- --------------------------------------------------------

--
-- Table structure for table `controlled_vocab_entries`
--

CREATE TABLE `controlled_vocab_entries` (
  `controlled_vocab_entry_id` bigint(20) NOT NULL,
  `controlled_vocab_id` bigint(20) NOT NULL,
  `seq` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `controlled_vocab_entries`
--

INSERT INTO `controlled_vocab_entries` (`controlled_vocab_entry_id`, `controlled_vocab_id`, `seq`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 1),
(5, 2, 2),
(6, 2, 3),
(7, 2, 4),
(8, 2, 5),
(9, 2, 6),
(10, 2, 7),
(11, 3, 1),
(12, 3, 2),
(13, 3, 3),
(14, 4, 1),
(15, 4, 2),
(16, 4, 3),
(17, 4, 4),
(18, 4, 5),
(19, 4, 6),
(20, 4, 7),
(21, 4, 8),
(22, 4, 9),
(23, 4, 10),
(24, 4, 11),
(25, 4, 12),
(26, 5, 1),
(27, 5, 2),
(28, 6, 1),
(29, 6, 2),
(30, 6, 3),
(31, 6, 4),
(32, 6, 5),
(33, 6, 6),
(34, 6, 7),
(35, 7, 1),
(36, 7, 2),
(37, 7, 3),
(38, 7, 4),
(39, 7, 5),
(40, 7, 6),
(41, 7, 7),
(42, 13, 1),
(43, 34, 1);

-- --------------------------------------------------------

--
-- Table structure for table `controlled_vocab_entry_settings`
--

CREATE TABLE `controlled_vocab_entry_settings` (
  `controlled_vocab_entry_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `controlled_vocab_entry_settings`
--

INSERT INTO `controlled_vocab_entry_settings` (`controlled_vocab_entry_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'name', 'personal', 'string'),
(2, '', 'name', 'corporate', 'string'),
(3, '', 'name', 'conference', 'string'),
(4, '', 'description', 'Author', 'string'),
(4, '', 'name', 'aut', 'string'),
(5, '', 'description', 'Contributor', 'string'),
(5, '', 'name', 'ctb', 'string'),
(6, '', 'description', 'Editor', 'string'),
(6, '', 'name', 'edt', 'string'),
(7, '', 'description', 'Illustrator', 'string'),
(7, '', 'name', 'ill', 'string'),
(8, '', 'description', 'Photographer', 'string'),
(8, '', 'name', 'pht', 'string'),
(9, '', 'description', 'Sponsor', 'string'),
(9, '', 'name', 'spn', 'string'),
(10, '', 'description', 'Translator', 'string'),
(10, '', 'name', 'trl', 'string'),
(11, '', 'name', 'multimedia', 'string'),
(12, '', 'name', 'still image', 'string'),
(13, '', 'name', 'text', 'string'),
(14, '', 'name', 'article', 'string'),
(15, '', 'name', 'book', 'string'),
(16, '', 'name', 'conference publication', 'string'),
(17, '', 'name', 'issue', 'string'),
(18, '', 'name', 'journal', 'string'),
(19, '', 'name', 'newspaper', 'string'),
(20, '', 'name', 'picture', 'string'),
(21, '', 'name', 'review', 'string'),
(22, '', 'name', 'periodical', 'string'),
(23, '', 'name', 'series', 'string'),
(24, '', 'name', 'thesis', 'string'),
(25, '', 'name', 'web site', 'string'),
(26, '', 'name', 'electronic', 'string'),
(27, '', 'name', 'print', 'string'),
(28, '', 'name', 'journal', 'string'),
(29, '', 'name', 'issue', 'string'),
(30, '', 'name', 'article', 'string'),
(31, '', 'name', 'proceeding', 'string'),
(32, '', 'name', 'conference', 'string'),
(33, '', 'name', 'preprint', 'string'),
(34, '', 'name', 'unknown', 'string'),
(35, '', 'name', 'book', 'string'),
(36, '', 'name', 'bookitem', 'string'),
(37, '', 'name', 'proceeding', 'string'),
(38, '', 'name', 'conference', 'string'),
(39, '', 'name', 'report', 'string'),
(40, '', 'name', 'document', 'string'),
(41, '', 'name', 'unknown', 'string'),
(42, 'en_US', 'submissionKeyword', 'Quick Count, SMS GATEWAY, Grobogan elections.', 'string'),
(43, 'en_US', 'submissionKeyword', 'Quick Count, SMS GATEWAY, Grobogan elections.', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `custom_issue_orders`
--

CREATE TABLE `custom_issue_orders` (
  `issue_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `seq` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_section_orders`
--

CREATE TABLE `custom_section_orders` (
  `issue_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `seq` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_object_tombstones`
--

CREATE TABLE `data_object_tombstones` (
  `tombstone_id` bigint(20) NOT NULL,
  `data_object_id` bigint(20) NOT NULL,
  `date_deleted` datetime NOT NULL,
  `set_spec` varchar(255) NOT NULL,
  `set_name` varchar(255) NOT NULL,
  `oai_identifier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_object_tombstone_oai_set_objects`
--

CREATE TABLE `data_object_tombstone_oai_set_objects` (
  `object_id` bigint(20) NOT NULL,
  `tombstone_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_object_tombstone_settings`
--

CREATE TABLE `data_object_tombstone_settings` (
  `tombstone_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `edit_decisions`
--

CREATE TABLE `edit_decisions` (
  `edit_decision_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `review_round_id` bigint(20) DEFAULT NULL,
  `stage_id` bigint(20) DEFAULT NULL,
  `round` tinyint(4) NOT NULL,
  `editor_id` bigint(20) NOT NULL,
  `decision` tinyint(4) NOT NULL,
  `date_decided` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edit_decisions`
--

INSERT INTO `edit_decisions` (`edit_decision_id`, `submission_id`, `review_round_id`, `stage_id`, `round`, `editor_id`, `decision`, `date_decided`) VALUES
(1, 2, 0, 1, 0, 5, 8, '2019-03-02 18:59:01'),
(2, 2, 1, 3, 1, 5, 3, '2019-03-02 19:20:54'),
(3, 2, 2, 3, 2, 5, 2, '2019-03-02 19:38:34'),
(4, 2, 2, 3, 2, 5, 1, '2019-03-02 19:49:08'),
(5, 2, 2, 3, 2, 5, 1, '2019-03-02 19:51:58'),
(6, 2, 0, 4, 0, 5, 7, '2019-03-02 19:54:37');

-- --------------------------------------------------------

--
-- Table structure for table `email_log`
--

CREATE TABLE `email_log` (
  `log_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `sender_id` bigint(20) NOT NULL,
  `date_sent` datetime NOT NULL,
  `ip_address` varchar(39) DEFAULT NULL,
  `event_type` bigint(20) DEFAULT NULL,
  `from_address` varchar(255) DEFAULT NULL,
  `recipients` text,
  `cc_recipients` text,
  `bcc_recipients` text,
  `subject` varchar(255) DEFAULT NULL,
  `body` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_log_users`
--

CREATE TABLE `email_log_users` (
  `email_log_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `email_id` bigint(20) NOT NULL,
  `email_key` varchar(64) NOT NULL,
  `assoc_type` bigint(20) DEFAULT '0',
  `assoc_id` bigint(20) DEFAULT '0',
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_data`
--

CREATE TABLE `email_templates_data` (
  `email_key` varchar(64) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT 'en_US',
  `assoc_type` bigint(20) DEFAULT '0',
  `assoc_id` bigint(20) DEFAULT '0',
  `subject` varchar(120) NOT NULL,
  `body` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_default`
--

CREATE TABLE `email_templates_default` (
  `email_id` bigint(20) NOT NULL,
  `email_key` varchar(64) NOT NULL,
  `can_disable` tinyint(4) NOT NULL DEFAULT '1',
  `can_edit` tinyint(4) NOT NULL DEFAULT '1',
  `from_role_id` bigint(20) DEFAULT NULL,
  `to_role_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates_default`
--

INSERT INTO `email_templates_default` (`email_id`, `email_key`, `can_disable`, `can_edit`, `from_role_id`, `to_role_id`) VALUES
(1, 'NOTIFICATION', 0, 1, NULL, NULL),
(2, 'NOTIFICATION_CENTER_DEFAULT', 0, 1, NULL, NULL),
(3, 'PASSWORD_RESET_CONFIRM', 0, 1, NULL, NULL),
(4, 'PASSWORD_RESET', 0, 1, NULL, NULL),
(5, 'USER_REGISTER', 0, 1, NULL, NULL),
(6, 'USER_VALIDATE', 0, 1, NULL, NULL),
(7, 'REVIEWER_REGISTER', 0, 1, NULL, NULL),
(8, 'PUBLISH_NOTIFY', 0, 1, NULL, NULL),
(9, 'LOCKSS_EXISTING_ARCHIVE', 0, 1, NULL, NULL),
(10, 'LOCKSS_NEW_ARCHIVE', 0, 1, NULL, NULL),
(11, 'SUBMISSION_ACK', 1, 1, NULL, 65536),
(12, 'SUBMISSION_ACK_NOT_USER', 1, 1, NULL, 65536),
(13, 'EDITOR_ASSIGN', 1, 1, 256, 512),
(14, 'REVIEW_CANCEL', 1, 1, 512, 4096),
(15, 'REVIEW_REQUEST', 1, 1, 512, 4096),
(16, 'REVIEW_REQUEST_SUBSEQUENT', 1, 1, 512, 4096),
(17, 'REVIEW_REQUEST_ONECLICK', 1, 1, 512, 4096),
(18, 'REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 1, 1, 512, 4096),
(19, 'REVIEW_REQUEST_ATTACHED', 0, 1, 512, 4096),
(20, 'REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 0, 1, 512, 4096),
(21, 'REVIEW_REQUEST_REMIND_AUTO', 0, 1, NULL, 4096),
(22, 'REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 0, 1, NULL, 4096),
(23, 'REVIEW_CONFIRM', 1, 1, 4096, 512),
(24, 'REVIEW_DECLINE', 1, 1, 4096, 512),
(25, 'REVIEW_ACK', 1, 1, 512, 4096),
(26, 'REVIEW_REMIND', 0, 1, 512, 4096),
(27, 'REVIEW_REMIND_AUTO', 0, 1, NULL, 4096),
(28, 'REVIEW_REMIND_ONECLICK', 0, 1, 512, 4096),
(29, 'REVIEW_REMIND_AUTO_ONECLICK', 0, 1, NULL, 4096),
(30, 'EDITOR_DECISION_ACCEPT', 0, 1, 512, 65536),
(31, 'EDITOR_DECISION_SEND_TO_EXTERNAL', 0, 1, 17, 65536),
(32, 'EDITOR_DECISION_SEND_TO_PRODUCTION', 0, 1, 17, 65536),
(33, 'EDITOR_DECISION_REVISIONS', 0, 1, 512, 65536),
(34, 'EDITOR_DECISION_RESUBMIT', 0, 1, 512, 65536),
(35, 'EDITOR_DECISION_DECLINE', 0, 1, 512, 65536),
(36, 'EDITOR_DECISION_INITIAL_DECLINE', 0, 1, 512, 65536),
(37, 'EDITOR_RECOMMENDATION', 0, 1, 17, 16),
(38, 'COPYEDIT_REQUEST', 1, 1, 512, 8192),
(39, 'LAYOUT_REQUEST', 1, 1, 512, 768),
(40, 'LAYOUT_COMPLETE', 1, 1, 768, 512),
(41, 'EMAIL_LINK', 0, 1, 1048576, NULL),
(42, 'SUBSCRIPTION_NOTIFY', 0, 1, NULL, 1048576),
(43, 'OPEN_ACCESS_NOTIFY', 0, 1, NULL, 1048576),
(44, 'SUBSCRIPTION_BEFORE_EXPIRY', 0, 1, NULL, 1048576),
(45, 'SUBSCRIPTION_AFTER_EXPIRY', 0, 1, NULL, 1048576),
(46, 'SUBSCRIPTION_AFTER_EXPIRY_LAST', 0, 1, NULL, 1048576),
(47, 'SUBSCRIPTION_PURCHASE_INDL', 0, 1, NULL, 2097152),
(48, 'SUBSCRIPTION_PURCHASE_INSTL', 0, 1, NULL, 2097152),
(49, 'SUBSCRIPTION_RENEW_INDL', 0, 1, NULL, 2097152),
(50, 'SUBSCRIPTION_RENEW_INSTL', 0, 1, NULL, 2097152),
(51, 'CITATION_EDITOR_AUTHOR_QUERY', 0, 1, NULL, NULL),
(52, 'REVISED_VERSION_NOTIFY', 0, 1, NULL, 512),
(53, 'ORCID_COLLECT_AUTHOR_ID', 0, 1, NULL, NULL),
(54, 'MANUAL_PAYMENT_NOTIFICATION', 0, 1, NULL, NULL),
(55, 'PAYPAL_INVESTIGATE_PAYMENT', 0, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_templates_default_data`
--

CREATE TABLE `email_templates_default_data` (
  `email_key` varchar(64) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT 'en_US',
  `subject` varchar(120) NOT NULL,
  `body` text,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `email_templates_default_data`
--

INSERT INTO `email_templates_default_data` (`email_key`, `locale`, `subject`, `body`, `description`) VALUES
('CITATION_EDITOR_AUTHOR_QUERY', 'ar_IQ', 'تحرير إقتباس', '{$authorFirstName},<br />\n<br />\nلطفاً، هل بإمكانك التحقق من أو تزويدنا بالشكل الصحيح للاقتباس للمرجع المبين أدناه والمذكور في مؤلَّفك الموسوم {$submissionTitle}:<br />\n<br />\n{$rawCitation}<br />\n<br />\nمع الشكر!<br />\n<br />\n{$userFirstName}<br />\nالمدقق، {$contextName}<br />\n', 'هذه الرسالة تسمح لمدققي المجلة بالمطالبة بمعلومات إضافية عن المراجع من المؤلفين.'),
('CITATION_EDITOR_AUTHOR_QUERY', 'en_US', 'Citation Editing', '{$authorFirstName},<br />\n<br />\nCould you please verify or provide us with the proper citation for the following reference from your article, {$submissionTitle}:<br />\n<br />\n{$rawCitation}<br />\n<br />\nThanks!<br />\n<br />\n{$userFirstName}<br />\nCopy-Editor, {$contextName}<br />\n', 'This email allows copyeditors to request additional information about references from authors.'),
('CITATION_EDITOR_AUTHOR_QUERY', 'id_ID', 'Penyuntingan Sitiran', '{$authorFirstName},<br />\n<br />\nMohon verifikasi atau berikan sitasi yang benar untuk rujukan berikut dari artikel Anda, {$submissionTitle}:<br />\n<br />\n{$rawCitation}<br />\n<br />\nTerimakasih!<br />\n<br />\n{$userFirstName}<br />\nCopy-Editor, {$contextName}<br />\n', 'Email ini memungkinkan copyeditor untuk meminta informasi tambahan tentang rujukan dari penulis.'),
('COPYEDIT_REQUEST', 'ar_IQ', 'إلتماس نسخة التدقيق', '{$participantName}:<br />\n<br />\nأود مطالبتك بتوفير نسخة قابلة للتدقيق للمؤلَّف الموسوم &quot;{$submissionTitle}&quot; والمرسل إلى {$contextName} عبر اتباع الخطوات الآتية:<br />\n1. أنقر على رابط طلب النشر أدناه.<br />\n2. إفتح أي ملفات متوافرة ضمن الملفات المسودة وأجر متطلبات التدقيق مضيفاً أي قرارات أو ملاحظات تتعلق بالتدقيق حسب الحاجة.<br />\n3. إحفظ الملفات المدققة، ثم إرفعها إلى لوحة المدققات.<br />\n4. أعلم المحرر بأن جميع الملفات صارت جاهزة، وأن بالإمكان الشروع بعملية الإنتاج.<br />\n<br />\nرابط {$contextName}: {$contextUrl}<br />\nرابط الطلب: {$submissionUrl}<br />\nاسم الدخول: {$participantUsername}<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يعنونها محرر القسم إلى مدقق القسم مطالباً إياه بالشروع في عملية معالجة نسخة المؤلَّف قيد المعالجة، وهي توفر له معلومات عن طلب النشر وكيفية الوصول إليه ضمن موقع المجلة.'),
('COPYEDIT_REQUEST', 'en_US', 'Copyediting Request', '{$participantName}:<br />\n<br />\nI would ask that you undertake the copyediting of &quot;{$submissionTitle}&quot; for {$contextName} by following these steps.<br />\n1. Click on the Submission URL below.<br />\n2. Open any files available under Draft Files and do your copyediting, while adding any Copyediting Discussions as needed.<br />\n3. Save copyedited file(s), and upload to Copyedited panel.<br />\n4. Notify the Editor that all files have been prepared, and that the Production process may begin.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$participantUsername}<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to a submission\'s Copyeditor to request that they begin the copyediting process. It provides information about the submission and how to access it.'),
('COPYEDIT_REQUEST', 'id_ID', 'Permohonan Copyediting', '{$participantName}:<br />\n<br />\nKami meminta Anda melakukan copyediting terhadap &quot;{$submissionTitle}&quot; untuk {$contextName} mengikuti langkah-langkah berikut.<br />\n1. Klik URL Naskah di bawah.<br />\n2. Buka semua file yang ada di file Draft dan lakukan copyediting, tambahkan Diskusi Copyediting sesuai kebutuhan.<br />\n3. Simpan file yang telah di-copyedit, dan unggah ke panel Sudah Copyedit.<br />\n4. Beritahu Editor bahwa semua file telah siap, dan bahwa proses Produksi dapat dimulai.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$participantUsername}<br />\n<br />\n{$editorialContactSignature}', 'Email ini dari Editor Bagian ke Copyeditor naskah agar memulai proses copyediting.  Email ini memuat informasi terkait penyerahan naskah dan bagaimana mengaksesnya.'),
('EDITOR_ASSIGN', 'ar_IQ', 'تعيين المحرر', '{$editorialContactName}:<br />\n<br />\nإن طلب النشر الموسوم &quot;{$submissionTitle},&quot; المقدم إلى {$contextName} قد تمت إحالته إليك لتقوم بإجراءات التحرير المتعلقة به بصفتك أحد أعضاء هيئة التحرير في هذا القسم من المجلة..<br />\n<br />\nرابط الطلب: {$submissionUrl}<br />\nاسم الدخول: {$editorUsername}<br />\n<br />\nشكراً لك.<br />\n{$editorialContactSignature}', 'هذه الرسالة تبلغ أحد المحررين في قسم ما من المجلة بإحالة مهمة تحرير أحد طلبات التقديم المرسلة إلى ذلك القسم من المجلة إليه، وتقدم له رابط الشروع بالتحرير عبر موقع المجلة.'),
('EDITOR_ASSIGN', 'en_US', 'Editorial Assignment', '{$editorialContactName}:<br />\n<br />\nThe submission, &quot;{$submissionTitle},&quot; to {$contextName} has been assigned to you to see through the editorial process in your role as Section Editor.<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$editorUsername}<br />\n<br />\nThank you,<br />\n{$editorialContactSignature}', 'This email notifies a Section Editor that the Editor has assigned them the task of overseeing a submission through the editing process. It provides information about the submission and how to access the journal site.'),
('EDITOR_ASSIGN', 'id_ID', 'Penugasan Editorial', '{$editorialContactName}:<br />\n<br />\nNaskah, &quot;{$submissionTitle},&quot; di {$contextName} telah ditugaskan kepada Anda sebagai Editor Bagian untuk memastikan selesainya seluruh proses editorial.<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$editorUsername}<br />\n<br />\nTerimakasih,<br />\n{$editorialContactSignature}', 'Email ini memberitahu Editor Bagian bahwa Editor telah memberikan tugas untuk mengawasi suatu naskah dalam menyelesaikan proses editorial. Email ini memberikan informasi mengenai naskah dan cara mengaksesnya di website jurnal.'),
('EDITOR_DECISION_ACCEPT', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nقرارنا هو: قبول طلب النشر<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى طالب النشر يعلمه فيها بقبول هيئة التحرير لطلبه.'),
('EDITOR_DECISION_ACCEPT', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Accept Submission<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them of a final \"accept submission\" decision regarding their submission.'),
('EDITOR_DECISION_ACCEPT', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Naskah Diterima<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Naskah Diterima\".'),
('EDITOR_DECISION_DECLINE', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nقرارنا هو: الرفض<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى طالب النشر يعلمه فيها بالقرار النهائي لهيئة التحرير بشأن طلبه، ملخصه (رفض الطلب).'),
('EDITOR_DECISION_DECLINE', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Decline Submission<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them of a final \"decline\" decision regarding their submission.'),
('EDITOR_DECISION_DECLINE', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Naskah Ditolak<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Naskah Ditolak\".'),
('EDITOR_DECISION_INITIAL_DECLINE', 'ar_IQ', 'قرار المحرر', '\n			{$authorName}:<br />\n<br />\nلقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}، &quot;{$submissionTitle}&quot;.<br />\n<br />\nإن قرارنا هو: رفض النشر<br />\n<br />\n{$editorialContactSignature}<br />', 'هذه الرسالة تعنون إلى المؤلف إذا رفض المحرر طلبه بشكل مبدئي قبل الدخول في مرحلة التحكيم'),
('EDITOR_DECISION_INITIAL_DECLINE', 'en_US', 'Editor Decision', '\n			{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Decline Submission<br />\n<br />\n{$editorialContactSignature}<br />', 'This email is send to the author if the editor declines his submission initially, before the review stage'),
('EDITOR_DECISION_RESUBMIT', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nقرارنا هو: إعادة التقديم مجدداً<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى طالب النشر يعلمه فيها بالقرار النهائي لهيئة التحرير بشأن طلبه، ملخصه (إعادة تقديم طلب النشر).'),
('EDITOR_DECISION_RESUBMIT', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Resubmit for Review<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them of a final \"resubmit\" decision regarding their submission.'),
('EDITOR_DECISION_RESUBMIT', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Kirim Ulang untuk Review<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa Penulis perlu mengirim ulang naskahnya.'),
('EDITOR_DECISION_REVISIONS', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nقرارنا هو: بعض التنقيحات مطلوبة<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى طالب النشر يعلمه فيها بالقرار النهائي لهيئة التحرير بأن مؤلفه بحاجة إلى بعض التنقيحات.'),
('EDITOR_DECISION_REVISIONS', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is: Revisions Required<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them of a final \"revisions required\" decision regarding their submission.'),
('EDITOR_DECISION_REVISIONS', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Perlu Revisi<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan keputusan final \"Perlu Revisi\".'),
('EDITOR_DECISION_SEND_TO_EXTERNAL', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nقد توصلنا إلى قرار بشأن طلبك للنشر في {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nقرارنا هو: إرسال طلبك إلى التحكيم<br />\n<br />\nرابط الطلب: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى طالب النشر يعلمه فيها بقرار هيئة التحرير بأن طلبه قد أرسل إلى محكم خارجي.'),
('EDITOR_DECISION_SEND_TO_EXTERNAL', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nWe have reached a decision regarding your submission to {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nOur decision is to: Send to Review<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them that their submission is being sent to an external review.'),
('EDITOR_DECISION_SEND_TO_EXTERNAL', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nKami telah membuat keputusan terkait naskah yang Anda kirimkan ke {$contextName}, &quot;{$submissionTitle}&quot;.<br />\n<br />\nKeputusan kami adalah: Dikirimkan ke Reviewer Eksternal<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa naskah dikirimkan ke reviewer eksternal.'),
('EDITOR_DECISION_SEND_TO_PRODUCTION', 'ar_IQ', 'قرار هيئة التحرير', '{$authorName}:<br />\n<br />\nإن متعلقات تحرير مؤلَّفك الموسوم &quot;{$submissionTitle},&quot; قد اكتملت. نحن الآن نرسله إلى مرحلة الإنتاج.<br />\n<br />\nرابط الطلب: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها مدير التحرير أو محرر القسم إلى المؤلف لإبلاغه بأن عمله قد تم إرساله إلى مرحلة الإنتاج.'),
('EDITOR_DECISION_SEND_TO_PRODUCTION', 'en_US', 'Editor Decision', '{$authorName}:<br />\n<br />\nThe editing of your submission, &quot;{$submissionTitle},&quot; is complete.  We are now sending it to production.<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the Editor or Section Editor to an Author notifies them that their submission is being sent to production.'),
('EDITOR_DECISION_SEND_TO_PRODUCTION', 'id_ID', 'Keputusan Editor', '{$authorName}:<br />\n<br />\nProses editing naskah Anda, &quot;{$submissionTitle},&quot; telah selesai.  Kami sekarang mengirimkannya ke produksi.<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'Email ini dari Editor atau Editor Bagian kepada Penulis memberitahukan bahwa naskah dikirim ke produksi.'),
('EDITOR_RECOMMENDATION', 'ar_IQ', 'توصيات المحرر', '{$editors}:<br />\n<br />\nإن التوصيات المتعلقة بطلب النشر في {$contextName}، &quot;{$submissionTitle}&quot; هي: {$recommendation}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'هذه الرسالة يعنونها المحرر المشارك في تقديم التوصيات أو محرر القسم إلى جهة التحرير المعنية باتخاذ القرار لإشعارها بالتوصية النهائية الخاصة بطلب نشر مقدم إلى المجلة.'),
('EDITOR_RECOMMENDATION', 'en_US', 'Editor Recommendation', '{$editors}:<br />\n<br />\nThe recommendation regarding the submission to {$contextName}, &quot;{$submissionTitle}&quot; is: {$recommendation}<br />\n<br />\n{$editorialContactSignature}<br />\n', 'This email from the recommending Editor or Section Editor to the decision making Editors or Section Editors notifies them of a final recommendation regarding the submission.'),
('EMAIL_LINK', 'ar_IQ', 'مقالة قد تكون مهمة', 'لقد وجدت أن المقالة العلمية الموسومة &quot;{$submissionTitle}&quot; قد تحوز على إهتمامك، وهي من تأليف {$authorName} ومنشورة في المجلد {$volume}، العدد {$number} لسنة ({$year}) من {$contextName} على الرابط &quot;{$articleUrl}&quot;.', 'قالب هذه الرسالة نموذج لما يرسله قارئ مسجل في موقع المجلة إلى أحد معارفه ملفتاً نظره إلى مقالة علمية أو بحث. تلك الخاصية متوافرة عبر أدوات القراءة وينبغي تفعيلها من قبل مدير موقع المجلة في صفحة إدارة أدوات القراءة.'),
('EMAIL_LINK', 'en_US', 'Article of Possible Interest', 'Thought you might be interested in seeing &quot;{$submissionTitle}&quot; by {$authorName} published in Vol {$volume}, No {$number} ({$year}) of {$contextName} at &quot;{$articleUrl}&quot;.', 'This email template provides a registered reader with the opportunity to send information about an article to somebody who may be interested. It is available via the Reading Tools and must be enabled by the Journal Manager in the Reading Tools Administration page.'),
('EMAIL_LINK', 'id_ID', 'Artikel Menarik', 'Anda mungkin tertarik dengan &quot;{$submissionTitle}&quot; oleh {$authorName} diterbitkan di Vol {$volume}, No {$number} ({$year}) {$contextName} yang tersedia di &quot;{$articleUrl}&quot;.', 'Template email ini memungkinkan pembaca terdaftar mengirimkan informasi tentang suatu artikel kepada seseorang yang mungkin akan tertarik. Email ini tersedia di Alat Baca dan harus diaktifkan oleh Manajer Jurnal di halaman Administrasi Alat Baca.'),
('LAYOUT_COMPLETE', 'ar_IQ', 'التصميم الطباعي إكتمل', '{$editorialContactName}:<br />\n<br />\nالألواح الطباعية قد إكتمل تحضيرها للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName} وصارت جاهزة للمراجعة.<br />\n<br />\nإن كانت لديكم أي استفسارات، لطفاً، إتصلوا بي.<br />\n<br />\n{$participantName}', 'هذه الرسالة يعنونها المصمم الطباعي إلى محرر القسم يعلمه فيها بأن مهمة التحضير الطباعي لمؤلَّف معين قد إكتملت.'),
('LAYOUT_COMPLETE', 'en_US', 'Galleys Complete', '{$editorialContactName}:<br />\n<br />\nGalleys have now been prepared for the manuscript, &quot;{$submissionTitle},&quot; for {$contextName} and are ready for proofreading.<br />\n<br />\nIf you have any questions, please contact me.<br />\n<br />\n{$participantName}', 'This email from the Layout Editor to the Section Editor notifies them that the layout process has been completed.'),
('LAYOUT_COMPLETE', 'id_ID', 'Galley Selesai', '{$editorialContactName}:<br />\n<br />\nGalley telah dipersiapkan bagi naskah, &quot;{$submissionTitle},&quot; di {$contextName} dan siap untuk proofreading.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$participantName}', 'Email from the Layout Editor kepada Editor Bagian memberitahukan bahwa proses layout telah selesai.'),
('LAYOUT_REQUEST', 'ar_IQ', 'إلتماس التنضيد الطباعي', '{$participantName}:<br />\n<br />\nإن المؤلَّف الموسوم &quot;{$submissionTitle}&quot; والمرسل إلى {$contextName} يحتاج الآن إلى تهيئة إخراجه الطباعي عبر اتباع الخطوات الآتية:<br />\n1. أنقر على رابط طلب النشر أدناه.<br />\n2. أدخل إلى موقع المجلة واستعمل النسخة المحضرة للمعالجة الطباعية لصناعة اللوح الطباعي المتوافق مع معايير المجلة.<br />\n3. إرفع هذه الألواح إلى قسم ملفات الألواح.<br />\n4. أبلغ المحرر مستعملاً محاورات الإنتاج بأن الألواح الطباعية مرفوعة وجاهزة.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nرابط الطلب: {$submissionLayoutUrl}<br />\nاسم الدخول: {$participantUsername}<br />\n<br />\nلو تعذر عليك تولي هذه المهمة حالياً أو كان لديك أية أسئلة، لطفاً لا تترد في مراسلتي. نشكر لك مساهمتك في هذه المجلة.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يعنونها محرر القسم إلى المصمم الطباعي معلماً إياه بتعيينه لمهمة التصميم الطباعي لمؤلَّفٍ معين، وهي توفر له رابط الوصول إلى الملف المعني في موقع المجلة.'),
('LAYOUT_REQUEST', 'en_US', 'Request Galleys', '{$participantName}:<br />\n<br />\nThe submission &quot;{$submissionTitle}&quot; to {$contextName} now needs galleys laid out by following these steps.<br />\n1. Click on the Submission URL below.<br />\n2. Log into the journal and use the Production Ready files to create the galleys according to the journal\'s standards.<br />\n3. Upload the galleys to the Galley Files section.<br />\n4. Notify the Editor using Production Discussions that the galleys are uploaded and ready.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nSubmission URL: {$submissionLayoutUrl}<br />\nUsername: {$participantUsername}<br />\n<br />\nIf you are unable to undertake this work at this time or have any questions, please contact me. Thank you for your contribution to this journal.<br />\n<br />\n{$editorialContactSignature}', 'This email from the Section Editor to the Layout Editor notifies them that they have been assigned the task of performing layout editing on a submission. It provides information about the submission and how to access it.'),
('LAYOUT_REQUEST', 'id_ID', 'Permohonan Galley', '{$participantName}:<br />\n<br />\nNaskah &quot;{$submissionTitle}&quot; di {$contextName} memerlukan pembuatan galley dengan mengikuti langkah-langkah berikut.<br />\n1. Klik URL Naskah di bawah.<br />\n2. Login ke jurnal dan gunakan file Siap Produksi untuk membuat galley sesuai standar jurnal.<br />\n3. Unggah galley ke bagian File Galley.<br />\n4. Beritahu Editor menggunakan Diskusi Produksi bahwa galley telah diunggah dan siap digunakan.<br />\n<br />\n{$contextName} URL: {$contextUrl}<br />\nURL Naskah: {$submissionLayoutUrl}<br />\nNama pengguna: {$participantUsername}<br />\n<br />\nJika Anda tidak dapat melakukan tugas ini saat ini atau mempunyai pertanyaan, silakan hubungi kami.  Terimakasih atas kontribusi Anda ke jurnal ini.<br />\n<br />\n{$editorialContactSignature}', 'Email dari Editor Bagian kepada Layout Editor memberitahukan bahwa mereka telah ditugaskan untuk melakukan layout editing terhadap suatu naskah.  Email ini memuat informasi terkait penyerahan naskah dan bagaimana mengaksesnya.'),
('LOCKSS_EXISTING_ARCHIVE', 'ar_IQ', 'طلب أرشفة لمجلة {$contextName}', 'عزيزنا [أمين مكتبة الجامعة]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;، هي مجلة علمية فيها أحد أعضاء مؤسستك، والمدعو [اسم العضو]، يعمل بصفة [صفة العضو]. وهذه المجلة تتطلع إلى تأسيس أرشيف متوافق مع نوع LOCKSS (Lots of Copies Keep Stuff Safe) مع هذه المكتبة وغيرها من مكتبات الجامعات الأخرى.<br />\n<br />\n[وصف موجز للمجلة]<br />\n<br />\nوالرابط الخاص ببيان الناشر المتعلق بأرشيف LOCKSS لمجلتنا هو: {$contextUrl}/gateway/lockss<br />\n<br />\nنحن نتفهم أنك مشترك سابقاً في LOCKSS. إن كان بإمكاننا تزويدك بأي بيانات وصفية إضافية لغرض تسجيل مجلتنا في نسختك من LOCKSS، فإننا سنكون سعداء بتوفيرها لك.<br />\n<br />\nشكراً لك.<br />\n{$principalContactSignature}', 'هذه الرسالة تطلب من القيِّم على أرشيف LOCKSS ليأخذ بنظر الإعتبار إضافة هذه المجلة إلى أرشيفه. إنها تزوده برابط بيان الناشر المتعلق بأرشيف LOCKSS الخاص بالمجلة.'),
('LOCKSS_EXISTING_ARCHIVE', 'en_US', 'Archiving Request for {$contextName}', 'Dear [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, is a journal for which a member of your faculty, [name of member], serves as a [title of position]. The journal is seeking to establish a LOCKSS (Lots of Copies Keep Stuff Safe) compliant archive with this and other university libraries.<br />\n<br />\n[Brief description of journal]<br />\n<br />\nThe URL to the LOCKSS Publisher Manifest for our journal is: {$contextUrl}/gateway/lockss<br />\n<br />\nWe understand that you are already participating in LOCKSS. If we can provide any additional metadata for purposes of registering our journal with your version of LOCKSS, we would be happy to provide it.<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email requests the keeper of a LOCKSS archive to consider including this journal in their archive. It provides the URL to the journal\'s LOCKSS Publisher Manifest.'),
('LOCKSS_EXISTING_ARCHIVE', 'id_ID', 'Permohonan Pengarsipan untuk {$contextName}', 'Yang terhormat [Pustakawan Universitas]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, merupakan jurnal yang salah satu anggota fakultas Anda, [name of member], menjadi [title of position]. Jurnal ini hendak membuat arsip sesuai LOCKSS (Lots of Copies Keep Stuff Safe) di perpustakaan Anda dan perpustakaan universitas lainnya.<br />\n<br />\n[Deskripsi singkat jurnal]<br />\n<br />\nURL Publisher Manifest LOCKSS jurnal kami adalah: {$contextUrl}/gateway/lockss<br />\n<br />\nKami memahami bahwa Anda telah bergabung dengan LOCKSS. Jika kami perlu memberikan metadata tambahan untuk mendaftarkan jurnal kami di versi LOCKSS Anda, dengan senang hati kami akan memberikannya.<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini meminta penjaga arsip LOCKSS untuk mempertimbangkan memasukkan jurnal ini ke dalam arsip mereka. Email ini memberikan URL Publisher Manifest LOCKSS jurnal.'),
('LOCKSS_NEW_ARCHIVE', 'ar_IQ', 'إلتماس أرشفة لمجلة {$contextName}', 'عزيزنا [أمين مكتبة الجامعة<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;، هي مجلة علمية فيها أحد أعضاء مؤسستك، والمدعو [اسم العضو]، يعمل بصفة [صفة العضو]. وهذه المجلة تتطلع إلى تأسيس أرشيف متوافق مع نوع LOCKSS (Lots of Copies Keep Stuff Safe) مع هذه المكتبة وغيرها من مكتبات الجامعات الأخرى.<br />\n<br />\n[وصف موجز للمجلة]<br />\n<br />\nإن برنامج LOCKSS &amp;lt;http://lockss.org/&amp;gt; باعتباره أحد الحقوق العالمية للمكتبات والناشرين، هو مثال عملي على مخازن الأرشفة والحفظ المتوزع. المزيد من التفاصيل تتوضح أدناه. هذا البرنامج، الذي يمكن تشغيله على حاسوب شخصي عادي، مجاني، وبالإمكان تفعيله بسهولة على الشبكة العنكبوتية، ولا يتطلب إلا القليل من إجراءات الصيانة والمتابعة.<br />\n<br />\nللمساعدة على أرشفة مجلتنا، ندعوك لتكون أحد أعضاء مجتمع LOCKSS، للتمكن من جمع وحفظ العناوين المقدمة من قبل مؤسستك التعليمية ومن قبل الدارسين عبر العالم. للقيام بذلك، حرض أحداً من منتسبيك لزيارة موقع LOCKSS لتحصل على معلومات عن كيفية عمل هذا النظام. أنا أتطلع شوقاً لأتعرف إمكانيتك على توفير هذا النوع من الدعم لتساعد المجلة على أرشفة محتوياتها.<br />\n<br />\nشكراً لك.<br />\n{$principalContactSignature}', 'هذه الرسالة تشجع متلقيها على المبادرة إلى اعتماد العمل بنظام LOCKSS وتضمين هذه المجلة في الأرشيف الخاص به. إنها تتضمن معلومات عن تمهيد نظام LOCKSS وكيفية المشاركة فيه.'),
('LOCKSS_NEW_ARCHIVE', 'en_US', 'Archiving Request for {$contextName}', 'Dear [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, is a journal for which a member of your faculty, [name of member] serves as a [title of position]. The journal is seeking to establish a LOCKSS (Lots of Copies Keep Stuff Safe) compliant archive with this and other university libraries.<br />\n<br />\n[Brief description of journal]<br />\n<br />\nThe LOCKSS Program &amp;lt;http://lockss.org/&amp;gt;, an international library/publisher initiative, is a working example of a distributed preservation and archiving repository, additional details are below. The software, which runs on an ordinary personal computer is free; the system is easily brought on-line; very little ongoing maintenance is required.<br />\n<br />\nTo assist in the archiving of our journal, we invite you to become a member of the LOCKSS community, to help collect and preserve titles produced by your faculty and by other scholars worldwide. To do so, please have someone on your staff visit the LOCKSS site for information on how this system operates. I look forward to hearing from you on the feasibility of providing this archiving support for this journal.<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email encourages the recipient to participate in the LOCKSS initiative and include this journal in the archive. It provides information about the LOCKSS initiative and ways to become involved.'),
('LOCKSS_NEW_ARCHIVE', 'id_ID', 'Permohonan Pengarsipan untuk {$contextName}', 'Yang terhormat [University Librarian]<br />\n<br />\n{$contextName} &amp;lt;{$contextUrl}&amp;gt;, merupakan jurnal yang salah satu anggota fakultas Anda, [name of member], menjadi [title of position]. Jurnal ini hendak membuat arsip sesuai LOCKSS (Lots of Copies Keep Stuff Safe) di perpustakaan Anda dan perpustakaan universitas lainnya.<br />\n<br />\n[Deskripsi singkat jurnal]<br />\n<br />\nProgram LOCKSS &amp;lt;http://lockss.org/&amp;gt;, suatu gerakan perpustakaan/penerbit internasional, merupakan contoh nyata penyimpanan dan pengarsipan terdistribusi, detail tambahan dapat dilihat di bawah. Piranti lunak ini, yang berjalan di komputer biasa, merupakan piranti lunak yang gratis; sistemnya mudah di-online-kan; tidak membutuhkan banyak perawatan.<br />\n<br />\nUntuk membantu mengarsipkan jurnal kami, kami mengundang Anda untuk menjadi anggota komunitas LOCKSS, guna membantu mengumpulkan dan menyimpan judul-judul karya anggota fakultas Anda dan ilmuwan lainnya di seluruh dunia. Jika berkenan, silakan mengunjungi website LOCKSS untuk informasi mengenai bagaimana cara kerja sistem ini. Kami berharap Anda berkenan mendukung pengarsipan jurnal ini.<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini mendorong penerima untuk berpartisipasi di gerakan LOCKSS dan memasukkan jurnal ini dalam arsip. Email ini memberikan informasi tentang gerakan LOCKSS dan cara untuk bergabung.'),
('MANUAL_PAYMENT_NOTIFICATION', 'ar_IQ', 'إشعار الدفع اليدوي', '\n			عملية دفع يدوي بحاجة إلى معالجتها تخص المجلة {$contextName} والمستخدم {$userFullName} (اسم المستخدم &quot;{$userName}&quot;).<br />\n<br />\nالفقرة التي في النية دفع المال لها هي &quot;{$itemName}&quot;.<br />\nالكلفة هي {$itemCost} ({$itemCurrencyCode}).<br />\n<br />\nتم إنشاء رسالة البريد الالكتروني هذه من قبل إضافة الدفع المالي اليدوي لنظام المجلات المفتوحة.', 'قالب هذه الرسالة يستعمل لإشعار رئيس تحرير المجلة أو مدير تحريرها بحدوث مطالبة لدفع المال يدوياً في المجلة.'),
('MANUAL_PAYMENT_NOTIFICATION', 'en_US', 'Manual Payment Notification', 'A manual payment needs to be processed for the journal {$contextName} and the user {$userFullName} (username &quot;{$userName}&quot;).<br />\n<br />\nThe item being paid for is &quot;{$itemName}&quot;.<br />\nThe cost is {$itemCost} ({$itemCurrencyCode}).<br />\n<br />\nThis email was generated by Open Journal Systems\' Manual Payment plugin.', 'This email template is used to notify a journal manager contact that a manual payment was requested.'),
('MANUAL_PAYMENT_NOTIFICATION', 'id_ID', ' Pemberitahuan Pembayaran Manual ', ' Pembayaran manual harus diproses untuk jurnal dan {$contextName} pengguna {$userFullName} (nama pengguna &quot;{$userName}&quot;).<br />\nThe item being paid for &quot;{$itemName}&quot;. <br />\nBiaya adalah {$itemCost} ({$itemCurrencyCode}). <br />\nSurat elektronik ini dibuat oleh plugin pembayaran manual IOJS.', ' Email ini digunakan untuk memberitahukan kontak manajer jurnal bahwa pembayaran manual dibutuhkan.'),
('NOTIFICATION', 'ar_IQ', 'إشعار جديد من {$siteTitle}', 'لديك إشعار جديد من {$siteTitle}:<br />\n<br />\n{$notificationContents}<br />\n<br />\nالرابط: {$url}<br />\n<br />\n{$principalContactSignature}', 'رسالة البريد الالكتروني هذه مرسلة إلى المستخدمين المسجلين الذين اختاروا استلام إشعار من هذا النوع عبر البريد الالكتروني.'),
('NOTIFICATION', 'en_US', 'New notification from {$siteTitle}', 'You have a new notification from {$siteTitle}:<br />\n<br />\n{$notificationContents}<br />\n<br />\nLink: {$url}<br />\n<br />\n{$principalContactSignature}', 'The email is sent to registered users that have selected to have this type of notification emailed to them.'),
('NOTIFICATION', 'id_ID', 'Notifikasi baru dari {$siteTitle}', 'Anda memperoleh satu notifikasi baru dari {$siteTitle}:<br />\n<br />\n{$notificationContents}<br />\n<br />\nTautan: {$url}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar yang memilih untuk memperoleh jenis notifikasi ini dikirimkan melalui email.'),
('NOTIFICATION_CENTER_DEFAULT', 'ar_IQ', 'رسالة تتعلق بالمجلة {$contextName}', 'لطفاً أكتب رسالتك هنا.', 'هذه الرسالة الإفتراضية (الخالية) التي يستعملها منشئ الرسائل في مركز إشعارات النظام.'),
('NOTIFICATION_CENTER_DEFAULT', 'en_US', 'A message regarding {$contextName}', 'Please enter your message.', 'The default (blank) message used in the Notification Center Message Listbuilder.'),
('NOTIFICATION_CENTER_DEFAULT', 'id_ID', 'Pesan terkait {$contextName}', 'Silakan tuliskan pesan Anda.', 'Pesan default (kosong) yang digunakan di Pengelola Pesan Pusat Notifikasi.'),
('OPEN_ACCESS_NOTIFY', 'ar_IQ', 'صدور عدد مجاني الآن', 'قراءنا:<br />\n<br />\n{$contextName} قد أصدرت تواً عدداً مجانياً. نحن ندعوك لمراجعة جدول المحتويات هنا، ومن ثم زيارة موقعنا ({$contextUrl}) لتطلع على المواضيع والفقرات التي تحوز على إهتمامك.<br />\n<br />\nنشكر لكم إهتمامكم الدائم بنتاجاتنا؛<br />\n{$editorialContactSignature}', 'هذه الرسالة تُعلم قارئاً مسجلاً بأن مدير المجلة قد أنشأ له اشتراكاً، وهي تتضمن رابط المجلة مع تعليمات الوصول إليها.'),
('OPEN_ACCESS_NOTIFY', 'en_US', 'Issue Now Open Access', 'Readers:<br />\n<br />\n{$contextName} has just made available in an open access format the following issue. We invite you to review the Table of Contents here and then visit our web site ({$contextUrl}) to review articles and items of interest.<br />\n<br />\nThanks for the continuing interest in our work,<br />\n{$editorialContactSignature}', 'This email is sent to registered readers who have requested to receive a notification email when an issue becomes open access.'),
('OPEN_ACCESS_NOTIFY', 'id_ID', 'Terbitan Sudah Open Access', 'Readers:<br />\n<br />\n{$contextName} telah menjadikan terbitan berikut Open Access. Kami mengundang Anda untuk menelaah Daftar Isi berikut dan mengunjungi website kami ({$contextUrl}) untuk membaca artikel dan item yang Anda minati.<br />\n<br />\nTerimakasih atas perhatiannya,<br />\n{$editorialContactSignature}', 'Email ini dikirimkan ke pembaca terdaftar yang meminta untuk dikirimkan email pemberitahuan saat suatu terbitan menjadi Open Access.'),
('ORCID_COLLECT_AUTHOR_ID', 'en_US', 'Submission ORCID', 'Dear {$authorName},\n\nYou have been listed as a co-author on the manuscript submission \"{$articleTitle}\" to {$journalName}. \n\nTo confirm your authorship, please add your ORCID id to this submission by visiting the link provided below.\n\n{$authorOrcidUrl}\n\nIf you have any questions, please contact me.\n\n{$editorialContactSignature}\n\n\n', 'This email template is used to collect the ORCID id\'s from co-authors.'),
('PASSWORD_RESET', 'ar_IQ', 'إعادة تعيين كلمة المرور', 'كلمة مرورك أُعيد تعيينها بنجاح لتتمكن من الولوج إلى موقع {$siteTitle}. لطفاً، إحتفظ باسم الدخول وكلمة المرور المبينين أدناه كونهما ضروريان لمتابعة كل أعمالك في موقع المجلة.<br />\n<br />\nاسم الدخول: {$username}<br />\nكلمة المرور: {$password}<br />\n<br />\n{$principalContactSignature}', 'هذه الرسالة معنونة إلى المستخدمين المسجلين الذي تمكنوا من إعادة تعيين كلمات مرورهم بنجاح متبعين الخطوات الموضحة في رسالة إعادة تعيين كلمة المرور.'),
('PASSWORD_RESET', 'en_US', 'Password Reset', 'Your password has been successfully reset for use with the {$siteTitle} web site. Please retain this username and password, as it is necessary for all work with the journal.<br />\n<br />\nYour username: {$username}<br />\nPassword: {$password}<br />\n<br />\n{$principalContactSignature}', 'This email is sent to a registered user when they have successfully reset their password following the process described in the PASSWORD_RESET_CONFIRM email.'),
('PASSWORD_RESET', 'id_ID', 'Reset Sandi', 'Sandi akun Anda di website {$siteTitle} telah berhasil di-reset. Silakan simpan nama pengguna dan sandi ini karena akan dibutuhkan untuk semua kegiatan terkait jurnal di website ini.<br />\n<br />\nNama pengguna Anda: {$username}<br />\nSandi: {$password}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar saat proses reset password berhasil sesuai prosedur di email PASSWORD_RESET_CONFIRM.'),
('PASSWORD_RESET_CONFIRM', 'ar_IQ', 'تأكيد إعادة تعيين كلمة المرور', 'لقد تلقينا طلباً لإعادة تعيين كلمة مرورك في الموقع {$siteTitle}.<br />\n<br />\nإن لم تكن أنت شخصياً من أرسل هذا الطلب، لطفاً تجاهل هذه الرسالة لتحتفظ بكلمة مرورك إلى الموقع دون تغيير. لو أردت إعادة تعيين كلمة مرورك، أنقر الرابط أدناه.<br />\n<br />\nأعد تعيين كلمة مروري: {$url}<br />\n<br />\n{$principalContactSignature}', 'هذه الرسالة معنونة إلى المستخدمين المسجلين الذي أبلغوا عن فقدانهم لكلمات مرورهم إلى الموقع، أو الذين تعذر عليهم الولوج لأي سبب آخر. سيحصل هؤلاء على رابط يمكنهم عند النقر عليه إعادة تعيين كلمات مرورهم.'),
('PASSWORD_RESET_CONFIRM', 'en_US', 'Password Reset Confirmation', 'We have received a request to reset your password for the {$siteTitle} web site.<br />\n<br />\nIf you did not make this request, please ignore this email and your password will not be changed. If you wish to reset your password, click on the below URL.<br />\n<br />\nReset my password: {$url}<br />\n<br />\n{$principalContactSignature}', 'This email is sent to a registered user when they indicate that they have forgotten their password or are unable to login. It provides a URL they can follow to reset their password.'),
('PASSWORD_RESET_CONFIRM', 'id_ID', 'Konfirmasi Reset Sandi', 'Kami menerima permintaan reset sandi untuk akun Anda di website {$siteTitle}.<br />\n<br />\nJika Anda tidak merasa mengajukan permintaan ini, abaikan pesan ini dan sandi Anda tidak akan diubah.  Jika Anda memang ingin melakukan reset sandi, klik tautan berikut ini.<br />\n<br />\nReset sandi saya: {$url}<br />\n<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna terdaftar ketika lupa sandi atau tidak dapat login. Email ini memberikan URL untuk melakukan reset sandi.'),
('PAYPAL_INVESTIGATE_PAYMENT', 'ar_IQ', 'نشاط غير عادي من PayPal', 'نظام المجلات المفتوحة قد تعرض إلى نشاط غير عادي يتعلق بآلية دعم المدفوعات عبر PayPal يخص المجلة {$contextName}. هذا النشاط قد يستدعي المزيد من التحقيق بشأنه ولو يدوياً.<br />\n                       <br />\nهذه الرسالة أنشأتها إضافة PayPal لنظام المجلات المفتوحة.<br />\n<br />\nمعلومات الطرح الكامل للطلب:<br />\n{$postInfo}<br />\n<br />\nمعلومات إضافية (لو توفرت):<br />\n{$additionalInfo}<br />\n<br />\nمتغيرات الملقم:<br />\n{$serverVars}<br />\n', 'قالب الرسالة الالكترونية هذا يستعمل لإشعار عنوان الاتصال الرئيسي في المجلة بحدوث نشاط مريب من قبل إضافة PayPal مما قد يستدعي الالتفات إليه أو التحقيق بشأنه.'),
('PAYPAL_INVESTIGATE_PAYMENT', 'en_US', 'Unusual PayPal Activity', 'Open Journal Systems has encountered unusual activity relating to PayPal payment support for the journal {$contextName}. This activity may need further investigation or manual intervention.<br />\n                       <br />\nThis email was generated by Open Journal Systems\' PayPal plugin.<br />\n<br />\nFull post information for the request:<br />\n{$postInfo}<br />\n<br />\nAdditional information (if supplied):<br />\n{$additionalInfo}<br />\n<br />\nServer vars:<br />\n{$serverVars}<br />\n', 'This email template is used to notify a journal\'s primary contact that suspicious activity or activity requiring manual intervention was encountered by the PayPal plugin.'),
('PAYPAL_INVESTIGATE_PAYMENT', 'id_ID', ' Kegiatan pembayaran paypal yang tidak biasa ', ' IOJS telah menemukan kegiatan yang tidak biasa yang berhubungan dengan bantuan pembayaran paypal untuk jurnal {$contextName}. Kegiatan ini mungkin dibutuhkan investigasi yang lebih lanjut atau intervensi manual. Emai ini diciptakan oleh OJS PayPal plugin.<br />\nInformasi posting penuh untuk permohonan:<br />\n{$postInfo}<br />\nInformasi tambahan (jika tersedia):<br />\n{$additionalInfo}<br />\nVariasi Server :<br />\n{$serverVars}<br />\n', ' Email ini digunakan untuk memberitahukan jurnal kontak utama yang mempunyai kegiatan yang mencurigakan atau kegiatan yang membutuhkan intervensi manual yang ditemukan oleh plugin paypal.'),
('PUBLISH_NOTIFY', 'ar_IQ', 'صدر عدد جديد من المجلة', 'القراء الأعزة:<br />\n<br />\n{$contextName} أصدرت تواً عددها الأخير في موقعها {$contextUrl}. نحن ندعوكم إلى الاطلاع على جدول محتويات العدد، ومن ثم زيارة موقعنا لمتابعة المقالات العلمية التي تحوز على اهتمامكم.<br />\n<br />\nنشكر لكم إهتمامكم الدائم بنتاجاتنا؛<br />\n{$editorialContactSignature}', 'هذه الرسالة معنونة إلى القراء المسجلين عبر خدمة \"إشعار القراء\" الموجود رابطها في صفحة المحرر الرئيسية. هذه الخدمة تُعلم القراء بصدور العدد الجديد من المجلة وتدعوهم إلى زيارة موقعها على الرابط المرسل لهم.'),
('PUBLISH_NOTIFY', 'en_US', 'New Issue Published', 'Readers:<br />\n<br />\n{$contextName} has just published its latest issue at {$contextUrl}. We invite you to review the Table of Contents here and then visit our web site to review articles and items of interest.<br />\n<br />\nThanks for the continuing interest in our work,<br />\n{$editorialContactSignature}', 'This email is sent to registered readers via the \"Notify Users\" link in the Editor\'s User Home. It notifies readers of a new issue and invites them to visit the journal at a supplied URL.'),
('PUBLISH_NOTIFY', 'id_ID', 'Terbitan Baru', 'Readers:<br />\n<br />\n{$contextName} baru saja menerbitkan terbitan terbarunya di {$contextUrl}. Kami mengundang Anda untuk membaca Daftar Isi dan selanjutnya mengunjungi website kami untuk membaca artikel yang Anda minati.<br />\n<br />\nTerimakasih atas perhatian Anda,<br />\n{$editorialContactSignature}', 'Email ini dikirimkan ke pembaca terdaftar melalui tautan \"Beritahu Pengguna\" di Beranda Pengguna Editor. Email ini memberitahu pembaca tentang terbitan baru dan mengundang pembaca untuk mengunjungi jurnal melalui URL yang diberikan.'),
('REVIEWER_REGISTER', 'ar_IQ', 'التسجيل بصفة محكم في {$contextName}', 'في ضوء ما هو معروف عنك من خبرة، سنحت لنا الظروف بتسجيل اسمك ضمن قاعدة بياناتنا للمحكمين في موقع {$contextName}.  هذا الأمر لا يترتب عليه أي مستوى من الإلتزام من جهتك، بل ببساطة، يمكننا من التواصل معك بشأن إحتمالية تحكيم بعض طلبات النشر التي ترد مجلتنا. فيما لو تمت دعوتك مستقبلاً لغرض التحكيم، ستكون لك فرصة الاطلاع على عنوان المؤلَّف وملخصه (دون معرفة أي شيء يتعلق بالمؤلف نفسه)، وستكون صاحب الرأي بشأن الموافقة على تلك الدعوة أو الاعتذار عنها. فضلاً عن ذلك، بإمكانك دوماً المطالبة برفع اسمك من قائمة المحكمين المعتمدين لدى مجلتنا.<br />\n<br />\nنحن نزودك باسم الدخول وكلمة المرور الخاصين بك، واللذين يتيحان لك القيام بكل الفعاليات المتعلقة بعملك ضمن موقعنا. فقد ترغب، على سبيل المثال، بتحديث بيانات ملفك الشخصي وتضمين المجالات العلمية التي يهمك التحكيم فيها.<br />\n<br />\nاسم الدخول: {$username}<br />\nكلمة المرور: {$password}<br />\n<br />\nشكراً لك.<br />\n{$principalContactSignature}', 'هذه الرسالة معنونة إلى المحكمين المسجلين حديثاً للترحيب بهم في موقعنا ولتزويدهم بأسماء الدخول وكلمات المرور الخاصين بهم.'),
('REVIEWER_REGISTER', 'en_US', 'Registration as Reviewer with {$contextName}', 'In light of your expertise, we have taken the liberty of registering your name in the reviewer database for {$contextName}. This does not entail any form of commitment on your part, but simply enables us to approach you with a submission to possibly review. On being invited to review, you will have an opportunity to see the title and abstract of the paper in question, and you\'ll always be in a position to accept or decline the invitation. You can also ask at any point to have your name removed from this reviewer list.<br />\n<br />\nWe are providing you with a username and password, which is used in all interactions with the journal through its website. You may wish, for example, to update your profile, including your reviewing interests.<br />\n<br />\nUsername: {$username}<br />\nPassword: {$password}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered reviewer to welcome them to the system and provide them with a record of their username and password.'),
('REVIEWER_REGISTER', 'id_ID', 'Registrasi sebagai Reviewer di {$contextName}', 'Dengan mempertimbangkan keahlian Anda, kami memasukkan nama Anda dalam database reviewer di {$contextName}. Hal ini tidak bersifat mengikat, hanya sekedar memudahkan kami untuk mengundang Anda untuk melakukan review terhadap suatu naskah. Ketika memperoleh undangan untuk melakukan review suatu naskah, Anda dapat melihat judul dan abstrak naskah tersebut, dan Anda berhak menentukan apakah akan menerima atau menolak undangan tersebut. Anda juga dapat meminta dihapus dari daftar reviewer kapan saja Anda menghendaki.<br />\n<br />\nKami menyertakan nama pengguna dan sandi Anda, yang digunakan dalam semua interaksi dengan jurnal melalui website. Anda dapat melakukan update profil, termasuk minat review Anda.<br />\n<br />\nNama pengguna: {$username}<br />\nSandi: {$password}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke reviewer baru untuk menyambut dan memberikan informasi nama pengguna dan sandi.'),
('REVIEW_ACK', 'ar_IQ', 'إشعار تحكيم المؤلَّف', '{$reviewerName}:<br />\n<br />\nنشكر إكمالك لتحكيم المؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. إننا نقدر لك مشاركتك في ترصين الأعمال التي تنشرها مجلتنا.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يصدرها محرر القسم لتأكيد استلامه لتقييمات وملاحظات المحكم الكاملة، وليشكره على مساهمته في عملية التحكيم.'),
('REVIEW_ACK', 'en_US', 'Article Review Acknowledgement', '{$reviewerName}:<br />\n<br />\nThank you for completing the review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We appreciate your contribution to the quality of the work that we publish.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to confirm receipt of a completed review and thank the reviewer for their contributions.'),
('REVIEW_ACK', 'id_ID', 'Ucapan Terimakasih atas Review Artikel', '{$reviewerName}:<br />\n<br />\nTerimakasih telah menyelesaikan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami sangat menghargai kontribusi Anda terhadap kualitas karya yang kami publikasikan.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian untuk mengkonfirmasi penerimaan review dan mengucapkan terimakasih atas kontribusinya.'),
('REVIEW_CANCEL', 'ar_IQ', 'إلغاء إلتماس التحكيم', '{$reviewerName}:<br />\n<br />\nحالياً، قررنا إلغاء إلتماسنا لكم بتحكيم المؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. نحن نعتذر عن أي إزعاج قد يسببه ذلك لحضرتكم، ونأمل التواصل معكم مستقبلاً فيما يتعلق بتحكيم طلبات النشر المرسلة إلى هذه المجلة العلمية.<br />\n<br />\nإن كانت لديكم أي استفسارات، لطفاً، إتصلوا بي.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يعنونها محرر القسم إلى المحكم الذي بحيازته طلب تقديم قيد المعالجة لإبلاغه بانتفاء الحاجة إلى تقييمه.'),
('REVIEW_CANCEL', 'en_US', 'Request for Review Cancelled', '{$reviewerName}:<br />\n<br />\nWe have decided at this point to cancel our request for you to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We apologize for any inconvenience this may cause you and hope that we will be able to call on you to assist with this journal\'s review process in the future.<br />\n<br />\nIf you have any questions, please contact me.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by the Section Editor to a Reviewer who has a submission review in progress to notify them that the review has been cancelled.'),
('REVIEW_CANCEL', 'id_ID', 'Permohonan Review Dibatalkan', '{$reviewerName}:<br />\n<br />\nKami memutuskan untuk membatalkan permohonan review kami kepada Anda untuk naskah, &quot;{$submissionTitle},&quot; di {$contextName}. Kami mohon maaf sebesar-besarnya untuk ketidaknyamanan ini dan kami harap di masa mendatang Anda dapat membantu proses review di jurnal ini.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian kepada Reviewer yang sedang melakukan review untuk memberitahukan pembatalan review.'),
('REVIEW_CONFIRM', 'ar_IQ', 'قادر على التحكيم', 'أعزائي المحررون:<br />\n<br />\nأنا متمكن وراغب بتحكيم المؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. أشكر لكم إعتمادكم عليَّ، وأخطط لإنجاز التحكيم المطلوب حتى يوم استيجابه مني في {$reviewDueDate} إن لم يكن قبله.<br />\n<br />\n{$reviewerName}', 'هذه الرسالة يعنونها المحكم إلى محرر القسم استجابة لالتماس سابق أرسله له الأخير. المحكم يبلغ المحرر بقبوله للتحكيم المذكور خلال مدة الاستيجاب المطلوبة منه.'),
('REVIEW_CONFIRM', 'en_US', 'Able to Review', 'Editors:<br />\n<br />\nI am able and willing to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and I plan to have the review completed by its due date, {$reviewDueDate}, if not before.<br />\n<br />\n{$reviewerName}', 'This email is sent by a Reviewer to the Section Editor in response to a review request to notify the Section Editor that the review request has been accepted and will be completed by the specified date.'),
('REVIEW_CONFIRM', 'id_ID', 'Dapat Melakukan Review', 'Editor:<br />\n<br />\nSaya dapat dan bersedia melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Terimakasih telah mempercayakan kepada saya, dan saya berencana untuk menyelesaikan review ini sesuai tenggat, {$reviewDueDate}, atau sebelumnya.<br />\n<br />\n{$reviewerName}', 'Email ini dikirimkan Reviewer kepada Editor Bagian sebagai respon terhadap permohonan review untuk memberitahu Editor Bagian bahwa permohonan review diterima dan akan diselesaikan pada tanggal yang ditetapkan.'),
('REVIEW_DECLINE', 'ar_IQ', 'غير قادر على التحكيم', 'أعزائي المحررون:<br />\n<br />\nأخشى أنني حالياً غير متفرغ لتحكيم المؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. أشكر لكم إعتمادكم عليَّ وأرجو التواصل معي في وقت آخر.<br />\n<br />\n{$reviewerName}', 'هذه الرسالة يعنونها المحكم إلى محرر القسم استجابة لالتماس سابق أرسله له الأخير. المحكم يعتذر فيها عن قبول التحكيم للمؤلَّف المذكور.'),
('REVIEW_DECLINE', 'en_US', 'Unable to Review', 'Editors:<br />\n<br />\nI am afraid that at this time I am unable to review the submission, &quot;{$submissionTitle},&quot; for {$contextName}. Thank you for thinking of me, and another time feel free to call on me.<br />\n<br />\n{$reviewerName}', 'This email is sent by a Reviewer to the Section Editor in response to a review request to notify the Section Editor that the review request has been declined.');
INSERT INTO `email_templates_default_data` (`email_key`, `locale`, `subject`, `body`, `description`) VALUES
('REVIEW_DECLINE', 'id_ID', 'Tidak Dapat Melakukan Review', 'Editor:<br />\n<br />\nMohon maaf saat ini saya tidak dapat melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Terimakasih telah mempercayakann kepada saya, dan lain waktu silakan menghubungi saya lagi.<br />\n<br />\n{$reviewerName}', 'Email ini dikirimkan Reviewer kepada Editor Bagian sebagai respon terhadap permohonan review untuk memberitahu Editor Bagian bahwa permohonan review ditolak.'),
('REVIEW_REMIND', 'ar_IQ', 'تذكير لتحكيم طلب نشر', '{$reviewerName}:<br />\n<br />\nهذا مجرد تذكير لجنابكم الكريم بشأن تحكيمكم للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. نحن نأمل الحصول منكم على هذا التحكيم حتى يوم {$reviewDueDate}، ونرجو أن نتلقاه منكم حالماً تكونوا جاهزين لإرساله.<br />\n<br />\nإن لم يكن اسم الدخول وكلمة المرور متوفرين بين يديك لتتمكن من الولوج إلى موقع المجلة، بإمكانك اتباع الرابط أدناه لإعادة تعيين كلمة مرورك (التي عندها ستُرسل إليك مع اسم الدخول) عبر البريد الالكتروني. {$passwordResetUrl}<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nلطفاً، أكدوا لنا قدرتكم على إكمال هذه المساهمة الحيوية في أعمال مجلتنا. إنني أتطلع إلى ردكم في أقرب وقت.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يذكر بها محرر القسم محكماً قد استوجب إكمال تقييمه لمؤلَّف ما ولم يرسله/يرفعه إلى المجلة بعد.'),
('REVIEW_REMIND', 'en_US', 'Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to remind a reviewer that their review is due.'),
('REVIEW_REMIND', 'id_ID', 'Pengingat Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan akan sangat berbahagia untuk dapat menerimanya segera setelah Anda menyelesaikannya.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian untuk mengingatkan reviewer bahwa review telah mencapai tenggat.'),
('REVIEW_REMIND_AUTO', 'ar_IQ', 'تذكير آلي لتحكيم طلب نشر', '{$reviewerName}:<br />\n<br />\nهذا مجرد تذكير لجنابكم الكريم بشأن تحكيمكم للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. نحن نأمل الحصول منكم على هذا التحكيم حتى يوم {$reviewDueDate} وهذه الرسالة مولدة آلياً نظراً لتجاوز ذلك التاريخ. مع ذلك، يسرنا أن نتلقاه منكم حالماً تكونوا جاهزين لإرساله.<br />\n<br />\nإن لم يكن اسم الدخول وكلمة المرور متوفرين بين يديك لتتمكن من الولوج إلى موقع المجلة، بإمكانك اتباع الرابط أدناه لإعادة تعيين كلمة مرورك (التي عندها ستُرسل إليك مع اسم الدخول) عبر البريد الالكتروني. {$passwordResetUrl}<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nلطفاً، أكدوا لنا قدرتكم على إكمال هذه المساهمة الحيوية في أعمال مجلتنا. إنني أتطلع إلى ردكم في أقرب وقت.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يولدها الموقع آلياً عندما يتم تجاوز موعد تسليم التحكيم من قبل أحد المحكمين (أنظر خيارات التحكيم ضمن: الإعدادات، المخطط الانسيابي، التحكيم) وعندما يكون خيار الوصول المباشر للمحكم غير مفعل. المهام المجدول يجب أن تكون مفعلة ومهيئة (أنظر ملف تهيئة الموقع.'),
('REVIEW_REMIND_AUTO', 'en_US', 'Automated Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO', 'id_ID', 'Pengingat Otomatis Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut. Kami masih tetap menanti review tersebut segera setelah Anda dapat menyelesaikannya.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan otomatis saat tenggat waktu review terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO_ONECLICK', 'ar_IQ', 'Automated Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nهذا مجرد تذكير لجنابكم الكريم بشأن تحكيمكم للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}. نحن نأمل الحصول منكم على هذا التحكيم حتى يوم {$reviewDueDate}،وهذه الرسالة مولدة آلياً نظراً لتجاوز ذلك التاريخ. مع ذلك، يسرنا أن نتلقاه منكم حالماً تكونوا جاهزين لإرساله.<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nلطفاً، أكدوا لنا قدرتكم على إكمال هذه المساهمة الحيوية في أعمال مجلتنا. إنني أتطلع إلى ردكم في أقرب وقت.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يولدها الموقع آلياً عندما يتم تجاوز موعد تسليم التحكيم من قبل أحد المحكمين (أنظر خيارات التحكيم ضمن: الإعدادات، المخطط الانسيابي، التحكيم) وعندما يكون خيار الوصول المباشر للمحكم مفعل. المهام المجدول يجب أن تكون مفعلة ومهيئة (أنظر ملف تهيئة الموقع.'),
('REVIEW_REMIND_AUTO_ONECLICK', 'en_US', 'Automated Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and this email has been automatically generated and sent with the passing of that date. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent when a reviewer\'s due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REMIND_AUTO_ONECLICK', 'id_ID', 'Pengingat Review Naskah Otomatis', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. We were hoping to have this review by {$reviewDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut. We would still be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan otomatis saat tenggat waktu review terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer diaktifkan. Tugas terjadwal harus diaktifkan dan diatur (lihat file konfigurasi website)'),
('REVIEW_REMIND_ONECLICK', 'ar_IQ', 'تذكير لتحكيم طلب نشر', '{$reviewerName}:<br />\n<br />\nهذا مجرد تذكير لجنابكم الكريم بشأن تحكيمكم للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والمرسل إلى {$contextName}.  نحن نأمل الحصول منكم على هذا التحكيم حتى يوم {$reviewDueDate}، ونرجو أن نتلقاه منكم حالماً تكونوا جاهزين لإرساله.<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nلطفاً، أكدوا لنا قدرتكم على إكمال هذه المساهمة الحيوية في أعمال مجلتنا. إنني أتطلع إلى ردكم في أقرب وقت.<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة يذكر بها محرر القسم محكماً باستيجاب مهلة تقييمه لمؤلَّف ما.'),
('REVIEW_REMIND_ONECLICK', 'en_US', 'Submission Review Reminder', '{$reviewerName}:<br />\n<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have this review by {$reviewDueDate}, and would be pleased to receive it as soon as you are able to prepare it.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nPlease confirm your ability to complete this vital contribution to the work of the journal. I look forward to hearing from you.<br />\n<br />\n{$editorialContactSignature}', 'This email is sent by a Section Editor to remind a reviewer that their review is due.'),
('REVIEW_REMIND_ONECLICK', 'id_ID', 'Pengingat Review Naskah', '{$reviewerName}:<br />\n<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami mengharapkan telah menerima review ini pada {$reviewDueDate}, dan akan sangat berbahagia untuk dapat menerimanya segera setelah Anda menyelesaikannya.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nMohon konfirmasi kesanggupan Anda untuk menyelesaikan kontribusi penting ini.  Kami menunggu balasan Anda.<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirimkan Editor Bagian untuk mengingatkan reviewer bahwa review telah mencapai tenggat.'),
('REVIEW_REQUEST', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nكلي ثقة بأنك ستكون محكماً متميزاً للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والذي أُرسل طلب لنشره إلى {$contextName}. إن ملخص موضوعه مبين أدناه، وأنا أرجو أنك ستأخذ على عاتقك إنجاز هذه المهمة لمجلتنا.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة حتى يوم {$responseDueDate} لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه. الرابط هو: {$contextUrl}<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nإن لم يكن اسم الدخول وكلمة المرور متوفرين بين يديك لتتمكن من الولوج إلى موقع المجلة، بإمكانك اتباع الرابط أدناه لإعادة تعيين كلمة مرورك (التي عندها ستُرسل إليك مع اسم الدخول) عبر البريد الالكتروني. {$passwordResetUrl}<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يرسلها محرر قسم المجلة إلى أحد المحكمين ليلتمس منه قبول مهمة التحكيم أو رفضها. وهي توفر معلومات متعلقة بطلب النشر من عنوانه وملخصه، فضلاً عن موعد استيجاب التحكيم ورابط الدخول إلى الموقع للاطلاع على الطلب نفسه. هذه الرسالة تستعمل عندما يكون التحكيم في المجلة متبعاً لعملية التحكيم القياسية وذلك عبر تفعيله ضمن (خيارات التحكيم في: الإعدادات، المخطط الانسيابي، التحكيم). (بخلاف ذلك أنظر. REVIEW_REQUEST_ATTACHED.)'),
('REVIEW_REQUEST', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; which has been submitted to {$contextName}. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_ATTACHED.)'),
('REVIEW_REQUEST', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; yang diserahkan ke {$contextName}. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat di REVIEW_REQUEST_ATTACHED.)'),
('REVIEW_REQUEST_ATTACHED', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nكلي ثقة بأنك ستكون محكماً متميزاً للمؤلَّف, &quot;{$submissionTitle},&quot; وأنا أرجو أنك ستأخذ على عاتقك إنجاز هذه المهمة لمجلتنا. إن دليل عمل المحكم في مجلتنا مضمن في أدنى الرسالة، وإن طلب النشر المذكور مرفق بها. إن تحكيمك لهذا المؤلَّف مع توصياتك بشأنه ينبغي إرسالها لي عبر البريد الالكتروني قبل يوم {$reviewDueDate}.<br />\n<br />\nلطفاً، بين عبر ردك على هذه الرسالة حتى يوم {$responseDueDate} مدى قدرتك وقبولك القيام بهذا التحكيم.<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nدليل عمل المحكم<br />\n<br />\n{$reviewGuidelines}<br />\n', 'هذه الرسالة يرسلها محرر قسم المجلة إلى أحد المحكمين ليلتمس منه قبول مهمة التحكيم أو رفضها. وهي مرفقة بطلب النشر بشكل ملف. يتم استعمال هذه الرسالة عندما تكون عمية التحكيم مبنية على أساس التراسل عبر البريد الالكتروني خلال تهيئة إعدادات المجلة. (إنظر خيارات التحكيم ضمن: الإعدادات، المخطط الانسيابي، التحكيم). (بخلاف ذلك، أنظر REVIEW_REQUEST.)'),
('REVIEW_REQUEST_ATTACHED', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; and I am asking that you consider undertaking this important task for us. The Review Guidelines for this journal are appended below, and the submission is attached to this email. Your review of the submission, along with your recommendation, should be emailed to me by {$reviewDueDate}.<br />\n<br />\nPlease indicate in a return email by {$responseDueDate} whether you are able and willing to do the review.<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nReview Guidelines<br />\n<br />\n{$reviewGuidelines}<br />\n', 'This email is sent by the Section Editor to a Reviewer to request that they accept or decline the task of reviewing a submission. It includes the submission as an attachment. This message is used when the Email-Attachment Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST.)'),
('REVIEW_REQUEST_ATTACHED', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; dan kami mengharapkan Anda berkenan melakukan review. Panduan Review untuk jurnal ini disertakan di bawah, dan naskah telah dilampirkan bersama email ini. Review beserta rekomendasi Anda, kami harap telah diemailkan sebelum tenggat review {$reviewDueDate}.<br />\n<br />\nMohon sampaikan dalam email balasan sebelum {$responseDueDate} tentang kesediaan Anda untuk melakukan review.<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nPanduan Review<br />\n<br />\n{$reviewGuidelines}<br />\n', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email disertai naskah terlampir bersama email. Pesan ini digunakan saat Proses Review Lampiran Email dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST.)'),
('REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nالأمر يتعلق بالمؤلَّف الموسوم &quot;{$submissionTitle},&quot; الذي هو قيد المعالجة من قبل {$contextName}.<br />\n<br />\nبالنظر إلى التنقيحات المؤشرة على النسخة السابقة من المؤلَّف، قدم المؤلف/المؤلفون الآن نسخة منقحة من أوراقهم. نكون شاكرين لتعاونك معنا في تقييمها.<br />\n<br />\nإن دليل عمل المحكم في مجلتنا مضمن في أدنى الرسالة، وإن طلب النشر المذكور مرفق بها. إن تحكيمك لهذا المؤلَّف مع توصياتك بشأنه ينبغي إرسالها لي عبر البريد الالكتروني قبل يوم {$reviewDueDate}.<br />\n<br />\nلطفاً، بين عبر ردك على هذه الرسالة حتى يوم {$responseDueDate} مدى قدرتك وقبولك القيام بهذا التحكيم.<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nدليل عمل المحكم<br />\n<br />\n{$reviewGuidelines}<br />\n', 'هذه الرسالة يعنونها محرر القسم إلى المحكم ليلتمس منه قبول أو رفض مهمة تحكيم المؤلَّف لمرة أخرى. وهي مرفقة بطلب النشر بشكل ملف. يتم استعمال هذه الرسالة عندما تكون عمية التحكيم مبنية على أساس التراسل عبر البريد الالكتروني خلال تهيئة إعدادات المجلة ضمن: الإعدادات، المخطط الانسيابي، التحكيم. (بخلاف ذلك، أنظر REVIEW_REQUEST_SUBSEQUENT.)'),
('REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nThe Review Guidelines for this journal are appended below, and the submission is attached to this email. Your review of the submission, along with your recommendation, should be emailed to me by {$reviewDueDate}.<br />\n<br />\nPlease indicate in a return email by {$responseDueDate} whether you are able and willing to do the review.<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nReview Guidelines<br />\n<br />\n{$reviewGuidelines}<br />\n', 'This email is sent by the Section Editor to a Reviewer to request that they accept or decline the task of reviewing a submission for a second or greater round of review. It includes the submission as an attachment. This message is used when the Email-Attachment Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_SUBSEQUENT.)'),
('REVIEW_REQUEST_ATTACHED_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nPanduan Review untuk jurnal ini disertakan di bawah, dan naskah dilampirkan bersama email ini. Review dan rekomendasi Anda, mohon dapat diemailkan ke kami pada {$reviewDueDate}.<br />\n<br />\nMohon sampaikan di email balasan sebelum {$responseDueDate} tentang kesediaan Anda untuk melakukan review.<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n<br />\nPanduan Review<br />\n<br />\n{$reviewGuidelines}<br />\n', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya.  Email disertai naskah terlampir bersama email. Pesan ini digunakan saat Proses Review Lampiran Email dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST_SUBSEQUENT.)'),
('REVIEW_REQUEST_ONECLICK', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nكلي ثقة بأنك ستكون محكماً متميزاً للمؤلَّف الموسوم &quot;{$submissionTitle},&quot; والذي أُرسل طلب لنشره إلى {$contextName}. إن ملخص موضوعه مبين أدناه، وأنا أرجو أنك ستأخذ على عاتقك إنجاز هذه المهمة لمجلتنا.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة حتى يوم {$responseDueDate} لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه.<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يرسلها محرر قسم المجلة إلى أحد المحكمين ليلتمس منه قبول مهمة التحكيم أو رفضها. وهي توفر معلومات متعلقة بطلب النشر من عنوانه وملخصه، فضلاً عن موعد استيجاب التحكيم ورابط الدخول إلى الموقع للاطلاع على الطلب نفسه. هذه الرسالة تستعمل عندما يكون التحكيم في المجلة متبعاً لعملية التحكيم القياسية وذلك عبر تفعيله ضمن (خيارات التحكيم في: الإعدادات، المخطط الانسيابي، التحكيم)، مع تفعيل الوصول المباشر للمحكم أيضاً.'),
('REVIEW_REQUEST_ONECLICK', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript, &quot;{$submissionTitle},&quot; which has been submitted to {$contextName}. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review, and one-click reviewer access is enabled.'),
('REVIEW_REQUEST_ONECLICK', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah, &quot;{$submissionTitle},&quot; yang diserahkan ke {$contextName}. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer  untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review, dan akses one-click reviewer diaktifkan.'),
('REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nالأمر يتعلق بالمؤلَّف الموسوم &quot;{$submissionTitle},&quot; الذي هو قيد المعالجة من قبل {$contextName}.<br />\n<br />\nبالنظر إلى التنقيحات المؤشرة على النسخة السابقة من المؤلَّف، قدم المؤلف/المؤلفون الآن نسخة منقحة من أوراقهم. نكون شاكرين لتعاونك معنا في تقييمها.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة حتى يوم {$responseDueDate} لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه.<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يعنونها محرر القسم إلى المحكم ليلتمس منه قبول أو رفض مهمة تحكيم المؤلَّف لمرة أخرى. الرسالة تشير إلى عنوان المؤلَّف وملخصه وتاريخ استيجاب التحكيم كما تعطي رابط الوصول إلى المؤلَّف في موقع المجلة نفسه. هذه الرسالة تستعمل عندما يكون التحكيم في المجلة متبعاً لعملية التحكيم القياسية وذلك عبر تفعيله ضمن: الإعدادات، المخطط الانسيابي، التحكيم، مع تفعيل الوصول المباشر للمحكم أيضاً.'),
('REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission for a second or greater round of review. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review, and one-click reviewer access is enabled.'),
('REVIEW_REQUEST_ONECLICK_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda.<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review, dan akses one-click reviewer diaktifkan.'),
('REVIEW_REQUEST_REMIND_AUTO', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\nهذا تذكير لحضرتكم بشأن إلتماسنا السابق المتعلق بطلب النشر الموسوم &quot;{$submissionTitle},&quot; والمقدم إلى {$contextName}. كنا نأمل أن نحصل منكم على إجابة بموعد أقصاه {$responseDueDate}، وإن هذه الرسالة مصنعة آلياً ومرسلة عند انتهاء هذا الموعد.\n<br />\nكلي ثقة بأنك ستكون محكماً متميزاً للمؤلَّف. إن ملخص موضوعه مبين أدناه، وأنا أرجو أنك ستأخذ على عاتقك إنجاز هذه المهمة لمجلتنا.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه. الرابط هو: {$contextUrl}<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nإن لم يكن اسم الدخول وكلمة المرور متوفرين بين يديك لتتمكن من الولوج إلى موقع المجلة، بإمكانك اتباع الرابط أدناه لإعادة تعيين كلمة مرورك (التي عندها ستُرسل إليك مع اسم الدخول) عبر البريد الالكتروني. {$passwordResetUrl}<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يرسلها النظام آلياً عندما يتباطأ أحد المحكمين عن تقديم إجابة في الموعد المقرر (إنظر خيارات التحكيم ضمن: الإعدادات، المخطط الانسيابي، التحكيم) عندما تكون ميزة وصول المحكم بنقرة واحدة معطلة. يجب أن تكون المهام المجدولة ممكنة ومعدة (إنظر ملف إعدادات الموقع).'),
('REVIEW_REQUEST_REMIND_AUTO', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is disabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami berharap telah menerima respon Anda pada {$responseDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut.\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah tersebut. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dikirim otomatis saat tenggat konfirmasi terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer dinonaktifkan. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\nهذا تذكير لحضرتكم بشأن إلتماسنا السابق المتعلق بطلب النشر الموسوم &quot;{$submissionTitle},&quot; والمقدم إلى {$contextName}. كنا نأمل أن نحصل منكم على إجابة بموعد أقصاه {$responseDueDate}, وإن هذه الرسالة مصنعة آلياً ومرسلة عند انتهاء هذا الموعد.\n<br />\nكلي ثقة بأنك ستكون محكماً متميزاً للمؤلَّف. إن ملخص موضوعه مبين أدناه، وأنا أرجو أنك ستأخذ على عاتقك إنجاز هذه المهمة لمجلتنا.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه.<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يرسلها النظام آلياً عندما يتباطأ أحد المحكمين عن تقديم إجابة في الموعد المقرر (إنظر خيارات التحكيم ضمن: الإعدادات، المخطط الانسيابي، التحكيم) عندما تكون ميزة وصول المحكم بنقرة واحدة ممكنة. يجب أن تكون المهام المجدولة ممكنة ومعدة (إنظر ملف إعدادات الموقع).'),
('REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\nJust a gentle reminder of our request for your review of the submission, &quot;{$submissionTitle},&quot; for {$contextName}. We were hoping to have your response by {$responseDueDate}, and this email has been automatically generated and sent with the passing of that date.\n<br />\nI believe that you would serve as an excellent reviewer of the manuscript. The submission\'s abstract is inserted below, and I hope that you will consider undertaking this important task for us.<br />\n<br />\nPlease log into the journal web site to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation.<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email is automatically sent when a reviewer\'s confirmation due date elapses (see Review Options under Settings > Workflow > Review) and one-click reviewer access is enabled. Scheduled tasks must be enabled and configured (see the site configuration file).'),
('REVIEW_REQUEST_REMIND_AUTO_ONECLICK', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\nSekedar mengingatkan tentang permohonan kami untuk melakukan review terhadap naskah, &quot;{$submissionTitle},&quot; untuk {$contextName}. Kami berharap telah menerima respon Anda pada {$responseDueDate}, dan email ini dikirim secara otomatis seiring terlewatinya tanggal tersebut.\n<br />\nKami memandang bahwa Anda merupakan reviewer yang tepat untuk naskah tersebut. Abstrak naskah tersebut disertakan di bawah ini, dan kami berharap Anda berkenan melakukannya.<br />\n<br />\nSilakan login ke website jurnal untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dikirim otomatis saat tenggat konfirmasi reviewer terlewati (lihat Opsi Review di menu Pengaturan > Alur Kerja > Review) dan akses one-click reviewer diaktifkan. Tugas Terjadwal harus diaktifkan dan diatur (lihat file konfigurasi website).'),
('REVIEW_REQUEST_SUBSEQUENT', 'ar_IQ', 'إلتماس تحكيم مؤلَّف', '{$reviewerName}:<br />\n<br />\nالأمر يتعلق بالمؤلَّف الموسوم &quot;{$submissionTitle},&quot; الذي هو قيد المعالجة من قبل {$contextName}.<br />\n<br />\nبالنظر إلى التنقيحات المؤشرة على النسخة السابقة من المؤلَّف، قدم المؤلف/المؤلفون الآن نسخة منقحة من أوراقهم. نكون شاكرين لتعاونك معنا في تقييمها.<br />\n<br />\nلطفاً، أدخل إلى موقع المجلة حتى يوم {$responseDueDate} لتبين لنا فيما لو أنك موافق على هذا التحكيم أم لا، كما يمكنك أيضاً عبر الرابط أدناه الوصول إلى متعلقات طلب النشر المذكور، وتثبيت تقييمك له وملاحظاتك بشأنه. الرابط هو: {$contextUrl}<br />\n<br />\nأما التحكيم نفسه، فإن موعده النهائي سيكون: {$reviewDueDate}.<br />\n<br />\nإن لم يكن اسم الدخول وكلمة المرور متوفرين بين يديك لتتمكن من الولوج إلى موقع المجلة، بإمكانك اتباع الرابط أدناه لإعادة تعيين كلمة مرورك (التي عندها ستُرسل إليك مع اسم الدخول) عبر البريد الالكتروني. {$passwordResetUrl}<br />\n<br />\nرابط الطلب: {$submissionReviewUrl}<br />\n<br />\nنشكر لك اهتمامك بالتماسنا هذا.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'هذه الرسالة يعنونها محرر القسم إلى المحكم ليلتمس منه قبول أو رفض مهمة تحكيم المؤلَّف لمرة أخرى. الرسالة تشير إلى عنوان المؤلَّف وملخصه وتاريخ استيجاب التحكيم كما تعطي رابط الوصول إلى المؤلَّف في موقع المجلة نفسه. هذه الرسالة تستعمل عندما يكون التحكيم في المجلة متبعاً لعملية التحكيم القياسية وذلك عبر تفعيله ضمن: الإعدادات، المخطط الانسيابي، التحكيم. (بخلاف ذلك، أنظر REVIEW_REQUEST_ATTACHED_SUBSEQUENT.)'),
('REVIEW_REQUEST_SUBSEQUENT', 'en_US', 'Article Review Request', '{$reviewerName}:<br />\n<br />\nThis regards the manuscript &quot;{$submissionTitle},&quot; which is under consideration by {$contextName}.<br />\n<br />\nFollowing the review of the previous version of the manuscript, the authors have now submitted a revised version of their paper. We would appreciate it if you could help evaluate it.<br />\n<br />\nPlease log into the journal web site by {$responseDueDate} to indicate whether you will undertake the review or not, as well as to access the submission and to record your review and recommendation. The web site is {$contextUrl}<br />\n<br />\nThe review itself is due {$reviewDueDate}.<br />\n<br />\nIf you do not have your username and password for the journal\'s web site, you can use this link to reset your password (which will then be emailed to you along with your username). {$passwordResetUrl}<br />\n<br />\nSubmission URL: {$submissionReviewUrl}<br />\n<br />\nThank you for considering this request.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'This email from the Section Editor to a Reviewer requests that the reviewer accept or decline the task of reviewing a submission for a second or greater round of review. It provides information about the submission such as the title and abstract, a review due date, and how to access the submission itself. This message is used when the Standard Review Process is selected in Management > Settings > Workflow > Review. (Otherwise see REVIEW_REQUEST_ATTACHED_SUBSEQUENT.)'),
('REVIEW_REQUEST_SUBSEQUENT', 'id_ID', 'Permohonan Review Artikel', '{$reviewerName}:<br />\n<br />\nEmail ini terkait naskah &quot;{$submissionTitle},&quot; yang sedang dalam pertimbangan di {$contextName}.<br />\n<br />\nSetelah review untuk versi naskah sebelumnya, penulis saat ini telah menyerahkan versi revisi naskahnya.  Kami berharap Anda dapat membantu mengevaluasinya.<br />\n<br />\nSilakan login ke website jurnal sebelum {$responseDueDate} untuk memberitahukan apakah Anda bersedia melakukan review atau tidak, juga untuk mengakses naskah dan menyerahkan review beserta rekomendasi Anda. Website jurnal adalah {$contextUrl}<br />\n<br />\nTenggat review ini sendiri adalah {$reviewDueDate}.<br />\n<br />\nJika Anda tidak memiliki nama pengguna dan sandi untuk website jurnal ini, Anda dapat menggunakan tautan berikut untuk mereset sandi Anda (yang selanjutnya akan diemailkan kepada Anda bersama nama pengguna Anda). {$passwordResetUrl}<br />\n<br />\nURL Naskah: {$submissionReviewUrl}<br />\n<br />\nTerimakasih atas perhatiannya.<br />\n<br />\n{$editorialContactSignature}<br />\n<br />\n&quot;{$submissionTitle}&quot;<br />\n<br />\n{$submissionAbstract}', 'Email ini dari Editor Bagian kepada Reviewer untuk meminta reviewer memberitahukan apakah menerima atau menolak permohonan review untuk ronde kedua atau berikutnya. Email ini memberikan informasi tentang naskah meliputi judul dan abstrak, tenggat review, dan cara mengakses naskah. Pesan ini digunakan saat Proses Review Standar dipilih di Manajemen > Pengaturan > Alur Kerja > Review. (Selain itu lihat REVIEW_REQUEST_ATTACHED_SUBSEQUENT.)'),
('REVISED_VERSION_NOTIFY', 'ar_IQ', 'تم رفع نسخة منقحة', 'أعزائي المحررون:<br />\n<br />\nلقد تم رفع نسخة منقحة من المؤلَّف الموسوم &quot;{$submissionTitle}&quot; من قبل المؤلف {$authorName}.<br />\n<br />\nرابط الطلب: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}', 'هذه الرسالة تُرسل تلقائياً إلى المحرر المعني عندما يقوم المؤلف برفع نسخة منقحة من مؤلَّفه إلى الموقعe.'),
('REVISED_VERSION_NOTIFY', 'en_US', 'Revised Version Uploaded', 'Editors:<br />\n<br />\nA revised version of &quot;{$submissionTitle}&quot; has been uploaded by the author {$authorName}.<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}', 'This email is automatically sent to the assigned editor when author uploads a revised version of an article.'),
('REVISED_VERSION_NOTIFY', 'id_ID', 'Versi Revisi telah Diunggah', 'Editor:<br />\n<br />\nVersi revisi dari &quot;{$submissionTitle}&quot; telah diunggah oleh penulis {$authorName}.<br />\n<br />\nURL naskah: {$submissionUrl}<br />\n<br />\n{$editorialContactSignature}', 'Email ini dikirim secara otomatis ke editor yang ditugaskan saat penulis mengunggah naskah versi revisi.'),
('SUBMISSION_ACK', 'ar_IQ', 'إشعار طلب التقديم', '{$authorName}:<br />\n<br />\nشكراً على تقديمك المؤلَّف &quot;{$submissionTitle}&quot; إلى {$contextName}. عبر نظام إدارة المجلة هذا الذي نستعمله، سيكون بإمكانك متابعة مراحل معالجة طلبك عبر ولوجك إلى موقع المجلة باستعمال الرابط:<br />\n<br />\nرابط المؤلَّف: {$submissionUrl}<br />\nاسم الدخول: {$authorUsername}<br />\n<br />\nإن كان لديك أي استفسارات، لطفاً تواصل معي. شكراً على اعتبارك هذه المجلة جديرة بعملك.<br />\n<br />\n{$editorialContactSignature}', 'عند تمكين هذه الرسالة، سيتم إرسالها تلقائياً للمؤلف بمجرد إكماله خطوات تقديم طلب النشر إلى المجلة. يتعرف من خلالها عن إمكانية متابعة طلبه عبر موقع المجلة كما يشكره على تقديم طلبه لها.'),
('SUBMISSION_ACK', 'en_US', 'Submission Acknowledgement', '{$authorName}:<br />\n<br />\nThank you for submitting the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. With the online journal management system that we are using, you will be able to track its progress through the editorial process by logging in to the journal web site:<br />\n<br />\nSubmission URL: {$submissionUrl}<br />\nUsername: {$authorUsername}<br />\n<br />\nIf you have any questions, please contact me. Thank you for considering this journal as a venue for your work.<br />\n<br />\n{$editorialContactSignature}', 'This email, when enabled, is automatically sent to an author when he or she completes the process of submitting a manuscript to the journal. It provides information about tracking the submission through the process and thanks the author for the submission.'),
('SUBMISSION_ACK', 'id_ID', 'Ucapan Terimakasih atas Penyerahan Naskah', '{$authorName}:<br />\n<br />\nTerimakasih telah menyerahkan naskah, &quot;{$submissionTitle}&quot; ke {$contextName}. Dengan sistem manajemenn jurnal online yang kami gunakan, Anda dapat memantau kemajuan proses editorial naskah Anda melalui:<br />\n<br />\nURL Naskah: {$submissionUrl}<br />\nNama pengguna: {$authorUsername}<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami. Terimakasih telah mempercayakan publikasi karya Anda di jurnal kami.<br />\n<br />\n{$editorialContactSignature}', 'Email ini, jika diaktifkan, secara otomatis dikirimkan ke penulis saat menyelesaikan proses penyerahan naskah ke jurnal. Email ini memberikan informasi mengenai pemantauan kemajuan proses editorial dan mengucapkan terimakasih atas naskah yang dikirimkan.'),
('SUBMISSION_ACK_NOT_USER', 'ar_IQ', 'إشعار طلب التقديم', 'مرحباً، <br />\n<br />\nإن {$submitterName} قد تقدم بطلب لنشر المؤلَّف &quot;{$submissionTitle}&quot; إلى {$contextName}. <br />\n<br />\nإن كان لديك أي استفسارات، لطفاً تواصل معي. شكراً على اعتبارك هذه المجلة جديرة بعملك.<br />\n<br />\n{$editorialContactSignature}', 'عند تمكين هذه الرسالة، سيتم إرسالها تلقائياً إلى المؤلفين الآخرين الذين لا يملكون حسابات في نظام المجلات المفتوحة المعني خلال مرحلة تقديم الطلب.'),
('SUBMISSION_ACK_NOT_USER', 'en_US', 'Submission Acknowledgement', 'Hello,<br />\n<br />\n{$submitterName} has submitted the manuscript, &quot;{$submissionTitle}&quot; to {$contextName}. <br />\n<br />\nIf you have any questions, please contact me. Thank you for considering this journal as a venue for your work.<br />\n<br />\n{$editorialContactSignature}', 'This email, when enabled, is automatically sent to the other authors who are not users within OJS specified during the submission process.'),
('SUBMISSION_ACK_NOT_USER', 'id_ID', 'Pemberitahuan Penyerahan Naskah', 'Hello,<br />\n<br />\n{$submitterName} telah menyerahkan naskah, &quot;{$submissionTitle}&quot; ke {$contextName}. <br />\n<br />\nJika ada pertanyaan, silakan hubungi kami. Terimakasih telah mempercayakan publikasi karya Anda di jurnal kami.<br />\n<br />\n{$editorialContactSignature}', 'Email ini, jika diaktifkan, secara otomatis dikirimkan ke penulis saat menyelesaikan proses penyerahan naskah ke jurnal. Email ini memberikan informasi mengenai pemantauan kemajuan proses editorial dan mengucapkan terimakasih atas naskah yang dikirimkan.');
INSERT INTO `email_templates_default_data` (`email_key`, `locale`, `subject`, `body`, `description`) VALUES
('SUBSCRIPTION_AFTER_EXPIRY', 'ar_IQ', 'إنتهت صلاحية الإشتراك', '{$subscriberName}:<br />\n<br />\nإن مدة إشتراكك في {$contextName} قد انتهت.<br />\n<br />\n{$subscriptionType}<br />\nتاريخ إنتهاء صلاحيته: {$expiryDate}<br />\n<br />\nلتجديد إشتراكك، لطفاً أدخل موقع المجلة. بإمكانك الولوج إلى النظام مستعملاً اسم الدخول: &quot;{$username}&quot;.<br />\n<br />\nإن كانت لديك أية استفسارات، فلا تتردد في مراسلتي.<br />\n<br />\n{$subscriptionContactSignature}', 'هذه الرسالة تُعلم مشتركاً بانتهاء فترة إشتراكه في المجلة، وهي تقدم له رابط الدخول إلى الموقع مع تعليمات الوصول.'),
('SUBSCRIPTION_AFTER_EXPIRY', 'en_US', 'Subscription Expired', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription has expired.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo renew your subscription, please go to the journal website. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription has expired. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_AFTER_EXPIRY', 'id_ID', 'Langganan Berakhir', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda telah berakhir.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk memperbaharui langganan Anda, silakan kunjungi website jurnal.  Anda dapat login dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya telah berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_AFTER_EXPIRY_LAST', 'ar_IQ', 'إنتهت صلاحية الإشتراك - التذكير الأخير', '{$subscriberName}:<br />\n<br />\nإن مدة إشتراكك في {$contextName} قد انتهت.<br />\nلطفاً، نشيرك إلى أن هذه الرسالة هي بمثابة التذكير الأخير بشأنه.<br />\n<br />\n{$subscriptionType}<br />\nتاريخ إنتهاء صلاحيته: {$expiryDate}<br />\n<br />\nلتجديد إشتراكك، لطفاً أدخل موقع المجلة. بإمكانك الولوج إلى النظام مستعملاً اسم الدخول: &quot;{$username}&quot;.<br />\n<br />\nإن كانت لديك أية استفسارات، فلا تتردد في مراسلتي.<br />\n<br />\n{$subscriptionContactSignature}', 'هذه الرسالة تُعلم مشتركاً بانتهاء فترة إشتراكه في المجلة، وهي تقدم له رابط الدخول إلى الموقع مع تعليمات الوصول.'),
('SUBSCRIPTION_AFTER_EXPIRY_LAST', 'en_US', 'Subscription Expired - Final Reminder', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription has expired.<br />\nPlease note that this is the final reminder that will be emailed to you.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo renew your subscription, please go to the journal website. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription has expired. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_AFTER_EXPIRY_LAST', 'id_ID', 'Langganan Berakhir - Pengingat Terakhir', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda telah berakhir.<br />\nIni adalah pengingat terakhir yang diemailkan kepada Anda.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk memperbaharui langganan Anda, silakan kunjungi website jurnal.  Anda dapat login dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya telah berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_BEFORE_EXPIRY', 'ar_IQ', 'إشعار بانتهاء صلاحية الإشتراك', '{$subscriberName}:<br />\n<br />\nإن إشتراكك في {$contextName} على وشك الإنتهاء.<br />\n<br />\n{$subscriptionType}<br />\nتاريخ إنتهاء صلاحيته: {$expiryDate}<br />\n<br />\nلضمان استمرارية وصولك إلى هذه المجلة، لطفاً أدخل موقعها وجدد إشتراكك. بإمكانك الولوج إلى النظام مستعملاً اسم الدخول: &quot;{$username}&quot;.<br />\n<br />\nإن كانت لديك أية استفسارات، فلا تتردد في مراسلتي.<br />\n<br />\n{$subscriptionContactSignature}', 'هذه الرسالة تُبلغ مشتركاً بأن مدة إشتراكه على وشك النفاد، وهي تقدم له رابط الدخول إلى الموقع مع تعليمات الوصول.'),
('SUBSCRIPTION_BEFORE_EXPIRY', 'en_US', 'Notice of Subscription Expiry', '{$subscriberName}:<br />\n<br />\nYour {$contextName} subscription is about to expire.<br />\n<br />\n{$subscriptionType}<br />\nExpiry date: {$expiryDate}<br />\n<br />\nTo ensure the continuity of your access to this journal, please go to the journal website and renew your subscription. You are able to log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a subscriber that their subscription will soon expire. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_BEFORE_EXPIRY', 'id_ID', 'Pemberitahuan Tanggal Berakhir Langganan', '{$subscriberName}:<br />\n<br />\nLangganan {$contextName} Anda hampir berakhir.<br />\n<br />\n{$subscriptionType}<br />\nTanggal berakhir: {$expiryDate}<br />\n<br />\nUntuk terus memperoleh akses ke jurnal ini, silakan kunjungi website jurnal dan perbaharui langganan Anda. Anda dapat login menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu seorang pelanggan bahwa langganannya hampir berakhir.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_NOTIFY', 'ar_IQ', 'إشعار الإشتراك', '{$subscriberName}:<br />\n<br />\nلقد حزت على الإشتراك في موقع مجلتنا على الإنترنت والتي تحمل اسم {$contextName}، وإن هذا الإشتراك هو من نوع:<br />\n<br />\n{$subscriptionType}<br />\n<br />\nللوصول إلى المحتوى المخصص للمشتركين حصراً، ببساطة قم بالدخول إلى الموقع مستعملاً اسم الدخول: &quot;{$username}&quot;.<br />\n<br />\nبمجرد دخولك إلى النظام، يمكنك تعديل تفاصيل ملفك التعريفي وكلمة مرورك وفي أي وقت تشاء.<br />\n<br />\nلاحظ لطفاً أنك إن كنت حاصلاً على اشتراك مؤسساتي، فلا داعي لأفراد مؤسستك لتسجيل دخولهم إلى الموقع، طالماً أن محتوى طلبات الإشتراك ستكون عندها مصرحاً لها تلقائياً في النظام.<br />\n<br />\nإن كانت لديك أية استفسارات، فلا تتردد في مراسلتي.<br />\n<br />\n{$subscriptionContactSignature}', 'هذه الرسالة تُعلم قارئاً مسجلاً بأن مدير المجلة قد أنشأ له اشتراكاً، وهي تتضمن رابط المجلة مع تعليمات الوصول إليها.'),
('SUBSCRIPTION_NOTIFY', 'en_US', 'Subscription Notification', '{$subscriberName}:<br />\n<br />\nYou have now been registered as a subscriber in our online journal management system for {$contextName}, with the following subscription:<br />\n<br />\n{$subscriptionType}<br />\n<br />\nTo access content that is available only to subscribers, simply log in to the system with your username, &quot;{$username}&quot;.<br />\n<br />\nOnce you have logged in to the system you can change your profile details and password at any point.<br />\n<br />\nPlease note that if you have an institutional subscription, there is no need for users at your institution to log in, since requests for subscription content will be automatically authenticated by the system.<br />\n<br />\nIf you have any questions, please feel free to contact me.<br />\n<br />\n{$subscriptionContactSignature}', 'This email notifies a registered reader that the Manager has created a subscription for them. It provides the journal\'s URL along with instructions for access.'),
('SUBSCRIPTION_NOTIFY', 'id_ID', 'Pemberitahuan Langganan', '{$subscriberName}:<br />\n<br />\nAnda sekarang telah terdaftar sebagai pelanggan di sistem manajemen jurnal online kami, {$contextName}, dengan jenis langganan:<br />\n<br />\n{$subscriptionType}<br />\n<br />\nUntuk mengakses konten yang hanya tersedia bagi pelanggan, silakan login ke sistem dengan menggunakan nama pengguna Anda, &quot;{$username}&quot;.<br />\n<br />\nSetelah login, Anda dapat mengubah detail profil Anda dan sandi Anda kapanpun Anda kehendaki.<br />\n<br />\nJika Anda memiliki langganan institusi, pengguna di institusi Anda tidak perlu login karena semua akses secara otomatis diotentikasi oleh sistem.<br />\n<br />\nJika ada pertanyaan, silakan hubungi kami.<br />\n<br />\n{$subscriptionContactSignature}', 'Email ini memberitahu pembaca terdaftar bahwa Manajer telah membuat akses langganan untuk mereka.  Email ini memberikan URL jurnal beserta petunjuk aksesnya.'),
('SUBSCRIPTION_PURCHASE_INDL', 'ar_IQ', 'شراء إشتراك: منفرد', 'لقد تم شراء إشتراك منفرد في المجلة {$contextName} عبر الإنترنت وبالتفاصيل أدناه:<br />\n<br />\nنوع الإشنراك:<br />\n{$subscriptionType}<br />\n<br />\nالمستخدم:<br />\n{$userDetails}<br />\n<br />\nمعلومات العضوية (إن وجدت):<br />\n{$membership}<br />\n<br />\nلعرض أو تعديل هذا الاشتراك، لطفاً استعمل الرابط الآتي:<br />\n<br />\nرابط الاشتراك: {$subscriptionUrl}<br />\n', 'هذه الرسالة تُشعر مدير الإشتراكات بشراء إشتراك منفرد عبر الإنترنت. إنها تقدم معلومات إجمالية عن الاشتراك مع رابط الوصول السريع إليه.'),
('SUBSCRIPTION_PURCHASE_INDL', 'en_US', 'Subscription Purchase: Individual', 'An individual subscription has been purchased online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nUser:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an individual subscription has been purchased online. It provides summary information about the subscription and a quick access link to the purchased subscription.'),
('SUBSCRIPTION_PURCHASE_INDL', 'id_ID', 'Pembelian Langganan: Individu', 'Sebuah langganan individu telah dibeli online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nPengguna:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan individu telah dibeli online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang dibeli.'),
('SUBSCRIPTION_PURCHASE_INSTL', 'ar_IQ', 'شراء إشتراك: مؤسساتي', 'لقد تم شراء إشتراك مؤسساتي في المجلة {$contextName} عبر الإنترنت وبالتفاصيل أدناه، ولتفعيل هذا الإشتراك، لطفاً استعمل رابط الاشتراك وحول حالته إلى فعّال.<br />\n<br />\nنوع الإشنراك:<br />\n{$subscriptionType}<br />\n<br />\nالمؤسسة:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nالمدى (إن توفر):<br />\n{$domain}<br />\n<br />\nمدى عناوين IP (إن توفرت):<br />\n{$ipRanges}<br />\n<br />\nالشخص المعني:<br />\n{$userDetails}<br />\n<br />\nمعلومات العضوية (إن وجدت):<br />\n{$membership}<br />\n<br />\nلعرض أو تعديل هذا الاشتراك، لطفاً استعمل الرابط الآتي:<br />\n<br />\nرابط الاشتراك: {$subscriptionUrl}<br />\n', 'هذه الرسالة تُشعر مدير الإشتراكات بشراء إشتراك منفرد عبر الإنترنت. إنها تقدم معلومات إجمالية عن الاشتراك مع رابط الوصول السريع إليه.'),
('SUBSCRIPTION_PURCHASE_INSTL', 'en_US', 'Subscription Purchase: Institutional', 'An institutional subscription has been purchased online for {$contextName} with the following details. To activate this subscription, please use the provided Subscription URL and set the subscription status to \'Active\'.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nInstitution:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (if provided):<br />\n{$domain}<br />\n<br />\nIP Ranges (if provided):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an institutional subscription has been purchased online. It provides summary information about the subscription and a quick access link to the purchased subscription.'),
('SUBSCRIPTION_PURCHASE_INSTL', 'id_ID', 'Pembelian Langganan: Institusi', 'Sebuah langganan institusi telah dibeli online untuk {$contextName} dengan rincian berikut. Untuk mengaktifkan langganan ini, silakan gunakan URL Langganan dan jadikan status langganan ke \'Aktif\'.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nInstitusi:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (jika ada):<br />\n{$domain}<br />\n<br />\nIP Ranges (jika ada):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan institusi telah dibeli online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang dibeli.'),
('SUBSCRIPTION_RENEW_INDL', 'ar_IQ', 'تجديد إشتراك: منفرد', 'لقد تم تجديد إشتراك منفرد في المجلة {$contextName} عبر الإنترنت وبالتفاصيل أدناه:<br />\n<br />\nنوع الإشنراك:<br />\n{$subscriptionType}<br />\n<br />\nالمستخدم:<br />\n{$userDetails}<br />\n<br />\nمعلومات العضوية (إن وجدت):<br />\n{$membership}<br />\n<br />\nلعرض أو تعديل هذا الاشتراك، لطفاً استعمل الرابط الآتي:<br />\n<br />\nرابط الاشتراك: {$subscriptionUrl}<br />\n', 'هذه الرسالة تُشعر مدير الإشتراكات بتجديد إشتراك منفرد عبر الإنترنت. إنها تقدم معلومات إجمالية عن الاشتراك مع رابط الوصول السريع إليه.'),
('SUBSCRIPTION_RENEW_INDL', 'en_US', 'Subscription Renewal: Individual', 'An individual subscription has been renewed online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nUser:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an individual subscription has been renewed online. It provides summary information about the subscription and a quick access link to the renewed subscription.'),
('SUBSCRIPTION_RENEW_INDL', 'id_ID', 'Pembaharuan Langganan: Individu', 'Sebuah langganan individu telah diperbaharui online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nPengguna:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan individu telah diperbaharui online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang diperbaharui.'),
('SUBSCRIPTION_RENEW_INSTL', 'ar_IQ', 'تجديد إشتراك: مؤسساتي', 'لقد تم تجديد إشتراك مؤسساتي في المجلة {$contextName} عبر الإنترنت وبالتفاصيل أدناه:<br />\n<br />\nنوع الإشنراك:<br />\n{$subscriptionType}<br />\n<br />\nالمؤسسة:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nالمدى (إن توفر):<br />\n{$domain}<br />\n<br />\nمدى عناوين IP (إن توفرت):<br />\n{$ipRanges}<br />\n<br />\nالشخص المعني:<br />\n{$userDetails}<br />\n<br />\nمعلومات العضوية (إن وجدت):<br />\n{$membership}<br />\n<br />\nلعرض أو تعديل هذا الاشتراك، لطفاً استعمل الرابط الآتي:<br />\n<br />\nرابط الاشتراك: {$subscriptionUrl}<br />\n', 'هذه الرسالة تُشعر مدير الإشتراكات بتجديد إشتراك مؤسساتي عبر الإنترنت. إنها تقدم معلومات إجمالية عن الاشتراك مع رابط الوصول السريع إليه.'),
('SUBSCRIPTION_RENEW_INSTL', 'en_US', 'Subscription Renewal: Institutional', 'An institutional subscription has been renewed online for {$contextName} with the following details.<br />\n<br />\nSubscription Type:<br />\n{$subscriptionType}<br />\n<br />\nInstitution:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (if provided):<br />\n{$domain}<br />\n<br />\nIP Ranges (if provided):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nMembership Information (if provided):<br />\n{$membership}<br />\n<br />\nTo view or edit this subscription, please use the following URL.<br />\n<br />\nSubscription URL: {$subscriptionUrl}<br />\n', 'This email notifies the Subscription Manager that an institutional subscription has been renewed online. It provides summary information about the subscription and a quick access link to the renewed subscription.'),
('SUBSCRIPTION_RENEW_INSTL', 'id_ID', 'Pembaharuan Langganan: Institusi', 'Sebuah langganan institusi telah diperbaharui online untuk {$contextName} dengan rincian berikut.<br />\n<br />\nJenis Langganan:<br />\n{$subscriptionType}<br />\n<br />\nInstitusi:<br />\n{$institutionName}<br />\n{$institutionMailingAddress}<br />\n<br />\nDomain (jika ada):<br />\n{$domain}<br />\n<br />\nIP Ranges (jika ada):<br />\n{$ipRanges}<br />\n<br />\nContact Person:<br />\n{$userDetails}<br />\n<br />\nInformasi Keanggotaan (jika ada):<br />\n{$membership}<br />\n<br />\nUntuk melihat atau mengubah langganan ini, silakan gunakan URL berikut ini.<br />\n<br />\nURL Langganan: {$subscriptionUrl}<br />\n', 'Email ini memberitahu Manajer Langganan bahwa sebuah langganan institusi telah diperbaharui online.  Email ini memberikan ringkasan informasi tentang langganan tersebut beserta tautan untuk mengakses langganan yang diperbaharui.'),
('USER_REGISTER', 'ar_IQ', 'التسجيل في المجلة', '{$userFullName}<br />\n<br />\nأنت الآن مسجل كمستخدم ضمن {$contextName}. قمنا بتضمين اسم الدخول وكلمة المرور في هذه الرسالة، كونهما ضروريان لمتابعة كل الأعمال والوظائف في موقع المجلة. يمكنك المطالبة في أي وقت بإزالة اسمك من قائمة المستخدمين المسجلين عبر مراسلتي.<br />\n<br />\nاسم الدخول: {$username}<br />\nكلمة المرور: {$password}<br />\n<br />\nشكراً لك.<br />\n{$principalContactSignature}', 'هذه الرسالة معنونة إلى المستخدمين المسجلين حديثاً للترحيب بهم في نظام موقع المجلة، ولتزويدهم باسم الدخول وكلمة المرور الخاصين بالموقع.'),
('USER_REGISTER', 'en_US', 'Journal Registration', '{$userFullName}<br />\n<br />\nYou have now been registered as a user with {$contextName}. We have included your username and password in this email, which are needed for all work with this journal through its website. At any point, you can ask to be removed from the journal\'s list of users by contacting me.<br />\n<br />\nUsername: {$username}<br />\nPassword: {$password}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered user to welcome them to the system and provide them with a record of their username and password.'),
('USER_REGISTER', 'id_ID', 'Registrasi Jurnal', '{$userFullName}<br />\n<br />\nAnda sekarang telah terdaftar sebagai pengguna di {$contextName}.  Kami sertakan nama pengguna dan sandi Anda di email ini, keduanya diperlukan untuk semua kegiatan melalui website jurnal ini. Anda dapat keluar dari daftar pengguna jurnal kapan saja dengan menghubungi kami.<br />\n<br />\nNama pengguna: {$username}<br />\nSandi: {$password}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirimkan ke pengguna baru untuk menyambut dan memberikan informasi nama pengguna dan sandi.'),
('USER_VALIDATE', 'ar_IQ', 'تأكيد صحة حسابك', '{$userFullName}<br />\n<br />\nقمت بإنشاء حساب في {$contextName}، لكن قبل شروعك باستعماله، عليك تأكيد عنوان بريدك الالكتروني، وكل ما عليك القيام به الآن، اتباع الرابط أدناه:<br />\n<br />\n{$activateUrl}<br />\n<br />\nشكراً لك.<br />\n{$principalContactSignature}', 'هذه الرسالة معنونة إلى المستخدمين المسجلين حديثاً لتأكيد صحة عناوينهم البريدية.'),
('USER_VALIDATE', 'en_US', 'Validate Your Account', '{$userFullName}<br />\n<br />\nYou have created an account with {$contextName}, but before you can start using it, you need to validate your email account. To do this, simply follow the link below:<br />\n<br />\n{$activateUrl}<br />\n<br />\nThank you,<br />\n{$principalContactSignature}', 'This email is sent to a newly registered user to validate their email account.'),
('USER_VALIDATE', 'id_ID', 'Validasi Akun Anda', '{$userFullName}<br />\n<br />\nAnda telah membuat akun di {$contextName}.  Sebelum dapat menggunakannya, Anda perlu melakukan validasi akun email. Untuk melakukannya, klik tautan berikut ini:<br />\n<br />\n{$activateUrl}<br />\n<br />\nTerimakasih,<br />\n{$principalContactSignature}', 'Email ini dikirim ke pengguna baru agar melakukan validasi email yang mereka gunakan untuk registrasi.');

-- --------------------------------------------------------

--
-- Table structure for table `event_log`
--

CREATE TABLE `event_log` (
  `log_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_logged` datetime NOT NULL,
  `ip_address` varchar(39) NOT NULL,
  `event_type` bigint(20) DEFAULT NULL,
  `message` text,
  `is_translated` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_log`
--

INSERT INTO `event_log` (`log_id`, `assoc_type`, `assoc_id`, `user_id`, `date_logged`, `ip_address`, `event_type`, `message`, `is_translated`) VALUES
(1, 515, 1, 3, '2019-03-01 21:58:44', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(2, 1048585, 1, 3, '2019-03-01 22:00:13', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(3, 1048585, 1, 3, '2019-03-01 22:13:22', '::1', 268435457, 'submission.event.submissionSubmitted', 0),
(4, 515, 1, 1, '2019-03-01 22:34:20', '::1', 1342177282, 'submission.event.fileDeleted', 0),
(5, 1048585, 1, 1, '2019-03-01 22:34:21', '::1', 1342177283, 'submission.event.lastRevisionDeleted', 0),
(6, 515, 2, 3, '2019-03-01 22:39:41', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(7, 1048585, 2, 3, '2019-03-01 22:40:28', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(8, 1048585, 2, 3, '2019-03-01 22:45:14', '::1', 268435457, 'submission.event.submissionSubmitted', 0),
(9, 1048585, 2, 5, '2019-03-02 18:51:38', '::1', 16777217, 'informationCenter.history.messageSent', 0),
(10, 1048585, 2, 5, '2019-03-02 18:51:39', '::1', 268435459, 'submission.event.participantAdded', 0),
(11, 1048585, 2, 5, '2019-03-02 18:54:11', '::1', 268435460, 'submission.event.participantRemoved', 0),
(12, 1048585, 2, 5, '2019-03-02 18:55:18', '::1', 16777217, 'informationCenter.history.messageSent', 0),
(13, 1048585, 2, 5, '2019-03-02 18:55:19', '::1', 268435459, 'submission.event.participantAdded', 0),
(14, 1048585, 2, 5, '2019-03-02 18:59:03', '::1', 805306371, 'log.editor.decision', 0),
(15, 1048585, 2, 5, '2019-03-02 19:01:59', '::1', 1073741825, 'log.review.reviewerAssigned', 0),
(16, 1048585, 2, 4, '2019-03-02 19:07:30', '::1', 1073741830, 'log.review.reviewAccepted', 0),
(17, 515, 4, 4, '2019-03-02 19:12:31', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(18, 1048585, 2, 4, '2019-03-02 19:12:49', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(19, 1048585, 2, 5, '2019-03-02 19:20:55', '::1', 805306371, 'log.editor.decision', 0),
(20, 1048585, 2, 5, '2019-03-02 19:22:43', '::1', 268435460, 'submission.event.participantRemoved', 0),
(21, 1048585, 2, 5, '2019-03-02 19:23:15', '::1', 268435459, 'submission.event.participantAdded', 0),
(22, 1048585, 2, 5, '2019-03-02 19:23:34', '::1', 268435460, 'submission.event.participantRemoved', 0),
(23, 1048585, 2, 5, '2019-03-02 19:24:28', '::1', 16777217, 'informationCenter.history.messageSent', 0),
(24, 1048585, 2, 5, '2019-03-02 19:24:29', '::1', 268435459, 'submission.event.participantAdded', 0),
(25, 1048585, 2, 5, '2019-03-02 19:25:07', '::1', 268435460, 'submission.event.participantRemoved', 0),
(26, 1048585, 2, 5, '2019-03-02 19:25:43', '::1', 16777217, 'informationCenter.history.messageSent', 0),
(27, 1048585, 2, 5, '2019-03-02 19:25:44', '::1', 268435459, 'submission.event.participantAdded', 0),
(28, 1048585, 2, 5, '2019-03-02 19:38:35', '::1', 805306371, 'log.editor.decision', 0),
(29, 1048585, 2, 5, '2019-03-02 19:45:46', '::1', 268435460, 'submission.event.participantRemoved', 0),
(30, 1048585, 2, 5, '2019-03-02 19:46:21', '::1', 268435459, 'submission.event.participantAdded', 0),
(31, 1048585, 2, 5, '2019-03-02 19:47:06', '::1', 268435460, 'submission.event.participantRemoved', 0),
(32, 1048585, 2, 5, '2019-03-02 19:47:55', '::1', 16777217, 'informationCenter.history.messageSent', 0),
(33, 1048585, 2, 5, '2019-03-02 19:47:56', '::1', 268435459, 'submission.event.participantAdded', 0),
(34, 1048585, 2, 5, '2019-03-02 19:49:10', '::1', 805306371, 'log.editor.decision', 0),
(35, 515, 5, 5, '2019-03-02 19:50:46', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(36, 1048585, 2, 5, '2019-03-02 19:51:04', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(37, 1048585, 2, 5, '2019-03-02 19:51:59', '::1', 805306371, 'log.editor.decision', 0),
(38, 1048585, 2, 5, '2019-03-02 19:54:38', '::1', 805306371, 'log.editor.decision', 0),
(39, 515, 7, 5, '2019-03-02 19:55:39', '::1', 1342177288, 'submission.event.revisionUploaded', 0),
(40, 1048585, 2, 5, '2019-03-02 19:55:51', '::1', 1342177288, 'submission.event.fileRevised', 0),
(41, 515, 8, 5, '2019-03-02 19:57:23', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(42, 1048585, 2, 5, '2019-03-02 19:57:28', '::1', 1342177281, 'submission.event.fileUploaded', 0),
(43, 1048585, 2, 5, '2019-03-02 19:58:52', '::1', 268435464, 'submission.event.issueMetadataUpdated', 0),
(44, 515, 9, 3, '2019-03-04 03:00:29', '::1', 1342177281, 'submission.event.fileUploaded', 0);

-- --------------------------------------------------------

--
-- Table structure for table `event_log_settings`
--

CREATE TABLE `event_log_settings` (
  `log_id` bigint(20) NOT NULL,
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_log_settings`
--

INSERT INTO `event_log_settings` (`log_id`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'fileId', '1', 'int'),
(1, 'fileRevision', '1', 'int'),
(1, 'fileStage', '2', 'int'),
(1, 'originalFileName', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(1, 'revisedFileId', NULL, 'string'),
(1, 'submissionId', '1', 'int'),
(1, 'username', 'author', 'string'),
(2, 'fileId', '1', 'int'),
(2, 'fileRevision', '1', 'int'),
(2, 'fileStage', '2', 'int'),
(2, 'name', 'author, PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(2, 'originalFileName', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(2, 'submissionId', '1', 'int'),
(2, 'username', 'author', 'string'),
(4, 'fileId', '1', 'int'),
(4, 'fileRevision', '1', 'int'),
(4, 'fileStage', '2', 'int'),
(4, 'originalFileName', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(4, 'sourceFileId', NULL, 'string'),
(4, 'submissionId', '1', 'int'),
(4, 'username', 'admin', 'string'),
(5, 'submissionId', '1', 'int'),
(5, 'title', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(5, 'username', 'admin', 'string'),
(6, 'fileId', '2', 'int'),
(6, 'fileRevision', '1', 'int'),
(6, 'fileStage', '2', 'int'),
(6, 'originalFileName', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(6, 'revisedFileId', NULL, 'string'),
(6, 'submissionId', '2', 'int'),
(6, 'username', 'author', 'string'),
(7, 'fileId', '2', 'int'),
(7, 'fileRevision', '1', 'int'),
(7, 'fileStage', '2', 'int'),
(7, 'name', 'author, author artikel proposal.pdf', 'string'),
(7, 'originalFileName', 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(7, 'submissionId', '2', 'int'),
(7, 'username', 'author', 'string'),
(10, 'name', 'Editor editor', 'string'),
(10, 'userGroupName', 'Journal editor', 'string'),
(10, 'username', 'editor', 'string'),
(11, 'name', 'Editor editor', 'string'),
(11, 'userGroupName', 'Journal editor', 'string'),
(11, 'username', 'editor', 'string'),
(13, 'name', 'Editor editor', 'string'),
(13, 'userGroupName', 'Journal editor', 'string'),
(13, 'username', 'editor', 'string'),
(14, 'decision', 'Send to Review', 'string'),
(14, 'editorName', 'Editor editor', 'string'),
(14, 'submissionId', '2', 'string'),
(15, 'reviewAssignmentId', '1', 'int'),
(15, 'reviewerName', 'Reviewer reviewer', 'string'),
(15, 'round', '1', 'int'),
(15, 'stageId', '3', 'int'),
(15, 'submissionId', '2', 'string'),
(16, 'reviewAssignmentId', '1', 'string'),
(16, 'reviewerName', 'Reviewer reviewer', 'string'),
(16, 'round', '1', 'string'),
(16, 'submissionId', '2', 'string'),
(17, 'fileId', '4', 'int'),
(17, 'fileRevision', '1', 'int'),
(17, 'fileStage', '5', 'int'),
(17, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(17, 'revisedFileId', NULL, 'string'),
(17, 'submissionId', '2', 'int'),
(17, 'username', 'reviewer', 'string'),
(18, 'fileId', '4', 'int'),
(18, 'fileRevision', '1', 'int'),
(18, 'fileStage', '5', 'int'),
(18, 'name', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(18, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(18, 'submissionId', '2', 'int'),
(18, 'username', 'reviewer', 'string'),
(19, 'decision', 'Resubmit for Review', 'string'),
(19, 'editorName', 'Editor editor', 'string'),
(19, 'submissionId', '2', 'string'),
(20, 'name', 'Editor editor', 'string'),
(20, 'userGroupName', 'Journal editor', 'string'),
(20, 'username', 'editor', 'string'),
(21, 'name', 'Editor editor', 'string'),
(21, 'userGroupName', 'Journal editor', 'string'),
(21, 'username', 'editor', 'string'),
(22, 'name', 'Editor editor', 'string'),
(22, 'userGroupName', 'Journal editor', 'string'),
(22, 'username', 'editor', 'string'),
(24, 'name', 'Editor editor', 'string'),
(24, 'userGroupName', 'Journal editor', 'string'),
(24, 'username', 'editor', 'string'),
(25, 'name', 'Editor editor', 'string'),
(25, 'userGroupName', 'Journal editor', 'string'),
(25, 'username', 'editor', 'string'),
(27, 'name', 'Editor editor', 'string'),
(27, 'userGroupName', 'Journal editor', 'string'),
(27, 'username', 'editor', 'string'),
(28, 'decision', 'Request Revisions', 'string'),
(28, 'editorName', 'Editor editor', 'string'),
(28, 'submissionId', '2', 'string'),
(29, 'name', 'Editor editor', 'string'),
(29, 'userGroupName', 'Journal editor', 'string'),
(29, 'username', 'editor', 'string'),
(30, 'name', 'Editor editor', 'string'),
(30, 'userGroupName', 'Journal editor', 'string'),
(30, 'username', 'editor', 'string'),
(31, 'name', 'Editor editor', 'string'),
(31, 'userGroupName', 'Journal editor', 'string'),
(31, 'username', 'editor', 'string'),
(33, 'name', 'Editor editor', 'string'),
(33, 'userGroupName', 'Journal editor', 'string'),
(33, 'username', 'editor', 'string'),
(34, 'decision', 'Accept and Skip Review', 'string'),
(34, 'editorName', 'Editor editor', 'string'),
(34, 'submissionId', '2', 'string'),
(35, 'fileId', '5', 'int'),
(35, 'fileRevision', '1', 'int'),
(35, 'fileStage', '15', 'int'),
(35, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(35, 'revisedFileId', NULL, 'string'),
(35, 'submissionId', '2', 'int'),
(35, 'username', 'editor', 'string'),
(36, 'fileId', '5', 'int'),
(36, 'fileRevision', '1', 'int'),
(36, 'fileStage', '15', 'int'),
(36, 'name', 'rev-Article Text, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(36, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(36, 'submissionId', '2', 'int'),
(36, 'username', 'editor', 'string'),
(37, 'decision', 'Accept and Skip Review', 'string'),
(37, 'editorName', 'Editor editor', 'string'),
(37, 'submissionId', '2', 'string'),
(38, 'decision', 'Send To Production', 'string'),
(38, 'editorName', 'Editor editor', 'string'),
(38, 'submissionId', '2', 'string'),
(39, 'fileId', '7', 'int'),
(39, 'fileRevision', '2', 'int'),
(39, 'fileStage', '11', 'int'),
(39, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(39, 'revisedFileId', '7', 'int'),
(39, 'submissionId', '2', 'int'),
(39, 'username', 'editor', 'string'),
(40, 'fileId', '7', 'int'),
(40, 'fileRevision', '2', 'int'),
(40, 'fileStage', '11', 'int'),
(40, 'name', 'editor, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(40, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(40, 'submissionId', '2', 'int'),
(40, 'username', 'editor', 'string'),
(41, 'fileId', '8', 'int'),
(41, 'fileRevision', '1', 'int'),
(41, 'fileStage', '10', 'int'),
(41, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(41, 'revisedFileId', NULL, 'string'),
(41, 'submissionId', '2', 'int'),
(41, 'username', 'editor', 'string'),
(42, 'fileId', '8', 'int'),
(42, 'fileRevision', '1', 'int'),
(42, 'fileStage', '10', 'int'),
(42, 'name', 'editor, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(42, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(42, 'submissionId', '2', 'int'),
(42, 'username', 'editor', 'string'),
(44, 'fileId', '9', 'int'),
(44, 'fileRevision', '1', 'int'),
(44, 'fileStage', '2', 'int'),
(44, 'originalFileName', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(44, 'revisedFileId', NULL, 'string'),
(44, 'submissionId', '3', 'int'),
(44, 'username', 'author', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `filters`
--

CREATE TABLE `filters` (
  `filter_id` bigint(20) NOT NULL,
  `filter_group_id` bigint(20) NOT NULL DEFAULT '0',
  `context_id` bigint(20) NOT NULL DEFAULT '0',
  `display_name` varchar(255) DEFAULT NULL,
  `class_name` varchar(255) DEFAULT NULL,
  `is_template` tinyint(4) NOT NULL DEFAULT '0',
  `parent_filter_id` bigint(20) NOT NULL DEFAULT '0',
  `seq` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filters`
--

INSERT INTO `filters` (`filter_id`, `filter_group_id`, `context_id`, `display_name`, `class_name`, `is_template`, `parent_filter_id`, `seq`) VALUES
(1, 1, 0, 'Extract metadata from a(n) Article', 'plugins.metadata.dc11.filter.Dc11SchemaArticleAdapter', 0, 0, 0),
(2, 2, 0, 'MODS 3.4', 'lib.pkp.plugins.metadata.mods34.filter.Mods34DescriptionXmlFilter', 0, 0, 0),
(3, 3, 0, 'Extract metadata from a(n) Article', 'plugins.metadata.mods34.filter.Mods34SchemaArticleAdapter', 0, 0, 0),
(4, 4, 0, 'Inject metadata into a(n) Article', 'plugins.metadata.mods34.filter.Mods34SchemaArticleAdapter', 0, 0, 0),
(5, 5, 0, 'Crossref XML issue export', 'plugins.importexport.crossref.filter.IssueCrossrefXmlFilter', 0, 0, 0),
(6, 6, 0, 'Crossref XML issue export', 'plugins.importexport.crossref.filter.ArticleCrossrefXmlFilter', 0, 0, 0),
(7, 7, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(8, 8, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(9, 9, 0, 'DataCite XML export', 'plugins.importexport.datacite.filter.DataciteXmlFilter', 0, 0, 0),
(10, 10, 0, 'DOAJ XML export', 'plugins.importexport.doaj.filter.DOAJXmlFilter', 0, 0, 0),
(11, 11, 0, 'DOAJ JSON export', 'plugins.importexport.doaj.filter.DOAJJsonFilter', 0, 0, 0),
(12, 12, 0, 'mEDRA XML issue export', 'plugins.importexport.medra.filter.IssueMedraXmlFilter', 0, 0, 0),
(13, 13, 0, 'mEDRA XML article export', 'plugins.importexport.medra.filter.ArticleMedraXmlFilter', 0, 0, 0),
(14, 14, 0, 'mEDRA XML article export', 'plugins.importexport.medra.filter.GalleyMedraXmlFilter', 0, 0, 0),
(15, 15, 0, 'Native XML submission export', 'plugins.importexport.native.filter.ArticleNativeXmlFilter', 0, 0, 0),
(16, 16, 0, 'Native XML submission import', 'plugins.importexport.native.filter.NativeXmlArticleFilter', 0, 0, 0),
(17, 17, 0, 'Native XML issue export', 'plugins.importexport.native.filter.IssueNativeXmlFilter', 0, 0, 0),
(18, 18, 0, 'Native XML issue import', 'plugins.importexport.native.filter.NativeXmlIssueFilter', 0, 0, 0),
(19, 19, 0, 'Native XML issue galley export', 'plugins.importexport.native.filter.IssueGalleyNativeXmlFilter', 0, 0, 0),
(20, 20, 0, 'Native XML issue galley import', 'plugins.importexport.native.filter.NativeXmlIssueGalleyFilter', 0, 0, 0),
(21, 21, 0, 'Native XML author export', 'plugins.importexport.native.filter.AuthorNativeXmlFilter', 0, 0, 0),
(22, 22, 0, 'Native XML author import', 'plugins.importexport.native.filter.NativeXmlAuthorFilter', 0, 0, 0),
(23, 26, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlArticleFileFilter', 0, 0, 0),
(24, 27, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlArtworkFileFilter', 0, 0, 0),
(25, 28, 0, 'Native XML submission file import', 'plugins.importexport.native.filter.NativeXmlSupplementaryFileFilter', 0, 0, 0),
(26, 23, 0, 'Native XML submission file export', 'lib.pkp.plugins.importexport.native.filter.SubmissionFileNativeXmlFilter', 0, 0, 0),
(27, 24, 0, 'Native XML submission file export', 'plugins.importexport.native.filter.ArtworkFileNativeXmlFilter', 0, 0, 0),
(28, 25, 0, 'Native XML submission file export', 'plugins.importexport.native.filter.SupplementaryFileNativeXmlFilter', 0, 0, 0),
(29, 29, 0, 'Native XML representation export', 'plugins.importexport.native.filter.ArticleGalleyNativeXmlFilter', 0, 0, 0),
(30, 30, 0, 'Native XML representation import', 'plugins.importexport.native.filter.NativeXmlArticleGalleyFilter', 0, 0, 0),
(31, 31, 0, 'ArticlePubMedXmlFilter', 'plugins.importexport.pubmed.filter.ArticlePubMedXmlFilter', 0, 0, 0),
(32, 32, 0, 'User XML user export', 'lib.pkp.plugins.importexport.users.filter.PKPUserUserXmlFilter', 0, 0, 0),
(33, 33, 0, 'User XML user import', 'lib.pkp.plugins.importexport.users.filter.UserXmlPKPUserFilter', 0, 0, 0),
(34, 34, 0, 'Native XML user group export', 'lib.pkp.plugins.importexport.users.filter.UserGroupNativeXmlFilter', 0, 0, 0),
(35, 35, 0, 'Native XML user group import', 'lib.pkp.plugins.importexport.users.filter.NativeXmlUserGroupFilter', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `filter_groups`
--

CREATE TABLE `filter_groups` (
  `filter_group_id` bigint(20) NOT NULL,
  `symbolic` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `input_type` varchar(255) DEFAULT NULL,
  `output_type` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `filter_groups`
--

INSERT INTO `filter_groups` (`filter_group_id`, `symbolic`, `display_name`, `description`, `input_type`, `output_type`) VALUES
(1, 'article=>dc11', 'plugins.metadata.dc11.articleAdapter.displayName', 'plugins.metadata.dc11.articleAdapter.description', 'class::classes.article.Article', 'metadata::plugins.metadata.dc11.schema.Dc11Schema(ARTICLE)'),
(2, 'mods34=>mods34-xml', 'plugins.metadata.mods34.mods34XmlOutput.displayName', 'plugins.metadata.mods34.mods34XmlOutput.description', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(*)', 'xml::schema(lib/pkp/plugins/metadata/mods34/filter/mods34.xsd)'),
(3, 'article=>mods34', 'plugins.metadata.mods34.articleAdapter.displayName', 'plugins.metadata.mods34.articleAdapter.description', 'class::classes.article.Article', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(ARTICLE)'),
(4, 'mods34=>article', 'plugins.metadata.mods34.articleAdapter.displayName', 'plugins.metadata.mods34.articleAdapter.description', 'metadata::plugins.metadata.mods34.schema.Mods34Schema(ARTICLE)', 'class::classes.article.Article'),
(5, 'issue=>crossref-xml', 'plugins.importexport.crossref.displayName', 'plugins.importexport.crossref.description', 'class::classes.issue.Issue[]', 'xml::schema(https://www.crossref.org/schemas/crossref4.3.6.xsd)'),
(6, 'article=>crossref-xml', 'plugins.importexport.crossref.displayName', 'plugins.importexport.crossref.description', 'class::classes.article.Article[]', 'xml::schema(https://www.crossref.org/schemas/crossref4.3.6.xsd)'),
(7, 'issue=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.issue.Issue', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(8, 'article=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.article.Article', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(9, 'galley=>datacite-xml', 'plugins.importexport.datacite.displayName', 'plugins.importexport.datacite.description', 'class::classes.article.ArticleGalley', 'xml::schema(http://schema.datacite.org/meta/kernel-4/metadata.xsd)'),
(10, 'article=>doaj-xml', 'plugins.importexport.doaj.displayName', 'plugins.importexport.doaj.description', 'class::classes.article.Article[]', 'xml::schema(plugins/importexport/doaj/doajArticles.xsd)'),
(11, 'article=>doaj-json', 'plugins.importexport.doaj.displayName', 'plugins.importexport.doaj.description', 'class::classes.article.Article', 'primitive::string'),
(12, 'issue=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.issue.Issue[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(13, 'article=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.article.Article[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(14, 'galley=>medra-xml', 'plugins.importexport.medra.displayName', 'plugins.importexport.medra.description', 'class::classes.article.ArticleGalley[]', 'xml::schema(http://www.medra.org/schema/onix/DOIMetadata/2.0/ONIX_DOIMetadata_2.0.xsd)'),
(15, 'article=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.article.Article[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(16, 'native-xml=>article', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.article.Article[]'),
(17, 'issue=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.issue.Issue[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(18, 'native-xml=>issue', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.issue.Issue[]'),
(19, 'issuegalley=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.issue.IssueGalley[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(20, 'native-xml=>issuegalley', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.issue.IssueGalley[]'),
(21, 'author=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.article.Author[]', 'xml::schema(plugins/importexport/native/native.xsd)'),
(22, 'native-xml=>author', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.article.Author[]'),
(23, 'SubmissionFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SubmissionFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(24, 'SubmissionArtworkFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SubmissionArtworkFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(25, 'SupplementaryFile=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::lib.pkp.classes.submission.SupplementaryFile', 'xml::schema(plugins/importexport/native/native.xsd)'),
(26, 'native-xml=>SubmissionFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SubmissionFile'),
(27, 'native-xml=>SubmissionArtworkFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SubmissionArtworkFile'),
(28, 'native-xml=>SupplementaryFile', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::lib.pkp.classes.submission.SupplementaryFile'),
(29, 'article-galley=>native-xml', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'class::classes.article.ArticleGalley', 'xml::schema(plugins/importexport/native/native.xsd)'),
(30, 'native-xml=>ArticleGalley', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(plugins/importexport/native/native.xsd)', 'class::classes.article.ArticleGalley[]'),
(31, 'article=>pubmed-xml', 'plugins.importexport.pubmed.displayName', 'plugins.importexport.pubmed.description', 'class::classes.article.Article[]', 'xml::dtd'),
(32, 'user=>user-xml', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'class::classes.user.User[]', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)'),
(33, 'user-xml=>user', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)', 'class::classes.users.User[]'),
(34, 'usergroup=>user-xml', 'plugins.importexport.users.displayName', 'plugins.importexport.users.description', 'class::lib.pkp.classes.security.UserGroup[]', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)'),
(35, 'user-xml=>usergroup', 'plugins.importexport.native.displayName', 'plugins.importexport.native.description', 'xml::schema(lib/pkp/plugins/importexport/users/pkp-users.xsd)', 'class::lib.pkp.classes.security.UserGroup[]');

-- --------------------------------------------------------

--
-- Table structure for table `filter_settings`
--

CREATE TABLE `filter_settings` (
  `filter_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `genre_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `seq` bigint(20) DEFAULT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  `category` bigint(20) NOT NULL DEFAULT '1',
  `dependent` tinyint(4) NOT NULL DEFAULT '0',
  `supplementary` tinyint(4) DEFAULT '0',
  `entry_key` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`genre_id`, `context_id`, `seq`, `enabled`, `category`, `dependent`, `supplementary`, `entry_key`) VALUES
(1, 1, 0, 1, 1, 0, 0, 'SUBMISSION'),
(2, 1, 1, 1, 3, 0, 1, 'RESEARCHINSTRUMENT'),
(3, 1, 2, 1, 3, 0, 1, 'RESEARCHMATERIALS'),
(4, 1, 3, 1, 3, 0, 1, 'RESEARCHRESULTS'),
(5, 1, 4, 1, 3, 0, 1, 'TRANSCRIPTS'),
(6, 1, 5, 1, 3, 0, 1, 'DATAANALYSIS'),
(7, 1, 6, 1, 3, 0, 1, 'DATASET'),
(8, 1, 7, 1, 3, 0, 1, 'SOURCETEXTS'),
(9, 1, 8, 1, 1, 1, 1, 'MULTIMEDIA'),
(10, 1, 9, 1, 2, 1, 0, 'IMAGE'),
(11, 1, 10, 1, 1, 1, 0, 'STYLE'),
(12, 1, 11, 1, 3, 0, 1, 'OTHER');

-- --------------------------------------------------------

--
-- Table structure for table `genre_settings`
--

CREATE TABLE `genre_settings` (
  `genre_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre_settings`
--

INSERT INTO `genre_settings` (`genre_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'ar_IQ', 'name', 'نص المقال', 'string'),
(1, 'en_US', 'name', 'Article Text', 'string'),
(1, 'id_ID', 'name', 'File Utama Naskah', 'string'),
(2, 'ar_IQ', 'name', 'أدوات البحث العلمي', 'string'),
(2, 'en_US', 'name', 'Research Instrument', 'string'),
(2, 'id_ID', 'name', 'Instrumen Penelitian', 'string'),
(3, 'ar_IQ', 'name', 'مواد البحث العلمي', 'string'),
(3, 'en_US', 'name', 'Research Materials', 'string'),
(3, 'id_ID', 'name', 'Bahan Penelitian', 'string'),
(4, 'ar_IQ', 'name', 'نتائج البحث العلمي', 'string'),
(4, 'en_US', 'name', 'Research Results', 'string'),
(4, 'id_ID', 'name', 'Hasil Penelitian', 'string'),
(5, 'ar_IQ', 'name', 'النسخ', 'string'),
(5, 'en_US', 'name', 'Transcripts', 'string'),
(5, 'id_ID', 'name', 'Transkrip', 'string'),
(6, 'ar_IQ', 'name', 'تحليل البيانات', 'string'),
(6, 'en_US', 'name', 'Data Analysis', 'string'),
(6, 'id_ID', 'name', 'Analisis Data', 'string'),
(7, 'ar_IQ', 'name', 'جداول البيانات', 'string'),
(7, 'en_US', 'name', 'Data Set', 'string'),
(7, 'id_ID', 'name', 'Data Set', 'string'),
(8, 'ar_IQ', 'name', 'نصوص المصادر', 'string'),
(8, 'en_US', 'name', 'Source Texts', 'string'),
(8, 'id_ID', 'name', 'Teks Sumber', 'string'),
(9, 'ar_IQ', 'name', 'متعدد الوسائط', 'string'),
(9, 'en_US', 'name', 'Multimedia', 'string'),
(9, 'id_ID', 'name', '##default.genres.multimedia##', 'string'),
(10, 'ar_IQ', 'name', 'صورة', 'string'),
(10, 'en_US', 'name', 'Image', 'string'),
(10, 'id_ID', 'name', '##default.genres.image##', 'string'),
(11, 'ar_IQ', 'name', 'ملف أنماط HTML', 'string'),
(11, 'en_US', 'name', 'HTML Stylesheet', 'string'),
(11, 'id_ID', 'name', '##default.genres.styleSheet##', 'string'),
(12, 'ar_IQ', 'name', 'الغير', 'string'),
(12, 'en_US', 'name', 'Other', 'string'),
(12, 'id_ID', 'name', '##default.genres.other##', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `institutional_subscriptions`
--

CREATE TABLE `institutional_subscriptions` (
  `institutional_subscription_id` bigint(20) NOT NULL,
  `subscription_id` bigint(20) NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `mailing_address` varchar(255) DEFAULT NULL,
  `domain` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `institutional_subscription_ip`
--

CREATE TABLE `institutional_subscription_ip` (
  `institutional_subscription_ip_id` bigint(20) NOT NULL,
  `subscription_id` bigint(20) NOT NULL,
  `ip_string` varchar(40) NOT NULL,
  `ip_start` bigint(20) NOT NULL,
  `ip_end` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issues`
--

CREATE TABLE `issues` (
  `issue_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `volume` smallint(6) DEFAULT NULL,
  `number` varchar(10) DEFAULT NULL,
  `year` smallint(6) DEFAULT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '0',
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `date_published` datetime DEFAULT NULL,
  `date_notified` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `access_status` tinyint(4) NOT NULL DEFAULT '1',
  `open_access_date` datetime DEFAULT NULL,
  `show_volume` tinyint(4) NOT NULL DEFAULT '0',
  `show_number` tinyint(4) NOT NULL DEFAULT '0',
  `show_year` tinyint(4) NOT NULL DEFAULT '0',
  `show_title` tinyint(4) NOT NULL DEFAULT '0',
  `style_file_name` varchar(90) DEFAULT NULL,
  `original_style_file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issues`
--

INSERT INTO `issues` (`issue_id`, `journal_id`, `volume`, `number`, `year`, `published`, `current`, `date_published`, `date_notified`, `last_modified`, `access_status`, `open_access_date`, `show_volume`, `show_number`, `show_year`, `show_title`, `style_file_name`, `original_style_file_name`) VALUES
(1, 1, 3, '1', 2019, 1, 1, '2019-03-02 00:00:00', NULL, '2019-03-02 22:31:18', 1, NULL, 1, 1, 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `issue_files`
--

CREATE TABLE `issue_files` (
  `file_id` bigint(20) NOT NULL,
  `issue_id` bigint(20) NOT NULL,
  `file_name` varchar(90) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `content_type` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issue_galleys`
--

CREATE TABLE `issue_galleys` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `issue_id` bigint(20) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `label` varchar(32) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issue_galley_settings`
--

CREATE TABLE `issue_galley_settings` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `issue_settings`
--

CREATE TABLE `issue_settings` (
  `issue_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `issue_settings`
--

INSERT INTO `issue_settings` (`issue_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'coverImage', 'cover_issue_1_en_US.png', 'string'),
(1, 'en_US', 'description', '', 'string'),
(1, 'en_US', 'title', '', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `item_views`
--

CREATE TABLE `item_views` (
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` varchar(32) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `date_last_viewed` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `item_views`
--

INSERT INTO `item_views` (`assoc_type`, `assoc_id`, `user_id`, `date_last_viewed`) VALUES
(515, '2-1', 5, '2019-03-02 18:56:16'),
(516, '1', 5, '2019-03-02 19:27:16'),
(515, '2-1', 3, '2019-03-02 19:32:29'),
(515, '8-1', 1, '2019-03-02 20:15:41');

-- --------------------------------------------------------

--
-- Table structure for table `journals`
--

CREATE TABLE `journals` (
  `journal_id` bigint(20) NOT NULL,
  `path` varchar(32) NOT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `primary_locale` varchar(14) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `journals`
--

INSERT INTO `journals` (`journal_id`, `path`, `seq`, `primary_locale`, `enabled`) VALUES
(1, 'tested', 1, 'en_US', 1);

-- --------------------------------------------------------

--
-- Table structure for table `journal_settings`
--

CREATE TABLE `journal_settings` (
  `journal_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `journal_settings`
--

INSERT INTO `journal_settings` (`journal_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'agenciesEnabledSubmission', '0', 'bool'),
(1, '', 'agenciesEnabledWorkflow', '1', 'bool'),
(1, '', 'agenciesRequired', '0', 'bool'),
(1, '', 'citationsEnabledSubmission', '0', 'bool'),
(1, '', 'citationsEnabledWorkflow', '1', 'bool'),
(1, '', 'citationsRequired', '0', 'bool'),
(1, '', 'contactEmail', 'fachrealheart96@gmail.com', 'string'),
(1, '', 'contactName', 'FachrealHeart', 'string'),
(1, '', 'contactPhone', '', 'string'),
(1, '', 'copyrightHolderType', NULL, 'string'),
(1, '', 'copyrightNoticeAgree', '0', 'bool'),
(1, '', 'copyrightYearBasis', NULL, 'string'),
(1, '', 'copySubmissionAckAddress', '', 'string'),
(1, '', 'copySubmissionAckPrimaryContact', '0', 'bool'),
(1, '', 'coverageEnabledSubmission', '0', 'bool'),
(1, '', 'coverageEnabledWorkflow', '1', 'bool'),
(1, '', 'coverageRequired', '0', 'bool'),
(1, '', 'currency', 'IDR', 'string'),
(1, '', 'defaultReviewMode', '2', 'int'),
(1, '', 'disciplinesEnabledSubmission', '0', 'bool'),
(1, '', 'disciplinesEnabledWorkflow', '1', 'bool'),
(1, '', 'disciplinesRequired', '0', 'bool'),
(1, '', 'emailSignature', '<p><br> ________________________________________________________________________<br> <a href=\"{$contextUrl}\">{$contextName}</a></p>', 'string'),
(1, '', 'enableAnnouncements', '1', 'bool'),
(1, '', 'enableAnnouncementsHomepage', '1', 'bool'),
(1, '', 'enableAuthorSelfArchive', '0', 'bool'),
(1, '', 'envelopeSender', NULL, 'string'),
(1, '', 'itemsPerPage', '25', 'int'),
(1, '', 'keywordsEnabledSubmission', '1', 'bool'),
(1, '', 'keywordsEnabledWorkflow', '1', 'bool'),
(1, '', 'keywordsRequired', '0', 'bool'),
(1, '', 'languagesEnabledSubmission', '0', 'bool'),
(1, '', 'languagesEnabledWorkflow', '1', 'bool'),
(1, '', 'languagesRequired', '0', 'bool'),
(1, '', 'licenseURL', 'http://creativecommons.org/licenses/by-nc-nd/4.0', 'string'),
(1, '', 'mailingAddress', 'Untuk informasi lebih lanjut, silahkan lihat peta lokasi atau bisa langsung menghubungi pihak terkait.', 'string'),
(1, '', 'membershipFee', '0', 'int'),
(1, '', 'navItems', 'N;', 'object'),
(1, '', 'numAnnouncementsHomepage', '2', 'int'),
(1, '', 'numDaysBeforeInviteReminder', '0', 'int'),
(1, '', 'numDaysBeforeSubmitReminder', '0', 'int'),
(1, '', 'numPageLinks', '10', 'int'),
(1, '', 'numWeeksPerResponse', '0', 'int'),
(1, '', 'numWeeksPerReview', '4', 'int'),
(1, '', 'onlineIssn', '', 'string'),
(1, '', 'paymentPluginName', 'ManualPayment', 'string'),
(1, '', 'paymentsEnabled', '0', 'bool'),
(1, '', 'printIssn', '2339-1162', 'string'),
(1, '', 'publicationFee', '0', 'int'),
(1, '', 'publisherInstitution', 'STAIPI Persis Bandung', 'string'),
(1, '', 'purchaseArticleFee', '0', 'int'),
(1, '', 'restrictReviewerFileAccess', '0', 'bool'),
(1, '', 'reviewerAccessKeysEnabled', '0', 'bool'),
(1, '', 'reviewerCompetingInterestsRequired', '0', 'bool'),
(1, '', 'rightsEnabledSubmission', '0', 'bool'),
(1, '', 'rightsEnabledWorkflow', '0', 'bool'),
(1, '', 'rightsRequired', '0', 'bool'),
(1, '', 'rtAbstract', '1', 'bool'),
(1, '', 'rtAddComment', '1', 'bool'),
(1, '', 'rtCaptureCite', '1', 'bool'),
(1, '', 'rtDefineTerms', '1', 'bool'),
(1, '', 'rtEmailAuthor', '1', 'bool'),
(1, '', 'rtEmailOthers', '1', 'bool'),
(1, '', 'rtPrinterFriendly', '1', 'bool'),
(1, '', 'rtSupplementaryFiles', '1', 'bool'),
(1, '', 'rtViewMetadata', '1', 'bool'),
(1, '', 'showEnsuringLink', '0', 'bool'),
(1, '', 'sourceEnabledSubmission', '0', 'bool'),
(1, '', 'sourceEnabledWorkflow', '0', 'bool'),
(1, '', 'sourceRequired', '0', 'bool'),
(1, '', 'subjectsEnabledSubmission', '0', 'bool'),
(1, '', 'subjectsEnabledWorkflow', '1', 'bool'),
(1, '', 'subjectsRequired', '0', 'bool'),
(1, '', 'supportedFormLocales', 'a:1:{i:0;s:5:\"en_US\";}', 'object'),
(1, '', 'supportedLocales', 'a:3:{i:0;s:5:\"en_US\";i:1;s:5:\"id_ID\";i:2;s:5:\"ar_IQ\";}', 'object'),
(1, '', 'supportedSubmissionLocales', 'a:1:{i:0;s:5:\"en_US\";}', 'object'),
(1, '', 'supportEmail', 'admin@staipi.com', 'string'),
(1, '', 'supportName', 'admin', 'string'),
(1, '', 'supportPhone', '(022) 7563521', 'string'),
(1, '', 'themePluginPath', 'bootstrap3', 'string'),
(1, '', 'typeEnabledSubmission', '0', 'bool'),
(1, '', 'typeEnabledWorkflow', '0', 'bool'),
(1, '', 'typeRequired', '0', 'bool'),
(1, 'en_US', 'abbreviation', 'ts', 'string'),
(1, 'en_US', 'about', '<p>This journal discusses a conceptualization of existing studies in the field of education that use wordless picturebooks with young readers. The main aim of this study is to encourage a more interdisciplinary understanding of meaning-making and persuade educational researchers and mediators to consider investigative approaches that are not based on verbalization but are more in tune with the invitations that wordless picturebooks extend to young readers. The findings showed that wordless picturebooks allow readers have chances from the authority and weight of the words they must continually deal with both in school and elsewhere.</p>', 'string'),
(1, 'en_US', 'acronym', 'tested', 'string'),
(1, 'en_US', 'authorGuidelines', '<p>Detail guidline for submiting article can be found in <a href=\"https://drive.google.com/file/d/1xh4Sc_oh3NlhjRIaKlGFjBNX97K-1fx8/view\" target=\"_blank\" rel=\"noopener\">Submission Kit</a>. It contains author guideline as well as template for article</p>', 'string'),
(1, 'en_US', 'authorInformation', 'Interested in submitting to this journal? We recommend that you review the <a href=\"http://localhost/journal/index.php/tested/about\">About the Journal</a> page for the journal\'s section policies, as well as the <a href=\"http://localhost/journal/index.php/tested/about/submissions#authorGuidelines\">Author Guidelines</a>. Authors need to <a href=\"http://localhost/journal/index.php/tested/user/register\">register</a> with the journal prior to submitting or, if already registered, can simply <a href=\"http://localhost/journal/index.php/index/login\">log in</a> and begin the five-step process.', 'string'),
(1, 'en_US', 'authorSelfArchivePolicy', '<p>This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.</p>', 'string'),
(1, 'en_US', 'clockssLicense', 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://clockss.org/\">More...</a>', 'string'),
(1, 'en_US', 'contactAffiliation', 'STAIPI Persis bandung', 'string'),
(1, 'en_US', 'contactTitle', 'fachreal', 'string'),
(1, 'en_US', 'copyeditInstructions', 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections.\n\nThe file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.\n\n\n<h4>Copyediting Systems</h4>\n\n<strong>1. Microsoft Word\'s Track Changes</strong>\n\nUnder Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author.\n\nThe editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets.\n\nAfter the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage.\n\n\n<strong>2. Harvard Educational Review</strong>\n\n<strong>Instructions for Making Electronic Revisions to the Manuscript</strong>\n\nPlease follow the following protocol for making electronic revisions to your manuscript:\n\n<strong>Responding to suggested changes.</strong>\n&nbsp; For each of the suggested changes that you accept, unbold the text.\n&nbsp; For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it.\n\n<strong>Making additions and deletions.</strong>\n&nbsp; Indicate additions by <strong>bolding</strong> the new text.\n&nbsp; Replace deleted sections with: <strong>[deleted text]</strong>.\n&nbsp; If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>.\n\n<strong>Responding to Queries to the Author (QAs).</strong>\n&nbsp; Keep all QAs intact and bolded within the text. Do not delete them.\n&nbsp; To reply to a QA, add a comment after it. Comments should be delimited using:\n<strong>[Comment:]</strong>\n&nbsp; e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>.\n\n<strong>Making comments.</strong>\n&nbsp; Use comments to explain organizational changes or major revisions\n&nbsp; e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>\n&nbsp; Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.\n\n<h4>An Illustration of an Electronic Revision</h4>\n\n<ol>\n<li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li>\n<li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li>\n<li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li>\n</ol>', 'string'),
(1, 'en_US', 'copyrightNotice', '<p style=\"text-align: justify;\">The Authors submitting a manuscript do so on the understanding that if accepted for publication, copyright of the article shall be assigned to journal Tested Faculty of Humanities, Universitas STAIPI as publisher of the journal, and the author&nbsp;also holds the copyright without restriction.</p>\r\n<p style=\"text-align: justify;\">Copyright encompasses exclusive rights to reproduce and deliver the article in all form and media, including reprints, photographs, microfilms and any other similar reproductions, as well as translations. The reproduction of any part of this journal, its storage in databases and its transmission by any form or media, such as electronic, electrostatic and mechanical copies, photocopies, recordings, magnetic media, etc. , are allowed with a written permission from journal Tested Faculty of Humanities, Universitas STAIPI.</p>\r\n<p style=\"text-align: justify;\">Jurnal Tested Board, Faculty of Humanities, Universitas STAIPI, the Editors and the Advisory International Editorial Board make every effort to ensure that no wrong or misleading data, opinions or statements be published in the journal. In any way, the contents of the articles and advertisements published in the journal Tested Faculty of Humanities, Universitas STAIPI are sole and exclusive responsibility of their respective authors and advertisers.</p>', 'string'),
(1, 'en_US', 'description', '<p>&nbsp;</p>\r\n<p><img style=\"width: 100%; height: 100%;\" src=\"http://stai-persis-bandung.ac.id/images/logo.png\" alt=\"Staipi\"></p>\r\n<p style=\"text-align: justify;\">This journal mostly discussed about the important of using narrative in the within language education. According to the researcher, narrative can help to address the field’s changing needs by further democratizing knowledge production and exchange, illuminating subtle yet vital dimensions of classroom interactions, and prompting imaginative interpretations and revisionists. His argument draws together relevant strands of applied linguistics research; narrative theories and research from education, sociology, and the arts. He concluded that incorporate narratives of classroom life, in light of some important dilemmas and cautions.</p>', 'string'),
(1, 'en_US', 'editorialTeam', '<div id=\"group\">\r\n<h4>Editorial in Chief</h4>\r\n<div class=\"member\">\r\n<ul>\r\n<li class=\"show\"><a>Diki Pahrilah Fat Hamubin</a>, Student Of LPKIA Bandung, Indonesia</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div id=\"group\">\r\n<h4>Managing Editor</h4>\r\n<div class=\"member\">\r\n<ul>\r\n<li class=\"show\"><a>Admin</a>, Administrator</li>\r\n</ul>\r\n</div>\r\n</div>\r\n<div id=\"group\">\r\n<h4>Editor Board</h4>\r\n<div class=\"member\">\r\n<ul>\r\n<li class=\"show\"><a>Diki Pahrilah Fat Hamubin</a>, Student Of LPKIA Bandung, Indonesia</li>\r\n</ul>\r\n<ul>\r\n<li class=\"show\"><a>Diaz Faizal Malik</a>, Student Of LPKIA Bandung, Indonesia</li>\r\n</ul>\r\n<ul>\r\n<li class=\"show\"><a>Panji Dennis Giantoro</a>, Student Of LPKIA Bandung, Indonesia</li>\r\n</ul>\r\n</div>\r\n</div>', 'string'),
(1, 'en_US', 'favicon', 'a:6:{s:4:\"name\";s:9:\"logo9.png\";s:10:\"uploadName\";s:17:\"favicon_en_US.png\";s:5:\"width\";i:254;s:6:\"height\";i:252;s:12:\"dateUploaded\";s:19:\"2019-02-28 08:20:19\";s:7:\"altText\";s:0:\"\";}', 'object'),
(1, 'en_US', 'journalThumbnail', 'a:6:{s:4:\"name\";s:37:\"Cityscape Buildings Movie Poster2.png\";s:10:\"uploadName\";s:26:\"journalThumbnail_en_US.png\";s:5:\"width\";i:526;s:6:\"height\";i:744;s:12:\"dateUploaded\";s:19:\"2019-02-28 09:44:41\";s:7:\"altText\";s:0:\"\";}', 'object'),
(1, 'en_US', 'librarianInformation', 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href=\"http://pkp.sfu.ca/ojs\">Open Journal Systems</a>).', 'string'),
(1, 'en_US', 'lockssLicense', 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href=\"http://www.lockss.org/\">More...</a>', 'string'),
(1, 'en_US', 'metaCitations', '1', 'string'),
(1, 'en_US', 'name', 'Tested', 'string'),
(1, 'en_US', 'openAccessPolicy', 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.', 'string'),
(1, 'en_US', 'pageFooter', '<p><strong>Jurnal Tested ter Index :</strong></p>\r\n<p><img src=\"/journal/public/site/images/admin/google-scholar-logo-png-5.png\" width=\"119\" height=\"34\"></p>', 'string'),
(1, 'en_US', 'pageHeaderLogoImage', 'a:6:{s:4:\"name\";s:9:\"logo9.png\";s:10:\"uploadName\";s:29:\"pageHeaderLogoImage_en_US.png\";s:5:\"width\";i:254;s:6:\"height\";i:252;s:12:\"dateUploaded\";s:19:\"2019-02-28 07:55:29\";s:7:\"altText\";s:6:\"STAIPI\";}', 'object'),
(1, 'en_US', 'privacyStatement', '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>', 'string'),
(1, 'en_US', 'proofInstructions', '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p>\n	<h4>For Spelling and Grammar Errors</h4>\n\n	<p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with \"CHANGE-TO\" instructions to the editor as follows:</p>\n\n	<pre>1. CHANGE...\n	then the others\n	TO...\n	than the others</pre>\n	<br />\n	<pre>2. CHANGE...\n	Malinowsky\n	TO...\n	Malinowski</pre>\n	<br />\n\n	<h4>For Formatting Errors</h4>\n\n	<p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title \"FORMATTING\" as follows:</p>\n	<br />\n	<pre>3. FORMATTING\n	The numbers in Table 3 are not aligned in the third column.</pre>\n	<br />\n	<pre>4. FORMATTING\n	The paragraph that begins \"This last topic...\" is not indented.</pre>', 'string'),
(1, 'en_US', 'readerInformation', 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href=\"http://localhost/journal/index.php/tested/user/register\">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href=\"http://localhost/journal/index.php/tested/about/submissions#privacyStatement\">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.', 'string'),
(1, 'en_US', 'refLinkInstructions', '<h4>To Add Reference Linking to the Layout Process</h4>\n	<p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p>\n	<h4>A. When the Author Provides a Link with the Reference</h4>\n	<ol>\n	<li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li>\n	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>\n	</ol>\n	<h4>B. Enabling Readers to Search Google Scholar For References</h4>\n	<ol>\n		<li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., \"Peace\"—then copy author and title).</li>\n		<li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&hl=en&lr=&btnG=Search.</li>\n\n	<li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li>\n	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>\n	</ol>\n	<h4>C. Enabling Readers to Search for References with a DOI</h4>\n	<ol>\n	<li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li>\n	<li>Paste each DOI that the Query provides in the following URL (between = and &): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&link_type=DOI.</li>\n	<li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li>\n	<li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li>\n	</ol>', 'string'),
(1, 'en_US', 'reviewGuidelines', '<h1>Peer Review Process</h1>\r\n<p style=\"text-align: justify;\">The research article submitted to this online journal will be peer-reviewed. The accepted research articles will be available online (free download) following the journal peer-reviewing process. The language used in this Journal is English. Detail information about reviewing, download <a href=\"http://arbitrer.fib.unand.ac.id/index.php/arbitrer/pages/view/review#\">Reviewer Kit</a>.</p>\r\n<p style=\"text-align: justify;\">Reviewing a manuscript written by a fellow scientist is a privilege. However, it is a time-consuming responsibility. Hence, Arbitrer Editorial Board, authors, and audiences appreciate your willingness to accept this responsibility and your dedication. Arbitreradheres to a double-blind peer-review process that is rapid and fair, and also ensures a high quality of articles published. In so doing, Arbiter needs reviewers who can provide insightful and helpful comments on submitted manuscripts with a turn around time of about 4 weeks. Maintaining Arbitrer as a scientific journal of high quality depends on reviewers with a high level of expertise and an ability to be objective, fair, and insightful in their evaluation of manuscripts.</p>\r\n<p style=\"text-align: justify;\">If Arbitrer Editor-in-Chief has invited you to review a manuscript, please consider the following:</p>\r\n<ol>\r\n<li style=\"text-align: justify;\">Reviewing manuscript critically, but constructively and preparing detailed comments about the manuscript to help authors improve their work</li>\r\n<li style=\"text-align: justify;\">Reviewing multiple versions of a manuscript as necessary</li>\r\n<li style=\"text-align: justify;\">Providing all required information within established deadlines</li>\r\n<li style=\"text-align: justify;\">Making recommendations to the editor regarding the suitability of the manuscript for publication in the journal</li>\r\n<li style=\"text-align: justify;\">Declaring to the editor any potential conflicts of interest with respect to the authors or the content of a manuscript they are asked to review</li>\r\n<li style=\"text-align: justify;\">Reporting possible research misconducts</li>\r\n<li style=\"text-align: justify;\">Suggesting alternative reviewers in case they cannot review the manuscript for any reasons</li>\r\n<li style=\"text-align: justify;\">Treating the manuscript as a confidential document</li>\r\n<li style=\"text-align: justify;\">Not making any use of the work described in the manuscript</li>\r\n<li style=\"text-align: justify;\">Not communicating directly with authors, if somehow they identify the authors</li>\r\n<li style=\"text-align: justify;\">Not identifying themselves to authors</li>\r\n<li style=\"text-align: justify;\">Not passing on the assigned manuscript to another reviewer</li>\r\n<li style=\"text-align: justify;\">Ensuring that the manuscript is of high quality and original work</li>\r\n<li style=\"text-align: justify;\">Informing the editor if he/she finds the assigned manuscript is under consideration in any other publication to his/her knowledge</li>\r\n<li style=\"text-align: justify;\">Writing review report in English only</li>\r\n<li style=\"text-align: justify;\">Authoring a commentary for publication related to the reviewed manuscript.</li>\r\n</ol>\r\n<p style=\"text-align: justify;\">Here list of items that need to be reviewed:</p>\r\n<ol>\r\n<li><strong>Novelty of the topic</strong></li>\r\n<li><strong>Originality</strong></li>\r\n<li><strong>Scientific reliability</strong></li>\r\n<li><strong>Valuable contribution to the science</strong></li>\r\n<li><strong>Adding new aspects to the existed field of study</strong></li>\r\n<li><strong>Ethical aspects</strong></li>\r\n<li><strong>Structure of the article submitted and its relevance to authors’ guidelines</strong></li>\r\n<li><strong>References provided to substantiate the content</strong></li>\r\n<li><strong>Grammar, punctuation, and spelling</strong></li>\r\n<li style=\"text-align: justify;\"><strong>Scientific misconduct</strong></li>\r\n</ol>', 'string'),
(1, 'en_US', 'searchDescription', 'Tested Journal', 'string'),
(1, 'en_US', 'submissionChecklist', 'a:5:{i:0;a:2:{s:7:\"content\";s:165:\"The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).\";s:5:\"order\";s:1:\"1\";}i:1;a:2:{s:7:\"content\";s:82:\"The submission file is in OpenOffice, Microsoft Word, or RTF document file format.\";s:5:\"order\";s:1:\"2\";}i:2;a:2:{s:7:\"content\";s:60:\"Where available, URLs for the references have been provided.\";s:5:\"order\";s:1:\"3\";}i:3;a:2:{s:7:\"content\";s:239:\"The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.\";s:5:\"order\";s:1:\"4\";}i:4;a:2:{s:7:\"content\";s:99:\"The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.\";s:5:\"order\";s:1:\"5\";}}', 'object');

-- --------------------------------------------------------

--
-- Table structure for table `library_files`
--

CREATE TABLE `library_files` (
  `file_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `original_file_name` varchar(255) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `public_access` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `library_file_settings`
--

CREATE TABLE `library_file_settings` (
  `file_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `metadata_descriptions`
--

CREATE TABLE `metadata_descriptions` (
  `metadata_description_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL DEFAULT '0',
  `assoc_id` bigint(20) NOT NULL DEFAULT '0',
  `schema_namespace` varchar(255) NOT NULL,
  `schema_name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `seq` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `metadata_description_settings`
--

CREATE TABLE `metadata_description_settings` (
  `metadata_description_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `metrics`
--

CREATE TABLE `metrics` (
  `load_id` varchar(255) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `pkp_section_id` bigint(20) DEFAULT NULL,
  `assoc_object_type` bigint(20) DEFAULT NULL,
  `assoc_object_id` bigint(20) DEFAULT NULL,
  `submission_id` bigint(20) DEFAULT NULL,
  `representation_id` bigint(20) DEFAULT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `day` varchar(8) DEFAULT NULL,
  `month` varchar(6) DEFAULT NULL,
  `file_type` tinyint(4) DEFAULT NULL,
  `country_id` varchar(2) DEFAULT NULL,
  `region` varchar(2) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `metric_type` varchar(255) NOT NULL,
  `metric` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `metrics`
--

INSERT INTO `metrics` (`load_id`, `context_id`, `pkp_section_id`, `assoc_object_type`, `assoc_object_id`, `submission_id`, `representation_id`, `assoc_type`, `assoc_id`, `day`, `month`, `file_type`, `country_id`, `region`, `city`, `metric_type`, `metric`) VALUES
('usage_events_20190228.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20190228', '201902', NULL, NULL, NULL, NULL, 'ojs::counter', 20),
('usage_events_20190301.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20190301', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 11),
('usage_events_20190301.log', 1, NULL, NULL, NULL, NULL, NULL, 259, 1, '20190301', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 1),
('usage_events_20190302.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20190302', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 28),
('usage_events_20190302.log', 1, NULL, NULL, NULL, NULL, NULL, 259, 1, '20190302', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 11),
('usage_events_20190302.log', 1, 1, 259, 1, 2, 1, 515, 8, '20190302', '201903', 2, NULL, NULL, NULL, 'ojs::counter', 1),
('usage_events_20190302.log', 1, 1, 259, 1, 2, NULL, 1048585, 2, '20190302', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 16),
('usage_events_20190303.log', 1, NULL, NULL, NULL, NULL, NULL, 256, 1, '20190303', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 4),
('usage_events_20190303.log', 1, 1, 259, 1, 2, NULL, 1048585, 2, '20190303', '201903', NULL, NULL, NULL, NULL, 'ojs::counter', 7);

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menus`
--

CREATE TABLE `navigation_menus` (
  `navigation_menu_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `area_name` varchar(255) DEFAULT '',
  `title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menus`
--

INSERT INTO `navigation_menus` (`navigation_menu_id`, `context_id`, `area_name`, `title`) VALUES
(1, 0, 'user', 'User Navigation Menu'),
(2, 1, 'user', 'User Navigation Menu'),
(3, 1, 'primary', 'Primary Navigation Menu');

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menu_items`
--

CREATE TABLE `navigation_menu_items` (
  `navigation_menu_item_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `url` varchar(255) DEFAULT '',
  `path` varchar(255) DEFAULT '',
  `type` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menu_items`
--

INSERT INTO `navigation_menu_items` (`navigation_menu_item_id`, `context_id`, `url`, `path`, `type`) VALUES
(1, 0, NULL, NULL, 'NMI_TYPE_USER_REGISTER'),
(2, 0, NULL, NULL, 'NMI_TYPE_USER_LOGIN'),
(3, 0, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(4, 0, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(5, 0, NULL, NULL, 'NMI_TYPE_USER_PROFILE'),
(6, 0, NULL, NULL, 'NMI_TYPE_ADMINISTRATION'),
(7, 0, NULL, NULL, 'NMI_TYPE_USER_LOGOUT'),
(8, 1, NULL, NULL, 'NMI_TYPE_USER_REGISTER'),
(9, 1, NULL, NULL, 'NMI_TYPE_USER_LOGIN'),
(10, 1, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(11, 1, NULL, NULL, 'NMI_TYPE_USER_DASHBOARD'),
(12, 1, NULL, NULL, 'NMI_TYPE_USER_PROFILE'),
(13, 1, NULL, NULL, 'NMI_TYPE_ADMINISTRATION'),
(14, 1, NULL, NULL, 'NMI_TYPE_USER_LOGOUT'),
(15, 1, NULL, NULL, 'NMI_TYPE_CURRENT'),
(16, 1, NULL, NULL, 'NMI_TYPE_ARCHIVES'),
(17, 1, NULL, NULL, 'NMI_TYPE_ANNOUNCEMENTS'),
(18, 1, NULL, NULL, 'NMI_TYPE_ABOUT'),
(19, 1, NULL, NULL, 'NMI_TYPE_ABOUT'),
(20, 1, NULL, NULL, 'NMI_TYPE_SUBMISSIONS'),
(21, 1, NULL, NULL, 'NMI_TYPE_EDITORIAL_TEAM'),
(22, 1, NULL, NULL, 'NMI_TYPE_PRIVACY'),
(23, 1, NULL, NULL, 'NMI_TYPE_CONTACT'),
(24, 1, NULL, NULL, 'NMI_TYPE_SEARCH');

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menu_item_assignments`
--

CREATE TABLE `navigation_menu_item_assignments` (
  `navigation_menu_item_assignment_id` bigint(20) NOT NULL,
  `navigation_menu_id` bigint(20) NOT NULL,
  `navigation_menu_item_id` bigint(20) NOT NULL,
  `parent_id` bigint(20) DEFAULT NULL,
  `seq` bigint(20) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menu_item_assignments`
--

INSERT INTO `navigation_menu_item_assignments` (`navigation_menu_item_assignment_id`, `navigation_menu_id`, `navigation_menu_item_id`, `parent_id`, `seq`) VALUES
(1, 1, 1, 0, 0),
(2, 1, 2, 0, 1),
(3, 1, 3, 0, 2),
(4, 1, 4, 3, 0),
(5, 1, 5, 3, 1),
(6, 1, 6, 3, 2),
(7, 1, 7, 3, 3),
(8, 2, 8, 0, 0),
(9, 2, 9, 0, 1),
(10, 2, 10, 0, 2),
(11, 2, 11, 10, 0),
(12, 2, 12, 10, 1),
(13, 2, 13, 10, 2),
(14, 2, 14, 10, 3),
(15, 3, 15, 0, 0),
(16, 3, 16, 0, 1),
(17, 3, 17, 0, 2),
(18, 3, 18, 0, 3),
(19, 3, 19, 18, 0),
(20, 3, 20, 18, 1),
(21, 3, 21, 18, 2),
(22, 3, 22, 18, 3),
(23, 3, 23, 18, 4);

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menu_item_assignment_settings`
--

CREATE TABLE `navigation_menu_item_assignment_settings` (
  `navigation_menu_item_assignment_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menu_item_assignment_settings`
--

INSERT INTO `navigation_menu_item_assignment_settings` (`navigation_menu_item_assignment_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(0, 'ar_IQ', 'title', 'اسم الاتصال', 'string'),
(0, 'en_US', 'title', 'Contact', 'string'),
(0, 'id_ID', 'title', 'Kontak', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `navigation_menu_item_settings`
--

CREATE TABLE `navigation_menu_item_settings` (
  `navigation_menu_item_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `navigation_menu_item_settings`
--

INSERT INTO `navigation_menu_item_settings` (`navigation_menu_item_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, '', 'titleLocaleKey', 'navigation.register', 'string'),
(1, 'ar_IQ', 'title', 'التسجيل', 'string'),
(1, 'en_US', 'title', 'Register', 'string'),
(1, 'id_ID', 'title', 'Daftar', 'string'),
(2, '', 'titleLocaleKey', 'navigation.login', 'string'),
(2, 'ar_IQ', 'title', 'الدخول', 'string'),
(2, 'en_US', 'title', 'Login', 'string'),
(2, 'id_ID', 'title', 'Login', 'string'),
(3, '', 'titleLocaleKey', '{$loggedInUsername}', 'string'),
(3, 'ar_IQ', 'title', '{$loggedInUsername}', 'string'),
(3, 'en_US', 'title', '{$loggedInUsername}', 'string'),
(3, 'id_ID', 'title', '{$loggedInUsername}', 'string'),
(4, '', 'titleLocaleKey', 'navigation.dashboard', 'string'),
(4, 'ar_IQ', 'title', 'لوحة التحكم الرئيسية', 'string'),
(4, 'en_US', 'title', 'Dashboard', 'string'),
(4, 'id_ID', 'title', 'Dashboard', 'string'),
(5, '', 'titleLocaleKey', 'common.viewProfile', 'string'),
(5, 'ar_IQ', 'title', 'معاينة الملف الشخصي', 'string'),
(5, 'en_US', 'title', 'View Profile', 'string'),
(5, 'id_ID', 'title', 'common.viewProfile', 'string'),
(6, '', 'titleLocaleKey', 'navigation.admin', 'string'),
(6, 'ar_IQ', 'title', 'الإدارة', 'string'),
(6, 'en_US', 'title', 'Administration', 'string'),
(6, 'id_ID', 'title', 'navigation.admin', 'string'),
(7, '', 'titleLocaleKey', 'user.logOut', 'string'),
(7, 'ar_IQ', 'title', 'الخروج', 'string'),
(7, 'en_US', 'title', 'Logout', 'string'),
(7, 'id_ID', 'title', 'Log Out', 'string'),
(8, '', 'titleLocaleKey', 'navigation.register', 'string'),
(8, 'ar_IQ', 'title', 'التسجيل', 'string'),
(8, 'en_US', 'title', 'Register', 'string'),
(8, 'id_ID', 'title', 'Daftar', 'string'),
(9, '', 'titleLocaleKey', 'navigation.login', 'string'),
(9, 'ar_IQ', 'title', 'الدخول', 'string'),
(9, 'en_US', 'title', 'Login', 'string'),
(9, 'id_ID', 'title', 'Login', 'string'),
(10, '', 'titleLocaleKey', '{$loggedInUsername}', 'string'),
(10, 'ar_IQ', 'title', '{$loggedInUsername}', 'string'),
(10, 'en_US', 'title', '{$loggedInUsername}', 'string'),
(10, 'id_ID', 'title', '{$loggedInUsername}', 'string'),
(11, '', 'titleLocaleKey', 'navigation.dashboard', 'string'),
(11, 'ar_IQ', 'title', 'لوحة التحكم الرئيسية', 'string'),
(11, 'en_US', 'title', 'Dashboard', 'string'),
(11, 'id_ID', 'title', 'Dashboard', 'string'),
(12, '', 'titleLocaleKey', 'common.viewProfile', 'string'),
(12, 'ar_IQ', 'title', 'معاينة الملف الشخصي', 'string'),
(12, 'en_US', 'title', 'View Profile', 'string'),
(12, 'id_ID', 'title', 'common.viewProfile', 'string'),
(13, '', 'titleLocaleKey', 'navigation.admin', 'string'),
(13, 'ar_IQ', 'title', 'الإدارة', 'string'),
(13, 'en_US', 'title', 'Administration', 'string'),
(13, 'id_ID', 'title', 'navigation.admin', 'string'),
(14, '', 'titleLocaleKey', 'user.logOut', 'string'),
(14, 'ar_IQ', 'title', 'الخروج', 'string'),
(14, 'en_US', 'title', 'Logout', 'string'),
(14, 'id_ID', 'title', 'Log Out', 'string'),
(15, '', 'titleLocaleKey', 'navigation.current', 'string'),
(15, 'ar_IQ', 'title', 'الحالي', 'string'),
(15, 'en_US', 'title', 'Current', 'string'),
(15, 'id_ID', 'title', 'Terkini', 'string'),
(16, '', 'titleLocaleKey', 'navigation.archives', 'string'),
(16, 'ar_IQ', 'title', 'المحفوظات', 'string'),
(16, 'en_US', 'title', 'Archives', 'string'),
(16, 'id_ID', 'title', 'navigation.archives', 'string'),
(17, '', 'titleLocaleKey', 'manager.announcements', 'string'),
(17, 'ar_IQ', 'title', 'الإعلانات', 'string'),
(17, 'en_US', 'title', 'Announcements', 'string'),
(17, 'id_ID', 'title', 'Pengumuman', 'string'),
(18, '', 'titleLocaleKey', 'navigation.about', 'string'),
(18, 'ar_IQ', 'title', 'عن', 'string'),
(18, 'en_US', 'title', 'About', 'string'),
(18, 'id_ID', 'title', 'Tentang Kami', 'string'),
(19, '', 'titleLocaleKey', 'about.aboutContext', 'string'),
(19, 'ar_IQ', 'title', 'عن المجلة', 'string'),
(19, 'en_US', 'title', 'About the Journal', 'string'),
(19, 'id_ID', 'title', 'Tentang Jurnal Ini', 'string'),
(20, '', 'titleLocaleKey', 'navigation.submissions', 'string'),
(20, 'ar_IQ', 'title', 'طلبات النشر', 'string'),
(20, 'en_US', 'title', 'Submissions', 'string'),
(20, 'id_ID', 'title', 'navigation.submissions', 'string'),
(21, '', 'titleLocaleKey', 'about.editorialTeam', 'string'),
(21, 'ar_IQ', 'title', 'هيئة التحرير', 'string'),
(21, 'en_US', 'title', 'Editorial Team', 'string'),
(21, 'id_ID', 'title', 'Dewan Editor', 'string'),
(22, '', 'titleLocaleKey', 'manager.setup.privacyStatement', 'string'),
(22, 'ar_IQ', 'title', 'بيان الخصوصية', 'string'),
(22, 'en_US', 'title', 'Privacy Statement', 'string'),
(22, 'id_ID', 'title', 'manager.setup.privacyStatement', 'string'),
(23, '', 'titleLocaleKey', 'about.contact', 'string'),
(23, 'ar_IQ', 'title', 'اسم الاتصال', 'string'),
(23, 'en_US', 'title', 'Contact', 'string'),
(23, 'id_ID', 'title', 'Kontak', 'string'),
(24, '', 'titleLocaleKey', 'common.search', 'string'),
(24, 'ar_IQ', 'title', 'إبحث', 'string'),
(24, 'en_US', 'title', 'Search', 'string'),
(24, 'id_ID', 'title', 'Cari', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `note_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `contents` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`note_id`, `assoc_type`, `assoc_id`, `user_id`, `date_created`, `date_modified`, `title`, `contents`) VALUES
(1, 1048586, 1, 5, '2019-03-02 18:51:38', '2019-03-02 18:51:38', '[tested] Editorial Assignment', '<p>Editor editor:<br> <br> The submission, \"PROGRAMMING LANGUAGE,\" to Tested has been assigned to you to see through the editorial process in your role as Section Editor.<br> <br> Submission URL: <a href=\'http://localhost/journal/index.php/tested/workflow/access/2\' class=\'submissionUrl-style-class\'>http://localhost/journal/index.php/tested/workflow/access/2</a><br> Username: editor<br> <br> Thank you,<br> Editor editor<br>editor@staipi.ac.id</p><br/><p><br> ________________________________________________________________________<br> <a href=\"http://localhost/journal/index.php/tested\">Tested</a></p>'),
(2, 1048586, 2, 5, '2019-03-02 18:55:18', '2019-03-02 18:55:18', '[tested] Editorial Assignment', '<p>Editor editor:<br> <br> The submission, \"PROGRAMMING LANGUAGE,\" to Tested has been assigned to you to see through the editorial process in your role as Section Editor.<br> <br> Submission URL: <a href=\'http://localhost/journal/index.php/tested/workflow/access/2\' class=\'submissionUrl-style-class\'>http://localhost/journal/index.php/tested/workflow/access/2</a><br> Username: editor<br> <br> Thank you,<br> Editor editor<br>editor@staipi.ac.id</p><br/><p><br> ________________________________________________________________________<br> <a href=\"http://localhost/journal/index.php/tested\">Tested</a></p>'),
(3, 1048586, 3, 5, '2019-03-02 19:24:28', '2019-03-02 19:24:28', '[tested] Editorial Assignment', '<p>Editor editor:<br> <br> The submission, \"PROGRAMMING LANGUAGE,\" to Tested has been assigned to you to see through the editorial process in your role as Section Editor.<br> <br> Submission URL: <a href=\'http://localhost/journal/index.php/tested/workflow/access/2\' class=\'submissionUrl-style-class\'>http://localhost/journal/index.php/tested/workflow/access/2</a><br> Username: editor<br> <br> Thank you,<br> Editor editor<br>editor@staipi.ac.id</p><br/><p><br> ________________________________________________________________________<br> <a href=\"http://localhost/journal/index.php/tested\">Tested</a></p>'),
(4, 1048586, 4, 5, '2019-03-02 19:25:43', '2019-03-02 19:25:43', '[tested] Editorial Assignment', '<p>Editor editor:<br> <br> The submission, \"PROGRAMMING LANGUAGE,\" to Tested has been assigned to you to see through the editorial process in your role as Section Editor.<br> <br> Submission URL: <a href=\'http://localhost/journal/index.php/tested/workflow/access/2\' class=\'submissionUrl-style-class\'>http://localhost/journal/index.php/tested/workflow/access/2</a><br> Username: editor<br> <br> Thank you,<br> Editor editor<br>editor@staipi.ac.id</p><br/><p><br> ________________________________________________________________________<br> <a href=\"http://localhost/journal/index.php/tested\">Tested</a></p>'),
(5, 1048586, 5, 5, '2019-03-02 19:47:55', '2019-03-02 19:47:55', '[tested] Editorial Assignment', '<p>Editor editor:<br> <br> The submission, \"PROGRAMMING LANGUAGE,\" to Tested has been assigned to you to see through the editorial process in your role as Section Editor.<br> <br> Submission URL: <a href=\'http://localhost/journal/index.php/tested/workflow/access/2\' class=\'submissionUrl-style-class\'>http://localhost/journal/index.php/tested/workflow/access/2</a><br> Username: editor<br> <br> Thank you,<br> Editor editor<br>editor@staipi.ac.id</p><br/><p><br> ________________________________________________________________________<br> <a href=\"http://localhost/journal/index.php/tested\">Tested</a></p>');

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `notification_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `level` bigint(20) NOT NULL,
  `type` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_read` datetime DEFAULT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`notification_id`, `context_id`, `user_id`, `level`, `type`, `date_created`, `date_read`, `assoc_type`, `assoc_id`) VALUES
(45, 1, 0, 3, 16777220, '2019-03-01 22:13:13', NULL, 1048585, 1),
(46, 1, 0, 3, 16777222, '2019-03-01 22:13:13', NULL, 1048585, 1),
(47, 1, 0, 3, 16777223, '2019-03-01 22:13:13', NULL, 1048585, 1),
(48, 1, 0, 3, 16777224, '2019-03-01 22:13:13', NULL, 1048585, 1),
(49, 1, 1, 2, 16777217, '2019-03-01 22:13:14', NULL, 1048585, 1),
(50, 1, 1, 3, 16777247, '2019-03-01 22:13:16', '2019-03-01 22:33:39', 1048585, 1),
(51, 1, 2, 2, 16777217, '2019-03-01 22:13:17', NULL, 1048585, 1),
(52, 1, 2, 3, 16777247, '2019-03-01 22:13:18', '2019-03-02 18:37:55', 1048585, 1),
(53, 1, 0, 2, 16777243, '2019-03-01 22:13:19', NULL, 1048585, 1),
(54, 1, 0, 2, 16777245, '2019-03-01 22:13:19', NULL, 1048585, 1),
(61, 1, 1, 2, 16777217, '2019-03-01 22:45:06', NULL, 1048585, 2),
(63, 1, 2, 2, 16777217, '2019-03-01 22:45:09', NULL, 1048585, 2),
(65, 1, 0, 2, 16777243, '2019-03-01 22:45:11', NULL, 1048585, 2),
(66, 1, 0, 2, 16777245, '2019-03-01 22:45:11', NULL, 1048585, 2),
(68, 1, 5, 3, 16777255, '2019-03-02 18:51:37', '2019-03-02 19:38:56', 1048585, 2),
(77, 1, 0, 2, 16777236, '2019-03-02 18:59:04', '2019-03-02 18:59:17', 523, 1),
(81, 1, 5, 2, 16777219, '2019-03-02 19:15:07', NULL, 517, 1),
(82, 1, 0, 2, 16777236, '2019-03-02 19:20:56', '2019-03-02 19:21:06', 523, 2),
(116, 1, 3, 2, 16777235, '2019-03-02 19:54:41', NULL, 1048585, 2),
(119, 1, 5, 2, 268435478, '2019-03-02 19:58:52', '2019-03-02 19:58:54', 1048585, 2),
(120, 1, 1, 2, 268435477, '2019-03-02 20:02:29', NULL, 0, 0),
(121, 1, 3, 2, 268435477, '2019-03-02 20:02:31', NULL, 0, 0),
(122, 1, 5, 2, 268435477, '2019-03-02 20:02:32', NULL, 0, 0),
(123, 1, 2, 2, 268435477, '2019-03-02 20:02:33', NULL, 0, 0),
(124, 1, 4, 2, 268435477, '2019-03-02 20:02:34', NULL, 0, 0),
(126, 1, 1, 2, 8, '2019-03-02 20:14:03', NULL, 522, 1),
(127, 1, 3, 2, 8, '2019-03-02 20:14:04', NULL, 522, 1),
(128, 1, 5, 2, 8, '2019-03-02 20:14:05', NULL, 522, 1),
(129, 1, 2, 2, 8, '2019-03-02 20:14:06', NULL, 522, 1),
(130, 1, 4, 2, 8, '2019-03-02 20:14:07', NULL, 522, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification_mail_list`
--

CREATE TABLE `notification_mail_list` (
  `notification_mail_list_id` bigint(20) NOT NULL,
  `email` varchar(90) NOT NULL,
  `confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `token` varchar(40) NOT NULL,
  `context` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification_settings`
--

CREATE TABLE `notification_settings` (
  `notification_id` bigint(20) NOT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `setting_name` varchar(64) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `notification_subscription_settings`
--

CREATE TABLE `notification_subscription_settings` (
  `setting_id` bigint(20) NOT NULL,
  `setting_name` varchar(64) NOT NULL,
  `setting_value` text,
  `user_id` bigint(20) NOT NULL,
  `context` bigint(20) NOT NULL,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `oai_resumption_tokens`
--

CREATE TABLE `oai_resumption_tokens` (
  `token` varchar(32) NOT NULL,
  `expire` bigint(20) NOT NULL,
  `record_offset` int(11) NOT NULL,
  `params` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `plugin_settings`
--

CREATE TABLE `plugin_settings` (
  `plugin_name` varchar(80) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `setting_name` varchar(80) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `plugin_settings`
--

INSERT INTO `plugin_settings` (`plugin_name`, `context_id`, `setting_name`, `setting_value`, `setting_type`) VALUES
('acronplugin', 0, 'crontab', 'a:6:{i:0;a:3:{s:9:\"className\";s:43:\"plugins.generic.usageStats.UsageStatsLoader\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:1:{i:0;s:9:\"autoStage\";}}i:1;a:3:{s:9:\"className\";s:48:\"plugins.importexport.crossref.CrossrefInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:2;a:3:{s:9:\"className\";s:48:\"plugins.importexport.datacite.DataciteInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:3;a:3:{s:9:\"className\";s:40:\"plugins.importexport.doaj.DOAJInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:4;a:3:{s:9:\"className\";s:42:\"plugins.importexport.medra.MedraInfoSender\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}i:5;a:3:{s:9:\"className\";s:35:\"lib.pkp.classes.task.ReviewReminder\";s:9:\"frequency\";a:1:{s:4:\"hour\";i:24;}s:4:\"args\";a:0:{}}}', 'object'),
('acronplugin', 0, 'enabled', '1', 'bool'),
('bootstrapthreethemeplugin', 1, 'bootstrapTheme', 'darkly', 'string'),
('bootstrapthreethemeplugin', 1, 'enabled', '1', 'bool'),
('classicthemeplugin', 1, 'enabled', '1', 'bool'),
('criticaltimesthemeplugin', 1, 'enabled', '1', 'bool'),
('customblockmanagerplugin', 1, 'blocks', 'a:2:{i:0;s:6:\"detail\";i:1;s:8:\"visitors\";}', 'object'),
('customblockmanagerplugin', 1, 'enabled', '1', 'bool'),
('defaultchildthemeplugin', 1, 'enabled', '1', 'bool'),
('defaultmanuscriptchildthemeplugin', 1, 'enabled', '1', 'bool'),
('defaultthemeplugin', 0, 'enabled', '1', 'bool'),
('defaultthemeplugin', 1, 'baseColour', '#09c244', 'text'),
('defaultthemeplugin', 1, 'enabled', '1', 'bool'),
('detail', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:604:\"<p>Journal Detail</p>\r\n<div class=\"content\">\r\n<ul>\r\n<li><a title=\"Editorial Team \" href=\"http://localhost/journal/index.php/tested/about/editorialTeam\">Editorial Team </a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/about/submissions\"> Guiedline and Submission</a></li>\r\n<li><a title=\"Focus and Scope \" href=\"http://localhost/journal/index.php/tested/Focus\"> Focus and Scope </a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/librarians\"> Publication Ethic </a></li>\r\n<li><a href=\"#\"> Peer Review </a></li>\r\n<li><a href=\"#\"> Partnership</a></li>\r\n</ul>\r\n</div>\";}', 'object'),
('detail', 1, 'context', '1', 'int'),
('detail', 1, 'enabled', '1', 'bool'),
('detail', 1, 'seq', '1', 'int'),
('developedbyblockplugin', 0, 'context', '1', 'int'),
('developedbyblockplugin', 0, 'enabled', '0', 'bool'),
('developedbyblockplugin', 0, 'seq', '0', 'int'),
('developedbyblockplugin', 1, 'context', '1', 'int'),
('developedbyblockplugin', 1, 'enabled', '0', 'bool'),
('developedbyblockplugin', 1, 'seq', '7', 'int'),
('dublincoremetaplugin', 1, 'enabled', '1', 'bool'),
('googlescholarplugin', 1, 'enabled', '1', 'bool'),
('healthsciencesthemeplugin', 1, 'enabled', '1', 'bool'),
('htmlarticlegalleyplugin', 1, 'enabled', '1', 'bool'),
('immersionthemeplugin', 1, 'enabled', '1', 'bool'),
('informationblockplugin', 1, 'context', '1', 'int'),
('informationblockplugin', 1, 'enabled', '1', 'bool'),
('informationblockplugin', 1, 'seq', '6', 'int'),
('journal detail', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:747:\"<div class=\"pkp_block block_information\">\r\n<div class=\"content\">\r\n<ul>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/readers\"> Editorial Team</a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/authors\"> Guideline and Submission</a></li>\r\n<li><a title=\"Focus and Scope\" href=\"http://localhost/journal/index.php/tested/Focus\"> Focus and Scope</a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/librarians\"> Publication Ethic</a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/librarians\"> Peer Preview</a></li>\r\n<li><a href=\"http://localhost/journal/index.php/tested/information/librarians\">Partnership</a></li>\r\n</ul>\r\n</div>\r\n<p>&nbsp;</p>\r\n</div>\";}', 'object'),
('keywordcloudblockplugin', 1, 'context', '1', 'int'),
('keywordcloudblockplugin', 1, 'enabled', '1', 'bool'),
('keywordcloudblockplugin', 1, 'seq', '4', 'int'),
('languagetoggleblockplugin', 0, 'context', '1', 'int'),
('languagetoggleblockplugin', 0, 'enabled', '1', 'bool'),
('languagetoggleblockplugin', 0, 'seq', '4', 'int'),
('languagetoggleblockplugin', 1, 'context', '1', 'int'),
('languagetoggleblockplugin', 1, 'enabled', '1', 'bool'),
('languagetoggleblockplugin', 1, 'seq', '2', 'int'),
('lensgalleyplugin', 1, 'enabled', '1', 'bool'),
('makesubmissionblockplugin', 1, 'context', '1', 'int'),
('makesubmissionblockplugin', 1, 'enabled', '0', 'bool'),
('makesubmissionblockplugin', 1, 'seq', '6', 'int'),
('manualpayment', 1, 'manualInstructions', 'For Payment method use this page.', 'string'),
('oldgreggthemeplugin', 1, 'enabled', '1', 'bool'),
('pdfjsviewerplugin', 1, 'enabled', '1', 'bool'),
('resolverplugin', 1, 'enabled', '1', 'bool'),
('shariffplugin', 1, 'enabled', '1', 'bool'),
('shariffplugin', 1, 'selectedOrientation', 'horizontal', 'string'),
('shariffplugin', 1, 'selectedPosition', 'footer', 'string'),
('shariffplugin', 1, 'selectedServices', 'a:20:{i:0;s:7:\"twitter\";i:1;s:8:\"facebook\";i:2;s:10:\"googleplus\";i:3;s:8:\"linkedin\";i:4;s:9:\"pinterest\";i:5;s:4:\"xing\";i:6;s:8:\"whatsapp\";i:7;s:7:\"addthis\";i:8;s:6:\"tumblr\";i:9;s:6:\"flattr\";i:10;s:8:\"diaspora\";i:11;s:6:\"reddit\";i:12;s:11:\"stumbleupon\";i:13;s:7:\"threema\";i:14;s:5:\"weibo\";i:15;s:13:\"tencent-weibo\";i:16;s:5:\"qzone\";i:17;s:4:\"mail\";i:18;s:5:\"print\";i:19;s:4:\"info\";}', 'object'),
('shariffplugin', 1, 'selectedTheme', 'standard', 'string'),
('staticpagesplugin', 1, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 1, 'context', '1', 'int'),
('subscriptionblockplugin', 1, 'enabled', '1', 'bool'),
('subscriptionblockplugin', 1, 'seq', '3', 'int'),
('tinymceplugin', 0, 'enabled', '1', 'bool'),
('tinymceplugin', 1, 'enabled', '1', 'bool'),
('usageeventplugin', 0, 'enabled', '1', 'bool'),
('usageeventplugin', 0, 'uniqueSiteId', '5c777a5b5329c', 'string'),
('usagestatsplugin', 0, 'accessLogFileParseRegex', '/^(?P<ip>\\S+) \\S+ \\S+ \\[(?P<date>.*?)\\] \"\\S+ (?P<url>\\S+).*?\" (?P<returnCode>\\S+) \\S+ \".*?\" \"(?P<userAgent>.*?)\"/', 'string'),
('usagestatsplugin', 0, 'chartType', 'bar', 'string'),
('usagestatsplugin', 0, 'createLogFiles', '1', 'bool'),
('usagestatsplugin', 0, 'datasetMaxCount', '4', 'string'),
('usagestatsplugin', 0, 'enabled', '1', 'bool'),
('usagestatsplugin', 0, 'optionalColumns', 'a:2:{i:0;s:4:\"city\";i:1;s:6:\"region\";}', 'object'),
('visitors', 1, 'blockContent', 'a:1:{s:5:\"en_US\";s:243:\"<p><a href=\"https://info.flagcounter.com/x7DX\"><img src=\"https://s04.flagcounter.com/count2/x7DX/bg_FFFFFF/txt_000000/border_CCCCCC/columns_3/maxflags_12/viewers_0/labels_1/pageviews_1/flags_0/percent_0/\" alt=\"Flag Counter\" border=\"0\"></a></p>\";}', 'object'),
('visitors', 1, 'context', '1', 'int'),
('visitors', 1, 'enabled', '1', 'bool'),
('visitors', 1, 'seq', '5', 'int'),
('webfeedblockplugin', 1, 'context', '1', 'int'),
('webfeedblockplugin', 1, 'enabled', '0', 'bool'),
('webfeedblockplugin', 1, 'seq', '6', 'int'),
('webfeedplugin', 1, 'displayItems', '1', 'bool'),
('webfeedplugin', 1, 'displayPage', 'homepage', 'string'),
('webfeedplugin', 1, 'enabled', '1', 'bool');

-- --------------------------------------------------------

--
-- Table structure for table `published_submissions`
--

CREATE TABLE `published_submissions` (
  `published_submission_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `issue_id` bigint(20) NOT NULL,
  `date_published` datetime DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `access_status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `published_submissions`
--

INSERT INTO `published_submissions` (`published_submission_id`, `submission_id`, `issue_id`, `date_published`, `seq`, `access_status`) VALUES
(1, 2, 1, '2019-03-02 19:58:49', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `queries`
--

CREATE TABLE `queries` (
  `query_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `stage_id` tinyint(4) NOT NULL DEFAULT '1',
  `seq` double NOT NULL DEFAULT '0',
  `date_posted` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `closed` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `queries`
--

INSERT INTO `queries` (`query_id`, `assoc_type`, `assoc_id`, `stage_id`, `seq`, `date_posted`, `date_modified`, `closed`) VALUES
(1, 1048585, 2, 1, 1, NULL, NULL, 0),
(2, 1048585, 2, 1, 2, NULL, NULL, 0),
(3, 1048585, 2, 3, 3, NULL, NULL, 0),
(4, 1048585, 2, 3, 4, NULL, NULL, 0),
(5, 1048585, 2, 3, 5, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `query_participants`
--

CREATE TABLE `query_participants` (
  `query_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `query_participants`
--

INSERT INTO `query_participants` (`query_id`, `user_id`) VALUES
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5);

-- --------------------------------------------------------

--
-- Table structure for table `queued_payments`
--

CREATE TABLE `queued_payments` (
  `queued_payment_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `expiry_date` date DEFAULT NULL,
  `payment_data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_assignments`
--

CREATE TABLE `review_assignments` (
  `review_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `reviewer_id` bigint(20) NOT NULL,
  `competing_interests` text,
  `recommendation` tinyint(4) DEFAULT NULL,
  `date_assigned` datetime DEFAULT NULL,
  `date_notified` datetime DEFAULT NULL,
  `date_confirmed` datetime DEFAULT NULL,
  `date_completed` datetime DEFAULT NULL,
  `date_acknowledged` datetime DEFAULT NULL,
  `date_due` datetime DEFAULT NULL,
  `date_response_due` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `reminder_was_automatic` tinyint(4) NOT NULL DEFAULT '0',
  `declined` tinyint(4) NOT NULL DEFAULT '0',
  `replaced` tinyint(4) NOT NULL DEFAULT '0',
  `reviewer_file_id` bigint(20) DEFAULT NULL,
  `date_rated` datetime DEFAULT NULL,
  `date_reminded` datetime DEFAULT NULL,
  `quality` tinyint(4) DEFAULT NULL,
  `review_round_id` bigint(20) DEFAULT NULL,
  `stage_id` tinyint(4) NOT NULL DEFAULT '1',
  `review_method` tinyint(4) NOT NULL DEFAULT '1',
  `round` tinyint(4) NOT NULL DEFAULT '1',
  `step` tinyint(4) NOT NULL DEFAULT '1',
  `review_form_id` bigint(20) DEFAULT NULL,
  `unconsidered` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review_assignments`
--

INSERT INTO `review_assignments` (`review_id`, `submission_id`, `reviewer_id`, `competing_interests`, `recommendation`, `date_assigned`, `date_notified`, `date_confirmed`, `date_completed`, `date_acknowledged`, `date_due`, `date_response_due`, `last_modified`, `reminder_was_automatic`, `declined`, `replaced`, `reviewer_file_id`, `date_rated`, `date_reminded`, `quality`, `review_round_id`, `stage_id`, `review_method`, `round`, `step`, `review_form_id`, `unconsidered`) VALUES
(1, 2, 4, NULL, 1, '2019-03-02 19:01:57', '2019-03-02 19:02:00', '2019-03-02 19:07:30', '2019-03-02 19:15:08', NULL, '2019-03-30 00:00:00', '2019-03-23 00:00:00', '2019-03-02 19:15:08', 0, 0, 0, NULL, '2019-03-02 19:27:16', NULL, 5, 1, 3, 2, 1, 4, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `review_files`
--

CREATE TABLE `review_files` (
  `review_id` bigint(20) NOT NULL,
  `file_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review_files`
--

INSERT INTO `review_files` (`review_id`, `file_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `review_forms`
--

CREATE TABLE `review_forms` (
  `review_form_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL,
  `seq` double DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_form_elements`
--

CREATE TABLE `review_form_elements` (
  `review_form_element_id` bigint(20) NOT NULL,
  `review_form_id` bigint(20) NOT NULL,
  `seq` double DEFAULT NULL,
  `element_type` bigint(20) DEFAULT NULL,
  `required` tinyint(4) DEFAULT NULL,
  `included` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_form_element_settings`
--

CREATE TABLE `review_form_element_settings` (
  `review_form_element_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_form_responses`
--

CREATE TABLE `review_form_responses` (
  `review_form_element_id` bigint(20) NOT NULL,
  `review_id` bigint(20) NOT NULL,
  `response_type` varchar(6) DEFAULT NULL,
  `response_value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_form_settings`
--

CREATE TABLE `review_form_settings` (
  `review_form_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `review_rounds`
--

CREATE TABLE `review_rounds` (
  `review_round_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `stage_id` bigint(20) DEFAULT NULL,
  `round` tinyint(4) NOT NULL,
  `review_revision` bigint(20) DEFAULT NULL,
  `status` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review_rounds`
--

INSERT INTO `review_rounds` (`review_round_id`, `submission_id`, `stage_id`, `round`, `review_revision`, `status`) VALUES
(1, 2, 3, 1, NULL, 2),
(2, 2, 3, 2, NULL, 4);

-- --------------------------------------------------------

--
-- Table structure for table `review_round_files`
--

CREATE TABLE `review_round_files` (
  `submission_id` bigint(20) NOT NULL,
  `review_round_id` bigint(20) NOT NULL,
  `stage_id` tinyint(4) NOT NULL,
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `review_round_files`
--

INSERT INTO `review_round_files` (`submission_id`, `review_round_id`, `stage_id`, `file_id`, `revision`) VALUES
(2, 1, 3, 3, 1),
(2, 1, 3, 4, 1),
(2, 2, 3, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rt_contexts`
--

CREATE TABLE `rt_contexts` (
  `context_id` bigint(20) NOT NULL,
  `version_id` bigint(20) NOT NULL,
  `title` varchar(120) NOT NULL,
  `abbrev` varchar(32) NOT NULL,
  `description` text,
  `cited_by` tinyint(4) NOT NULL DEFAULT '0',
  `author_terms` tinyint(4) NOT NULL DEFAULT '0',
  `define_terms` tinyint(4) NOT NULL DEFAULT '0',
  `geo_terms` tinyint(4) NOT NULL DEFAULT '0',
  `seq` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rt_searches`
--

CREATE TABLE `rt_searches` (
  `search_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `title` varchar(120) NOT NULL,
  `description` text,
  `url` text,
  `search_url` text,
  `search_post` text,
  `seq` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rt_versions`
--

CREATE TABLE `rt_versions` (
  `version_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `version_key` varchar(40) NOT NULL,
  `locale` varchar(14) DEFAULT 'en_US',
  `title` varchar(120) NOT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scheduled_tasks`
--

CREATE TABLE `scheduled_tasks` (
  `class_name` varchar(255) NOT NULL,
  `last_run` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scheduled_tasks`
--

INSERT INTO `scheduled_tasks` (`class_name`, `last_run`) VALUES
('lib.pkp.classes.task.ReviewReminder', '2019-03-04 02:56:39'),
('plugins.generic.usageStats.UsageStatsLoader', '2019-03-04 02:56:20'),
('plugins.importexport.crossref.CrossrefInfoSender', '2019-03-04 02:56:33'),
('plugins.importexport.datacite.DataciteInfoSender', '2019-03-04 02:56:37'),
('plugins.importexport.doaj.DOAJInfoSender', '2019-03-04 02:56:38'),
('plugins.importexport.medra.MedraInfoSender', '2019-03-04 02:56:38');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `section_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `review_form_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `editor_restricted` tinyint(4) NOT NULL DEFAULT '0',
  `meta_indexed` tinyint(4) NOT NULL DEFAULT '0',
  `meta_reviewed` tinyint(4) NOT NULL DEFAULT '1',
  `abstracts_not_required` tinyint(4) NOT NULL DEFAULT '0',
  `hide_title` tinyint(4) NOT NULL DEFAULT '0',
  `hide_author` tinyint(4) NOT NULL DEFAULT '0',
  `abstract_word_count` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`section_id`, `journal_id`, `review_form_id`, `seq`, `editor_restricted`, `meta_indexed`, `meta_reviewed`, `abstracts_not_required`, `hide_title`, `hide_author`, `abstract_word_count`) VALUES
(1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0),
(2, 1, 0, 2, 0, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `section_editors`
--

CREATE TABLE `section_editors` (
  `context_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `section_settings`
--

CREATE TABLE `section_settings` (
  `section_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section_settings`
--

INSERT INTO `section_settings` (`section_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'abbrev', 'ART', 'string'),
(1, 'en_US', 'policy', '', 'string'),
(1, 'en_US', 'title', 'Articles', 'string'),
(2, 'en_US', 'abbrev', 'RV', 'string'),
(2, 'en_US', 'identifyType', '', 'string'),
(2, 'en_US', 'policy', '', 'string'),
(2, 'en_US', 'title', 'Review', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `ip_address` varchar(39) NOT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `created` bigint(20) NOT NULL DEFAULT '0',
  `last_used` bigint(20) NOT NULL DEFAULT '0',
  `remember` tinyint(4) NOT NULL DEFAULT '0',
  `data` text,
  `domain` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `ip_address`, `user_agent`, `created`, `last_used`, `remember`, `data`, `domain`) VALUES
('1i3ku0kpqbpas6ddlgrf8idto5', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551685813, 1551685813, 0, '', 'localhost'),
('44lers0jcn7vm3hvg29m3p9l54', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551665109, 1551665109, 0, '', 'localhost'),
('48co7kqfvbb4m1gjmoues76edq', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551685534, 1551698789, 0, 'csrf|a:2:{s:9:\"timestamp\";i:1551698792;s:5:\"token\";s:32:\"cf7b61ea180db29fcd6f1a17a4cc367d\";}username|s:5:\"admin\";', 'localhost'),
('7am4joqromtj1o764nsmkgkfuh', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763', 1551664918, 1551664918, 0, '', 'localhost'),
('arofo1m9asc6tpgohpe7ue7grg', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551685808, 1551685808, 0, '', 'localhost'),
('bgvnj3o2gthbv0p5en3ad271gq', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763', 1551664920, 1551664920, 0, '', 'localhost'),
('f3sdbal02mpg9qb4tqdkoehsp5', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763', 1551664918, 1551664918, 0, '', 'localhost'),
('l9acp47198avag7datfg3fhiof', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551330743, 1551335292, 1, 'csrf|a:2:{s:9:\"timestamp\";i:1551335286;s:5:\"token\";s:32:\"ed47a6630d0af163fbbec414cf229fd8\";}username|s:5:\"admin\";userId|s:1:\"1\";', 'localhost'),
('pppa135gid9fne54kksrra3iaq', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551685809, 1551685809, 0, 'csrf|a:2:{s:9:\"timestamp\";i:1551685812;s:5:\"token\";s:32:\"da16c7cd9e7138445e12c5d6690e8439\";}', 'localhost'),
('qimarso47f4nkcgbhahse7q39c', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551665112, 1551665112, 0, '', 'localhost'),
('rlc50rfpa7mpc7n5vurrjadg6r', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551664573, 1551684880, 1, 'csrf|a:2:{s:9:\"timestamp\";i:1551684879;s:5:\"token\";s:32:\"9cceb51c78d85f45e56086162cbf209a\";}username|s:5:\"admin\";userId|s:1:\"1\";', 'localhost'),
('tht0ku0usgceg7nu4tc4vpagfd', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763', 1551664938, 1551664938, 0, '', 'localhost'),
('u2uocepe3u5mneces8hhigk4s3', NULL, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:65.0) Gecko/20100101 Firefox/65.0', 1551665110, 1551665110, 0, 'csrf|a:2:{s:9:\"timestamp\";i:1551665111;s:5:\"token\";s:32:\"70c7fdc93e45a0f137df0584b867fa81\";}', 'localhost');

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `redirect` bigint(20) NOT NULL DEFAULT '0',
  `primary_locale` varchar(14) NOT NULL,
  `min_password_length` tinyint(4) NOT NULL DEFAULT '6',
  `installed_locales` varchar(255) NOT NULL DEFAULT 'en_US',
  `supported_locales` varchar(255) DEFAULT NULL,
  `original_style_file_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`redirect`, `primary_locale`, `min_password_length`, `installed_locales`, `supported_locales`, `original_style_file_name`) VALUES
(0, 'en_US', 6, 'en_US:id_ID:ar_IQ', 'en_US:id_ID:ar_IQ', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE `site_settings` (
  `setting_name` varchar(255) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`setting_name`, `locale`, `setting_value`, `setting_type`) VALUES
('contactEmail', 'en_US', 'admin@staipi.com', 'string'),
('contactName', 'en_US', 'Open Journal Systems', 'string'),
('showDescription', '', '1', 'bool'),
('showThumbnail', '', '1', 'bool'),
('showTitle', '', '1', 'bool'),
('themePluginPath', '', 'default', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `stage_assignments`
--

CREATE TABLE `stage_assignments` (
  `stage_assignment_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `date_assigned` datetime NOT NULL,
  `recommend_only` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stage_assignments`
--

INSERT INTO `stage_assignments` (`stage_assignment_id`, `submission_id`, `user_group_id`, `user_id`, `date_assigned`, `recommend_only`) VALUES
(1, 1, 14, 3, '2019-03-01 21:53:25', 0),
(2, 2, 14, 3, '2019-03-01 22:38:37', 0),
(9, 2, 3, 5, '2019-03-02 19:47:53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `static_page_id` bigint(20) NOT NULL,
  `path` varchar(255) NOT NULL,
  `context_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`static_page_id`, `path`, `context_id`) VALUES
(1, 'Focus', 1);

-- --------------------------------------------------------

--
-- Table structure for table `static_page_settings`
--

CREATE TABLE `static_page_settings` (
  `static_page_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` longtext,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `static_page_settings`
--

INSERT INTO `static_page_settings` (`static_page_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'content', '<p><span style=\"font-size: 18px; font-weight: bold;\">JOURNAL ENTRY SOC 1 Name Withheld </span><span style=\"font-size: 18px;\"><br></span><span style=\"font-size: 18px;\">July 14th<br>In Class on Monday we spoke of Intersectionality; forms of intersecting systems of oppression and discrimination. Things such as Age, disability, race, gender, and class are all examples of reasons to justify mistreatment of people with different backgrounds and even gender stratification. The focus of the day was based on the inequalities of gender.An examples that was given to us were the glass ceiling which represents how women are able to make it so far until they reach the invisible ceiling or barrier to which they can overturn the male in charge. Where no matter the qualifications and achievements still cannot move them past the ranks above other men in charge. Although it is not exclusive to women, most of the time it is used as an explanation for lack of women in positions of power, well as minority groups. We saw how the notion of gender roles are promoted by labeling colors to genders as well as toys i.e. the Barbie dolls. We began to see a documentary called Miss Representation . It focused mainly on what we talked about with gender inequality and how females are depicted in the mass media and through politics and really all across mainstream medias in the U.S. Views y women and men who see that females are objectified if they attempt to rise into positions of power. Leadership positions depict females aas emotional and unstable for decision making.</span></p>', 'string'),
(1, 'en_US', 'title', 'Focus and Scope', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `submissions`
--

CREATE TABLE `submissions` (
  `submission_id` bigint(20) NOT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `context_id` bigint(20) NOT NULL,
  `section_id` bigint(20) DEFAULT NULL,
  `language` varchar(10) DEFAULT 'en',
  `citations` text,
  `date_submitted` datetime DEFAULT NULL,
  `last_modified` datetime DEFAULT NULL,
  `date_status_modified` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `submission_progress` tinyint(4) NOT NULL DEFAULT '1',
  `pages` varchar(255) DEFAULT NULL,
  `hide_author` tinyint(4) NOT NULL DEFAULT '0',
  `stage_id` bigint(20) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submissions`
--

INSERT INTO `submissions` (`submission_id`, `locale`, `context_id`, `section_id`, `language`, `citations`, `date_submitted`, `last_modified`, `date_status_modified`, `status`, `submission_progress`, `pages`, `hide_author`, `stage_id`) VALUES
(1, 'en_US', 1, 1, '', NULL, '2019-03-01 22:13:11', '2019-03-01 22:13:11', '2019-03-01 22:13:11', 1, 0, NULL, 0, 1),
(2, 'en_US', 1, 1, '', NULL, '2019-03-01 22:45:04', '2019-03-02 20:02:18', '2019-03-02 20:02:18', 3, 0, '11', 0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `submission_artwork_files`
--

CREATE TABLE `submission_artwork_files` (
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL,
  `caption` text,
  `credit` varchar(255) DEFAULT NULL,
  `copyright_owner` varchar(255) DEFAULT NULL,
  `copyright_owner_contact` text,
  `permission_terms` text,
  `permission_file_id` bigint(20) DEFAULT NULL,
  `chapter_id` bigint(20) DEFAULT NULL,
  `contact_author` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_comments`
--

CREATE TABLE `submission_comments` (
  `comment_id` bigint(20) NOT NULL,
  `comment_type` bigint(20) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `assoc_id` bigint(20) NOT NULL,
  `author_id` bigint(20) NOT NULL,
  `comment_title` text,
  `comments` text,
  `date_posted` datetime DEFAULT NULL,
  `date_modified` datetime DEFAULT NULL,
  `viewable` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_files`
--

CREATE TABLE `submission_files` (
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL,
  `source_file_id` bigint(20) DEFAULT NULL,
  `source_revision` bigint(20) DEFAULT NULL,
  `submission_id` bigint(20) NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `genre_id` bigint(20) DEFAULT NULL,
  `file_size` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `file_stage` bigint(20) NOT NULL,
  `direct_sales_price` varchar(255) DEFAULT NULL,
  `sales_type` varchar(255) DEFAULT NULL,
  `viewable` tinyint(4) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `uploader_user_id` bigint(20) DEFAULT NULL,
  `assoc_type` bigint(20) DEFAULT NULL,
  `assoc_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_files`
--

INSERT INTO `submission_files` (`file_id`, `revision`, `source_file_id`, `source_revision`, `submission_id`, `file_type`, `genre_id`, `file_size`, `original_file_name`, `file_stage`, `direct_sales_price`, `sales_type`, `viewable`, `date_uploaded`, `date_modified`, `uploader_user_id`, `assoc_type`, `assoc_id`) VALUES
(2, 1, NULL, NULL, 2, 'application/pdf', 1, 198680, 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 2, NULL, NULL, 1, '2019-03-01 22:39:41', '2019-03-01 22:39:41', 3, NULL, NULL),
(3, 1, 2, 1, 2, 'application/pdf', 1, 198680, 'PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 4, NULL, NULL, 1, '2019-03-01 22:39:41', '2019-03-02 18:59:04', 3, NULL, NULL),
(4, 1, NULL, NULL, 2, 'application/pdf', NULL, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 5, NULL, NULL, 0, '2019-03-02 19:12:31', '2019-03-02 19:12:31', 4, 517, 1),
(5, 1, NULL, NULL, 2, 'application/pdf', 1, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 15, NULL, NULL, 0, '2019-03-02 19:50:46', '2019-03-02 19:50:46', 5, NULL, NULL),
(6, 1, 5, 1, 2, 'application/pdf', 1, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 6, NULL, NULL, 1, '2019-03-02 19:50:46', '2019-03-02 19:52:01', 5, NULL, NULL),
(7, 1, 6, 1, 2, 'application/pdf', 1, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 11, NULL, NULL, 0, '2019-03-02 19:50:46', '2019-03-02 19:54:40', 5, NULL, NULL),
(7, 2, NULL, NULL, 2, 'application/pdf', 1, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 11, NULL, NULL, 0, '2019-03-02 19:55:39', '2019-03-02 19:55:39', 5, 0, 0),
(8, 1, NULL, NULL, 2, 'application/pdf', 1, 198680, 'Revision-1-Article Text-1-1-2-20190301.pdf', 10, NULL, NULL, 0, '2019-03-02 19:57:22', '2019-03-02 19:57:22', 5, 521, 1);

-- --------------------------------------------------------

--
-- Table structure for table `submission_file_settings`
--

CREATE TABLE `submission_file_settings` (
  `file_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_file_settings`
--

INSERT INTO `submission_file_settings` (`file_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(2, 'en_US', 'name', 'author, author artikel proposal.pdf', 'string'),
(3, 'en_US', 'name', 'Article Text, PROPOSAL PROYEK AKHIR DIKI PAHRILAH rev.pdf', 'string'),
(4, 'en_US', 'name', 'Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(5, 'en_US', 'name', 'rev-Article Text, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(6, 'en_US', 'name', 'rev-Article Text, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(7, 'en_US', 'name', 'editor, Revision-1-Article Text-1-1-2-20190301.pdf', 'string'),
(8, 'en_US', 'name', 'editor, Revision-1-Article Text-1-1-2-20190301.pdf', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `submission_galleys`
--

CREATE TABLE `submission_galleys` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) DEFAULT NULL,
  `submission_id` bigint(20) NOT NULL,
  `label` varchar(255) DEFAULT NULL,
  `file_id` bigint(20) DEFAULT NULL,
  `seq` double NOT NULL DEFAULT '0',
  `remote_url` varchar(2047) DEFAULT NULL,
  `is_approved` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_galleys`
--

INSERT INTO `submission_galleys` (`galley_id`, `locale`, `submission_id`, `label`, `file_id`, `seq`, `remote_url`, `is_approved`) VALUES
(1, 'en_US', 2, 'PDF', 8, 0, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `submission_galley_settings`
--

CREATE TABLE `submission_galley_settings` (
  `galley_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_search_keyword_list`
--

CREATE TABLE `submission_search_keyword_list` (
  `keyword_id` bigint(20) NOT NULL,
  `keyword_text` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_search_keyword_list`
--

INSERT INTO `submission_search_keyword_list` (`keyword_id`, `keyword_text`) VALUES
(53, 'analysis'),
(46, 'analyzes'),
(39, 'application'),
(49, 'applications'),
(1, 'author'),
(35, 'authors'),
(33, 'based'),
(15, 'bridge'),
(50, 'built'),
(62, 'code'),
(67, 'continue'),
(38, 'count'),
(75, 'counting'),
(20, 'creation'),
(64, 'database'),
(21, 'day'),
(58, 'design'),
(47, 'designing'),
(36, 'developed'),
(25, 'development'),
(4, 'diaz'),
(22, 'dynamic'),
(31, 'effectively'),
(32, 'efficiently'),
(77, 'elections'),
(5, 'faizal'),
(89, 'form'),
(19, 'forward'),
(66, 'gammu'),
(42, 'gateway'),
(72, 'generates'),
(88, 'graphical'),
(78, 'grobogan'),
(28, 'helps'),
(12, 'history'),
(14, 'human'),
(29, 'humans'),
(68, 'implementation'),
(55, 'includes'),
(27, 'information'),
(48, 'infrastructure'),
(73, 'integrates'),
(61, 'interface'),
(79, 'involving'),
(11, 'justify'),
(8, 'language'),
(76, 'local'),
(69, 'maintenance'),
(6, 'malik'),
(65, 'manager'),
(52, 'method'),
(59, 'modeling'),
(23, 'motion'),
(18, 'moving'),
(45, 'nbsp'),
(16, 'perfection'),
(43, 'php'),
(85, 'processed'),
(86, 'produce'),
(7, 'programming'),
(37, 'quick'),
(34, 'reason'),
(24, 'reflected'),
(87, 'reports'),
(57, 'requirements'),
(70, 'result'),
(82, 'results'),
(84, 'samples'),
(13, 'science'),
(81, 'send'),
(41, 'sms'),
(44, 'sql'),
(54, 'stage'),
(3, 'staipi'),
(71, 'study'),
(9, 'style'),
(40, 'system'),
(26, 'technology'),
(63, 'testing'),
(10, 'text-align'),
(83, 'tps'),
(17, 'truth'),
(60, 'uml'),
(2, 'universitas'),
(80, 'volunteers'),
(74, 'vote'),
(51, 'waterfall'),
(56, 'weaknesses'),
(30, 'work');

-- --------------------------------------------------------

--
-- Table structure for table `submission_search_objects`
--

CREATE TABLE `submission_search_objects` (
  `object_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `type` int(11) NOT NULL,
  `assoc_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_search_objects`
--

INSERT INTO `submission_search_objects` (`object_id`, `submission_id`, `type`, `assoc_id`) VALUES
(1, 2, 1, 0),
(2, 2, 2, 0),
(3, 2, 4, 0),
(4, 2, 8, 0),
(5, 2, 16, 0),
(6, 2, 32, 0),
(7, 2, 64, 0);

-- --------------------------------------------------------

--
-- Table structure for table `submission_search_object_keywords`
--

CREATE TABLE `submission_search_object_keywords` (
  `object_id` bigint(20) NOT NULL,
  `keyword_id` bigint(20) NOT NULL,
  `pos` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_search_object_keywords`
--

INSERT INTO `submission_search_object_keywords` (`object_id`, `keyword_id`, `pos`) VALUES
(1, 1, 0),
(1, 1, 1),
(3, 1, 46),
(1, 2, 2),
(1, 3, 3),
(1, 4, 4),
(1, 5, 5),
(1, 6, 6),
(2, 7, 0),
(3, 7, 74),
(2, 8, 1),
(3, 8, 75),
(3, 9, 0),
(3, 9, 39),
(3, 9, 43),
(3, 9, 87),
(3, 9, 91),
(3, 10, 1),
(3, 10, 40),
(3, 10, 44),
(3, 10, 88),
(3, 10, 92),
(3, 11, 2),
(3, 11, 41),
(3, 11, 45),
(3, 11, 89),
(3, 11, 93),
(3, 12, 3),
(3, 13, 4),
(3, 14, 5),
(3, 14, 8),
(3, 14, 15),
(3, 15, 6),
(3, 15, 82),
(3, 16, 7),
(3, 17, 9),
(3, 18, 10),
(3, 19, 11),
(3, 20, 12),
(3, 21, 13),
(3, 21, 14),
(3, 22, 16),
(3, 23, 17),
(3, 24, 18),
(3, 25, 19),
(3, 26, 20),
(3, 27, 21),
(3, 28, 22),
(3, 29, 23),
(3, 30, 24),
(3, 31, 25),
(3, 32, 26),
(3, 33, 27),
(3, 34, 28),
(3, 35, 29),
(3, 36, 30),
(3, 37, 31),
(3, 37, 101),
(3, 37, 122),
(3, 38, 32),
(3, 38, 102),
(3, 38, 123),
(3, 38, 125),
(3, 39, 33),
(3, 39, 99),
(3, 40, 34),
(3, 40, 58),
(3, 40, 60),
(3, 40, 66),
(3, 40, 105),
(3, 41, 35),
(3, 41, 80),
(3, 41, 97),
(3, 41, 113),
(3, 42, 36),
(3, 42, 81),
(3, 42, 98),
(3, 43, 37),
(3, 43, 73),
(3, 44, 38),
(3, 44, 76),
(3, 45, 42),
(3, 45, 90),
(3, 46, 47),
(3, 47, 48),
(3, 48, 49),
(3, 49, 50),
(3, 49, 120),
(3, 50, 51),
(3, 51, 52),
(3, 52, 53),
(3, 53, 54),
(3, 53, 57),
(3, 53, 62),
(3, 54, 55),
(3, 54, 84),
(3, 55, 56),
(3, 55, 65),
(3, 56, 59),
(3, 57, 61),
(3, 58, 63),
(3, 58, 64),
(3, 58, 70),
(3, 59, 67),
(3, 60, 68),
(3, 61, 69),
(3, 62, 71),
(3, 63, 72),
(3, 64, 77),
(3, 65, 78),
(3, 66, 79),
(3, 67, 83),
(3, 68, 85),
(3, 69, 86),
(3, 70, 94),
(3, 71, 95),
(3, 72, 96),
(3, 73, 100),
(3, 74, 103),
(3, 74, 114),
(3, 74, 124),
(3, 75, 104),
(3, 75, 115),
(3, 76, 106),
(3, 77, 107),
(3, 77, 108),
(3, 78, 109),
(3, 79, 110),
(3, 80, 111),
(3, 81, 112),
(3, 82, 116),
(3, 83, 117),
(3, 84, 118),
(3, 85, 119),
(3, 86, 121),
(3, 87, 126),
(3, 88, 127),
(3, 89, 128);

-- --------------------------------------------------------

--
-- Table structure for table `submission_settings`
--

CREATE TABLE `submission_settings` (
  `submission_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `submission_settings`
--

INSERT INTO `submission_settings` (`submission_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'en_US', 'abstract', '<p style=\"text-align: justify;\">In history, science is one of the human bridge towards perfection. Human tends to truth always moving forward to be a better creation from day to day. One among of the human dynamic motion is reflected on the development of technology and information that helps humans work more effectively and efficiently. Based on this reason, the authors developed a Quick Count Application System With Sms Gateway Using Php And My Sql.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">The author analyzes and designing infrastructure applications built using the waterfall method, the analysis stage includes analysis of system weaknesses and system requirements analysis, design, or design that includes system modeling using UML and interface design, code and testing using PHP as the programming language and My SQL as well as a database manager for Gammu SMS gateway bridge, continue on the stage of implementation and maintenance.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">The result of this study generates SMS gateway application that integrates with the quick count vote counting system local elections (Elections) Grobogan involving volunteers to send SMS vote counting results of TPS samples to be processed applications produce quick count vote count reports in graphical form.</p>', 'string'),
(1, 'en_US', 'cleanTitle', 'PROGRAMMING LANGUAGE', 'string'),
(1, 'en_US', 'prefix', '', 'string'),
(1, 'en_US', 'subtitle', 'Proposal Proyek Akhir', 'string'),
(1, 'en_US', 'title', 'PROGRAMMING LANGUAGE', 'string'),
(2, '', 'copyrightYear', '2019', 'string'),
(2, '', 'licenseURL', 'http://creativecommons.org/licenses/by-nc-nd/4.0', 'string'),
(2, 'en_US', 'abstract', '<p style=\"text-align: justify;\">In history, science is one of the human bridge towards perfection. Human tends to truth always moving forward to be a better creation from day to day. One among of the human dynamic motion is reflected on the development of technology and information that helps humans work more effectively and efficiently. Based on this reason, the authors developed a Quick Count Application System With Sms Gateway Using Php And My Sql.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">The author analyzes and designing infrastructure applications built using the waterfall method, the analysis stage includes analysis of system weaknesses and system requirements analysis, design, or design that includes system modeling using UML and interface design, code and testing using PHP as the programming language and My SQL as well as a database manager for Gammu SMS gateway bridge, continue on the stage of implementation and maintenance.</p>\r\n<p style=\"text-align: justify;\">&nbsp;</p>\r\n<p style=\"text-align: justify;\">The result of this study generates SMS gateway application that integrates with the quick count vote counting system local elections (Elections) Grobogan involving volunteers to send SMS vote counting results of TPS samples to be processed applications produce quick count vote count reports in graphical form.</p>', 'string'),
(2, 'en_US', 'cleanTitle', 'PROGRAMMING LANGUAGE', 'string'),
(2, 'en_US', 'copyrightHolder', 'Tested', 'string'),
(2, 'en_US', 'prefix', '', 'string'),
(2, 'en_US', 'subtitle', 'Proposal Proyek Akhir', 'string'),
(2, 'en_US', 'title', 'PROGRAMMING LANGUAGE', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `submission_supplementary_files`
--

CREATE TABLE `submission_supplementary_files` (
  `file_id` bigint(20) NOT NULL,
  `revision` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `submission_tombstones`
--

CREATE TABLE `submission_tombstones` (
  `tombstone_id` bigint(20) NOT NULL,
  `submission_id` bigint(20) NOT NULL,
  `date_deleted` datetime NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `set_spec` varchar(255) NOT NULL,
  `set_name` varchar(255) NOT NULL,
  `oai_identifier` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `subscription_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `date_start` date DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `membership` varchar(40) DEFAULT NULL,
  `reference_number` varchar(40) DEFAULT NULL,
  `notes` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_types`
--

CREATE TABLE `subscription_types` (
  `type_id` bigint(20) NOT NULL,
  `journal_id` bigint(20) NOT NULL,
  `cost` double NOT NULL,
  `currency_code_alpha` varchar(3) NOT NULL,
  `non_expiring` tinyint(4) NOT NULL DEFAULT '0',
  `duration` smallint(6) DEFAULT NULL,
  `format` smallint(6) NOT NULL,
  `institutional` tinyint(4) NOT NULL DEFAULT '0',
  `membership` tinyint(4) NOT NULL DEFAULT '0',
  `disable_public_display` tinyint(4) NOT NULL,
  `seq` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subscription_type_settings`
--

CREATE TABLE `subscription_type_settings` (
  `type_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temporary_files`
--

CREATE TABLE `temporary_files` (
  `file_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `file_name` varchar(90) NOT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `file_size` bigint(20) NOT NULL,
  `original_file_name` varchar(127) DEFAULT NULL,
  `date_uploaded` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temporary_files`
--

INSERT INTO `temporary_files` (`file_id`, `user_id`, `file_name`, `file_type`, `file_size`, `original_file_name`, `date_uploaded`) VALUES
(1, 1, 'pngB3.tmp', 'image/png', 202407, 'the PROGRAMMING LANGUANGE.png', '2019-03-02 22:29:57'),
(2, 1, 'pngC84C.tmp', 'image/png', 202407, 'the PROGRAMMING LANGUANGE.png', '2019-03-02 22:30:48'),
(3, 1, 'zipBFDD.tmp', 'application/zip', 45519, 'quickSubmit.zip', '2019-03-02 22:58:04');

-- --------------------------------------------------------

--
-- Table structure for table `usage_stats_temporary_records`
--

CREATE TABLE `usage_stats_temporary_records` (
  `assoc_id` bigint(20) NOT NULL,
  `assoc_type` bigint(20) NOT NULL,
  `day` bigint(20) NOT NULL,
  `entry_time` bigint(20) NOT NULL,
  `metric` bigint(20) NOT NULL DEFAULT '1',
  `country_id` varchar(2) DEFAULT NULL,
  `region` varchar(2) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `load_id` varchar(255) NOT NULL,
  `file_type` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` bigint(20) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salutation` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) NOT NULL,
  `suffix` varchar(255) DEFAULT NULL,
  `initials` varchar(5) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `url` varchar(2047) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `mailing_address` varchar(255) DEFAULT NULL,
  `billing_address` varchar(255) DEFAULT NULL,
  `country` varchar(90) DEFAULT NULL,
  `locales` varchar(255) DEFAULT NULL,
  `gossip` text,
  `date_last_email` datetime DEFAULT NULL,
  `date_registered` datetime NOT NULL,
  `date_validated` datetime DEFAULT NULL,
  `date_last_login` datetime NOT NULL,
  `must_change_password` tinyint(4) DEFAULT NULL,
  `auth_id` bigint(20) DEFAULT NULL,
  `auth_str` varchar(255) DEFAULT NULL,
  `disabled` tinyint(4) NOT NULL DEFAULT '0',
  `disabled_reason` text,
  `inline_help` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `salutation`, `first_name`, `middle_name`, `last_name`, `suffix`, `initials`, `email`, `url`, `phone`, `mailing_address`, `billing_address`, `country`, `locales`, `gossip`, `date_last_email`, `date_registered`, `date_validated`, `date_last_login`, `must_change_password`, `auth_id`, `auth_str`, `disabled`, `disabled_reason`, `inline_help`) VALUES
(1, 'admin', '$2y$10$F6h6h5RkJclPWskvK1McXux6a4/dQqtTe64JX.0.xdLIGBDfiqvFW', NULL, 'admin', NULL, 'admin', NULL, 'aa', 'admin@staipi.com', '', '', '', NULL, 'ID', 'en_US', NULL, NULL, '2019-02-28 06:11:43', NULL, '2019-03-04 08:46:41', 0, NULL, NULL, 0, NULL, 1),
(2, 'fachreal', '$2y$10$bIiR1Hr89x3c/7KbOB/JBud4RycHsfvYloWzQey7iCKw6zV7zCUOG', '', 'Fachreal', '', 'Heart', '', 'FH', 'fachrealheart96@gmail.com', '', '', '', NULL, 'ID', 'id_ID:en_US:ar_IQ', '', NULL, '2019-02-28 08:35:15', NULL, '2019-03-04 05:20:35', 0, NULL, NULL, 0, NULL, 1),
(3, 'author', '$2y$10$E8N31l.vtHoQbGFwU3X8luiaAnfxQuH1cs2rMXx9vASq3EDg9TUse', NULL, 'Author', '', 'Author', NULL, 'AA', 'dikipahrilah29@gmail.com', NULL, NULL, NULL, NULL, 'ID', '', NULL, NULL, '2019-03-01 21:51:31', NULL, '2019-03-04 02:57:17', 0, NULL, NULL, 0, NULL, 1),
(4, 'reviewer', '$2y$10$uq9QEFSrOqtn7m9JdDTCVunmVyniUViZVEguonebsEP209KEbuoza', '', 'Reviewer', '', 'reviewer', '', 'Rr', 'reviewer@staipi.ac.id', '', '', '', NULL, 'ID', '', NULL, NULL, '2019-03-01 22:24:15', NULL, '2019-03-02 19:39:28', 0, NULL, NULL, 0, NULL, 1),
(5, 'editor', '$2y$10$c2t4xBO/pfJ6nmVGjPoKg.5Kqan3Rnax8rlipKVo8gmgTFJzazqVa', '', 'Editor', '', 'editor', '', 'Ee', 'editor@staipi.ac.id', '', '', '', NULL, 'ID', '', NULL, NULL, '2019-03-01 23:11:44', NULL, '2019-03-02 19:43:12', 0, NULL, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `user_group_id` bigint(20) NOT NULL,
  `context_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  `is_default` tinyint(4) NOT NULL DEFAULT '0',
  `show_title` tinyint(4) NOT NULL DEFAULT '1',
  `permit_self_registration` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`user_group_id`, `context_id`, `role_id`, `is_default`, `show_title`, `permit_self_registration`) VALUES
(1, 0, 1, 1, 0, 0),
(2, 1, 16, 1, 0, 0),
(3, 1, 16, 1, 0, 0),
(4, 1, 16, 1, 0, 0),
(5, 1, 17, 1, 0, 0),
(6, 1, 17, 1, 0, 0),
(7, 1, 4097, 1, 0, 0),
(8, 1, 4097, 1, 0, 0),
(9, 1, 4097, 1, 0, 0),
(10, 1, 4097, 1, 0, 0),
(11, 1, 4097, 1, 0, 0),
(12, 1, 4097, 1, 0, 0),
(13, 1, 4097, 1, 0, 0),
(14, 1, 65536, 1, 0, 1),
(15, 1, 65536, 1, 0, 0),
(16, 1, 4096, 1, 0, 1),
(17, 1, 1048576, 1, 0, 1),
(18, 1, 2097152, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_group_settings`
--

CREATE TABLE `user_group_settings` (
  `user_group_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group_settings`
--

INSERT INTO `user_group_settings` (`user_group_id`, `locale`, `setting_name`, `setting_value`, `setting_type`) VALUES
(1, 'ar_IQ', 'name', '##default.groups.name.siteAdmin##', 'string'),
(1, 'en_US', 'name', 'Site Admin', 'string'),
(1, 'id_ID', 'name', '##default.groups.name.siteAdmin##', 'string'),
(2, '', 'abbrevLocaleKey', 'default.groups.abbrev.manager', 'string'),
(2, '', 'nameLocaleKey', 'default.groups.name.manager', 'string'),
(2, 'ar_IQ', 'abbrev', 'ر ت', 'string'),
(2, 'ar_IQ', 'name', 'رئيس تحرير المجلة', 'string'),
(2, 'en_US', 'abbrev', 'JM', 'string'),
(2, 'en_US', 'name', 'Journal manager', 'string'),
(2, 'id_ID', 'abbrev', 'JM', 'string'),
(2, 'id_ID', 'name', 'Manajer Jurnal', 'string'),
(3, '', 'abbrevLocaleKey', 'default.groups.abbrev.editor', 'string'),
(3, '', 'nameLocaleKey', 'default.groups.name.editor', 'string'),
(3, 'ar_IQ', 'abbrev', 'م', 'string'),
(3, 'ar_IQ', 'name', 'محرر', 'string'),
(3, 'en_US', 'abbrev', 'JE', 'string'),
(3, 'en_US', 'name', 'Journal editor', 'string'),
(3, 'id_ID', 'abbrev', 'JE', 'string'),
(3, 'id_ID', 'name', 'Editor Jurnal', 'string'),
(4, '', 'abbrevLocaleKey', 'default.groups.abbrev.productionEditor', 'string'),
(4, '', 'nameLocaleKey', 'default.groups.name.productionEditor', 'string'),
(4, 'ar_IQ', 'abbrev', 'مح إنتاج', 'string'),
(4, 'ar_IQ', 'name', 'محرر الإنتاج', 'string'),
(4, 'en_US', 'abbrev', 'ProdE', 'string'),
(4, 'en_US', 'name', 'Production editor', 'string'),
(4, 'id_ID', 'abbrev', '##default.groups.abbrev.productionEditor##', 'string'),
(4, 'id_ID', 'name', '##default.groups.name.productionEditor##', 'string'),
(5, '', 'abbrevLocaleKey', 'default.groups.abbrev.sectionEditor', 'string'),
(5, '', 'nameLocaleKey', 'default.groups.name.sectionEditor', 'string'),
(5, 'ar_IQ', 'abbrev', 'م ق', 'string'),
(5, 'ar_IQ', 'name', 'محرر قسم', 'string'),
(5, 'en_US', 'abbrev', 'SecE', 'string'),
(5, 'en_US', 'name', 'Section editor', 'string'),
(5, 'id_ID', 'abbrev', 'SecE', 'string'),
(5, 'id_ID', 'name', 'Editor Bagian', 'string'),
(6, '', 'abbrevLocaleKey', 'default.groups.abbrev.guestEditor', 'string'),
(6, '', 'nameLocaleKey', 'default.groups.name.guestEditor', 'string'),
(6, 'ar_IQ', 'abbrev', 'م ض', 'string'),
(6, 'ar_IQ', 'name', 'محرر مستضاف', 'string'),
(6, 'en_US', 'abbrev', 'GE', 'string'),
(6, 'en_US', 'name', 'Guest editor', 'string'),
(6, 'id_ID', 'abbrev', 'GE', 'string'),
(6, 'id_ID', 'name', 'Editor Tamu', 'string'),
(7, '', 'abbrevLocaleKey', 'default.groups.abbrev.copyeditor', 'string'),
(7, '', 'nameLocaleKey', 'default.groups.name.copyeditor', 'string'),
(7, 'ar_IQ', 'abbrev', 'مدق', 'string'),
(7, 'ar_IQ', 'name', 'مدقق', 'string'),
(7, 'en_US', 'abbrev', 'CE', 'string'),
(7, 'en_US', 'name', 'Copyeditor', 'string'),
(7, 'id_ID', 'abbrev', '##default.groups.abbrev.copyeditor##', 'string'),
(7, 'id_ID', 'name', '##default.groups.name.copyeditor##', 'string'),
(8, '', 'abbrevLocaleKey', 'default.groups.abbrev.designer', 'string'),
(8, '', 'nameLocaleKey', 'default.groups.name.designer', 'string'),
(8, 'ar_IQ', 'abbrev', 'تصميم', 'string'),
(8, 'ar_IQ', 'name', 'مصمم', 'string'),
(8, 'en_US', 'abbrev', 'Design', 'string'),
(8, 'en_US', 'name', 'Designer', 'string'),
(8, 'id_ID', 'abbrev', '##default.groups.abbrev.designer##', 'string'),
(8, 'id_ID', 'name', '##default.groups.name.designer##', 'string'),
(9, '', 'abbrevLocaleKey', 'default.groups.abbrev.funding', 'string'),
(9, '', 'nameLocaleKey', 'default.groups.name.funding', 'string'),
(9, 'ar_IQ', 'abbrev', 'منسق تل', 'string'),
(9, 'ar_IQ', 'name', 'منسق تمويل', 'string'),
(9, 'en_US', 'abbrev', 'FC', 'string'),
(9, 'en_US', 'name', 'Funding coordinator', 'string'),
(9, 'id_ID', 'abbrev', '##default.groups.abbrev.funding##', 'string'),
(9, 'id_ID', 'name', '##default.groups.name.funding##', 'string'),
(10, '', 'abbrevLocaleKey', 'default.groups.abbrev.indexer', 'string'),
(10, '', 'nameLocaleKey', 'default.groups.name.indexer', 'string'),
(10, 'ar_IQ', 'abbrev', 'مفس', 'string'),
(10, 'ar_IQ', 'name', 'مفهرس', 'string'),
(10, 'en_US', 'abbrev', 'IND', 'string'),
(10, 'en_US', 'name', 'Indexer', 'string'),
(10, 'id_ID', 'abbrev', '##default.groups.abbrev.indexer##', 'string'),
(10, 'id_ID', 'name', '##default.groups.name.indexer##', 'string'),
(11, '', 'abbrevLocaleKey', 'default.groups.abbrev.layoutEditor', 'string'),
(11, '', 'nameLocaleKey', 'default.groups.name.layoutEditor', 'string'),
(11, 'ar_IQ', 'abbrev', 'مصم طع', 'string'),
(11, 'ar_IQ', 'name', 'مصمم طباعي', 'string'),
(11, 'en_US', 'abbrev', 'LE', 'string'),
(11, 'en_US', 'name', 'Layout Editor', 'string'),
(11, 'id_ID', 'abbrev', '##default.groups.abbrev.layoutEditor##', 'string'),
(11, 'id_ID', 'name', '##default.groups.name.layoutEditor##', 'string'),
(12, '', 'abbrevLocaleKey', 'default.groups.abbrev.marketing', 'string'),
(12, '', 'nameLocaleKey', 'default.groups.name.marketing', 'string'),
(12, 'ar_IQ', 'abbrev', 'منسق تس بع', 'string'),
(12, 'ar_IQ', 'name', 'منسق تسويق ومبيعات', 'string'),
(12, 'en_US', 'abbrev', 'MS', 'string'),
(12, 'en_US', 'name', 'Marketing and sales coordinator', 'string'),
(12, 'id_ID', 'abbrev', '##default.groups.abbrev.marketing##', 'string'),
(12, 'id_ID', 'name', '##default.groups.name.marketing##', 'string'),
(13, '', 'abbrevLocaleKey', 'default.groups.abbrev.proofreader', 'string'),
(13, '', 'nameLocaleKey', 'default.groups.name.proofreader', 'string'),
(13, 'ar_IQ', 'abbrev', 'مص قر', 'string'),
(13, 'ar_IQ', 'name', 'مصحح القراءة', 'string'),
(13, 'en_US', 'abbrev', 'PR', 'string'),
(13, 'en_US', 'name', 'Proofreader', 'string'),
(13, 'id_ID', 'abbrev', '##default.groups.abbrev.proofreader##', 'string'),
(13, 'id_ID', 'name', '##default.groups.name.proofreader##', 'string'),
(14, '', 'abbrevLocaleKey', 'default.groups.abbrev.author', 'string'),
(14, '', 'nameLocaleKey', 'default.groups.name.author', 'string'),
(14, 'ar_IQ', 'abbrev', 'مؤ', 'string'),
(14, 'ar_IQ', 'name', 'مؤلف', 'string'),
(14, 'en_US', 'abbrev', 'AU', 'string'),
(14, 'en_US', 'name', 'Author', 'string'),
(14, 'id_ID', 'abbrev', '##default.groups.abbrev.author##', 'string'),
(14, 'id_ID', 'name', '##default.groups.name.author##', 'string'),
(15, '', 'abbrevLocaleKey', 'default.groups.abbrev.translator', 'string'),
(15, '', 'nameLocaleKey', 'default.groups.name.translator', 'string'),
(15, 'ar_IQ', 'abbrev', 'متجم', 'string'),
(15, 'ar_IQ', 'name', 'مترجم', 'string'),
(15, 'en_US', 'abbrev', 'Trans', 'string'),
(15, 'en_US', 'name', 'Translator', 'string'),
(15, 'id_ID', 'abbrev', '##default.groups.abbrev.translator##', 'string'),
(15, 'id_ID', 'name', '##default.groups.name.translator##', 'string'),
(16, '', 'abbrevLocaleKey', 'default.groups.abbrev.externalReviewer', 'string'),
(16, '', 'nameLocaleKey', 'default.groups.name.externalReviewer', 'string'),
(16, 'ar_IQ', 'abbrev', 'ح', 'string'),
(16, 'ar_IQ', 'name', 'محكم', 'string'),
(16, 'en_US', 'abbrev', 'R', 'string'),
(16, 'en_US', 'name', 'Reviewer', 'string'),
(16, 'id_ID', 'abbrev', 'R', 'string'),
(16, 'id_ID', 'name', 'Reviewer', 'string'),
(17, '', 'abbrevLocaleKey', 'default.groups.abbrev.reader', 'string'),
(17, '', 'nameLocaleKey', 'default.groups.name.reader', 'string'),
(17, 'ar_IQ', 'abbrev', 'قرء', 'string'),
(17, 'ar_IQ', 'name', 'قارئ', 'string'),
(17, 'en_US', 'abbrev', 'Read', 'string'),
(17, 'en_US', 'name', 'Reader', 'string'),
(17, 'id_ID', 'abbrev', '##default.groups.abbrev.reader##', 'string'),
(17, 'id_ID', 'name', '##default.groups.name.reader##', 'string'),
(18, '', 'abbrevLocaleKey', 'default.groups.abbrev.subscriptionManager', 'string'),
(18, '', 'nameLocaleKey', 'default.groups.name.subscriptionManager', 'string'),
(18, 'ar_IQ', 'abbrev', 'م ش', 'string'),
(18, 'ar_IQ', 'name', 'مدير الاشتراكات', 'string'),
(18, 'en_US', 'abbrev', 'SubM', 'string'),
(18, 'en_US', 'name', 'Subscription Manager', 'string'),
(18, 'id_ID', 'abbrev', 'SubM', 'string'),
(18, 'id_ID', 'name', 'Manajer Langganan', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `user_group_stage`
--

CREATE TABLE `user_group_stage` (
  `context_id` bigint(20) NOT NULL,
  `user_group_id` bigint(20) NOT NULL,
  `stage_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_group_stage`
--

INSERT INTO `user_group_stage` (`context_id`, `user_group_id`, `stage_id`) VALUES
(1, 3, 1),
(1, 3, 3),
(1, 3, 4),
(1, 3, 5),
(1, 4, 4),
(1, 4, 5),
(1, 5, 1),
(1, 5, 3),
(1, 5, 4),
(1, 5, 5),
(1, 6, 1),
(1, 6, 3),
(1, 6, 4),
(1, 6, 5),
(1, 7, 4),
(1, 8, 5),
(1, 9, 1),
(1, 9, 3),
(1, 10, 5),
(1, 11, 5),
(1, 12, 4),
(1, 13, 5),
(1, 14, 1),
(1, 14, 3),
(1, 14, 4),
(1, 14, 5),
(1, 15, 1),
(1, 15, 3),
(1, 15, 4),
(1, 15, 5),
(1, 16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `user_interests`
--

CREATE TABLE `user_interests` (
  `user_id` bigint(20) NOT NULL,
  `controlled_vocab_entry_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_settings`
--

CREATE TABLE `user_settings` (
  `user_id` bigint(20) NOT NULL,
  `locale` varchar(14) NOT NULL DEFAULT '',
  `setting_name` varchar(255) NOT NULL,
  `assoc_type` bigint(20) DEFAULT '0',
  `assoc_id` bigint(20) DEFAULT '0',
  `setting_value` text,
  `setting_type` varchar(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_settings`
--

INSERT INTO `user_settings` (`user_id`, `locale`, `setting_name`, `assoc_type`, `assoc_id`, `setting_value`, `setting_type`) VALUES
(2, 'en_US', 'biography', 0, 0, '', 'string'),
(2, 'en_US', 'signature', 0, 0, '', 'string'),
(2, 'en_US', 'affiliation', 0, 0, '', 'string'),
(2, '', 'orcid', 0, 0, '', 'string'),
(1, 'en_US', 'signature', 0, 0, '', 'string'),
(1, 'en_US', 'affiliation', 0, 0, '', 'string'),
(1, '', 'profileImage', 256, 0, 'a:5:{s:4:\"name\";s:17:\"administrator.png\";s:10:\"uploadName\";s:18:\"profileImage-1.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:12:\"dateUploaded\";s:19:\"2019-03-01 21:29:37\";}', 'object'),
(1, 'en_US', 'biography', 0, 0, '<p>Hambatan tidak bisa menghentikan Anda. Masalah tidak bisa menghentikan Anda. Orang lain tidak bisa menghentikan Anda. Hanya Anda yang bisa menghentikan Anda. :)</p>', 'string'),
(1, '', 'orcid', 0, 0, '', 'string'),
(3, 'en_US', 'affiliation', 0, 0, 'Universitas STAIPI', 'string'),
(4, 'en_US', 'biography', 0, 0, '', 'string'),
(4, 'en_US', 'signature', 0, 0, '', 'string'),
(4, 'en_US', 'affiliation', 0, 0, '', 'string'),
(4, '', 'orcid', 0, 0, '', 'string'),
(5, 'en_US', 'biography', 0, 0, '', 'string'),
(5, 'en_US', 'signature', 0, 0, '', 'string'),
(5, 'en_US', 'affiliation', 0, 0, '', 'string'),
(5, '', 'orcid', 0, 0, '', 'string');

-- --------------------------------------------------------

--
-- Table structure for table `user_user_groups`
--

CREATE TABLE `user_user_groups` (
  `user_group_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_user_groups`
--

INSERT INTO `user_user_groups` (`user_group_id`, `user_id`) VALUES
(1, 1),
(2, 1),
(2, 2),
(3, 5),
(14, 3),
(16, 4),
(17, 3);

-- --------------------------------------------------------

--
-- Table structure for table `versions`
--

CREATE TABLE `versions` (
  `major` int(11) NOT NULL DEFAULT '0',
  `minor` int(11) NOT NULL DEFAULT '0',
  `revision` int(11) NOT NULL DEFAULT '0',
  `build` int(11) NOT NULL DEFAULT '0',
  `date_installed` datetime NOT NULL,
  `current` tinyint(4) NOT NULL DEFAULT '0',
  `product_type` varchar(30) DEFAULT NULL,
  `product` varchar(30) DEFAULT NULL,
  `product_class_name` varchar(80) DEFAULT NULL,
  `lazy_load` tinyint(4) NOT NULL DEFAULT '0',
  `sitewide` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `versions`
--

INSERT INTO `versions` (`major`, `minor`, `revision`, `build`, `date_installed`, `current`, `product_type`, `product`, `product_class_name`, `lazy_load`, `sitewide`) VALUES
(1, 0, 0, 0, '2019-02-28 06:11:55', 1, 'plugins.metadata', 'dc11', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.metadata', 'mods34', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.metadata', 'openurl10', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.blocks', 'information', 'InformationBlockPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.blocks', 'makeSubmission', 'MakeSubmissionBlockPlugin', 1, 0),
(1, 1, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.blocks', 'subscription', 'SubscriptionBlockPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.blocks', 'developedBy', 'DevelopedByBlockPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.blocks', 'languageToggle', 'LanguageToggleBlockPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.gateways', 'resolver', '', 0, 0),
(1, 2, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.generic', 'acron', 'AcronPlugin', 1, 1),
(0, 1, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.generic', 'citationStyleLanguage', 'CitationStyleLanguagePlugin', 1, 0),
(1, 2, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.generic', 'customBlockManager', 'CustomBlockManagerPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:56', 1, 'plugins.generic', 'driver', 'DRIVERPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'dublinCoreMeta', 'DublinCoreMetaPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'googleAnalytics', 'GoogleAnalyticsPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'googleScholar', 'GoogleScholarPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'htmlArticleGalley', 'HtmlArticleGalleyPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'lensGalley', 'LensGalleyPlugin', 1, 0),
(1, 2, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'openAIRE', 'OpenAIREPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'orcidProfile', 'OrcidProfilePlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'pdfJsViewer', 'PdfJsViewerPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'recommendByAuthor', 'RecommendByAuthorPlugin', 1, 1),
(1, 2, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'staticPages', 'StaticPagesPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'tinymce', 'TinyMCEPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'usageEvent', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'usageStats', '', 0, 1),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.generic', 'webFeed', 'WebFeedPlugin', 1, 0),
(2, 1, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'crossref', '', 0, 0),
(2, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'datacite', '', 0, 0),
(1, 1, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'doaj', '', 0, 0),
(2, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'medra', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'native', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'pubmed', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.importexport', 'users', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.oaiMetadataFormats', 'dc', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:57', 1, 'plugins.oaiMetadataFormats', 'marc', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.oaiMetadataFormats', 'marcxml', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.oaiMetadataFormats', 'rfc1807', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.paymethod', 'manual', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.paymethod', 'paypal', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.pubIds', 'doi', 'DOIPubIdPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.pubIds', 'urn', 'URNPubIdPlugin', 1, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.reports', 'articles', '', 0, 0),
(1, 1, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.reports', 'counterReport', '', 0, 0),
(2, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.reports', 'reviewReport', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.reports', 'views', '', 0, 0),
(1, 0, 0, 0, '2019-02-28 06:11:58', 1, 'plugins.themes', 'default', 'DefaultThemePlugin', 1, 0),
(3, 1, 1, 4, '2019-02-28 06:09:53', 1, 'core', 'ojs2', '', 0, 1),
(1, 1, 4, 0, '2019-02-28 09:16:39', 1, 'plugins.themes', 'bootstrap3', 'BootstrapThreeThemePlugin', 0, 0),
(1, 1, 0, 1, '2019-03-01 19:10:00', 1, 'plugins.themes', 'oldGregg', 'OldGreggThemePlugin', 0, 0),
(1, 0, 3, 0, '2019-03-01 19:26:34', 1, 'plugins.themes', 'healthSciences', 'HealthSciencesThemePlugin', 0, 0),
(1, 0, 0, 0, '2019-03-01 19:43:15', 1, 'plugins.themes', 'classic', 'ClassicThemePlugin', 0, 0),
(0, 1, 0, 0, '2019-03-01 19:43:16', 1, 'plugins.themes', 'criticalTimes', 'CriticalTimesThemePlugin', 0, 0),
(1, 0, 0, 0, '2019-03-01 19:43:16', 1, 'plugins.themes', 'default-child', 'DefaultChildThemePlugin', 0, 0),
(1, 0, 2, 0, '2019-03-01 19:43:16', 1, 'plugins.themes', 'defaultManuscript', 'DefaultManuscriptChildThemePlugin', 0, 0),
(1, 0, 0, 1, '2019-03-02 21:56:51', 1, 'plugins.blocks', 'keywordCloud', 'KeywordCloudBlockPlugin', 1, 0),
(1, 0, 0, 0, '2019-03-02 22:48:50', 1, 'plugins.themes', 'immersion', 'ImmersionThemePlugin', 0, 0),
(1, 0, 3, 0, '2019-03-02 23:03:25', 1, 'plugins.importexport', 'quickSubmit', '', 0, 0),
(2, 0, 0, 1, '2019-03-02 23:10:11', 1, 'plugins.generic', 'shariff', 'ShariffPlugin', 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_keys`
--
ALTER TABLE `access_keys`
  ADD PRIMARY KEY (`access_key_id`),
  ADD KEY `access_keys_hash` (`key_hash`,`user_id`,`context`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`announcement_id`),
  ADD KEY `announcements_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `announcement_settings`
--
ALTER TABLE `announcement_settings`
  ADD UNIQUE KEY `announcement_settings_pkey` (`announcement_id`,`locale`,`setting_name`),
  ADD KEY `announcement_settings_announcement_id` (`announcement_id`);

--
-- Indexes for table `announcement_types`
--
ALTER TABLE `announcement_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `announcement_types_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `announcement_type_settings`
--
ALTER TABLE `announcement_type_settings`
  ADD UNIQUE KEY `announcement_type_settings_pkey` (`type_id`,`locale`,`setting_name`),
  ADD KEY `announcement_type_settings_type_id` (`type_id`);

--
-- Indexes for table `authors`
--
ALTER TABLE `authors`
  ADD PRIMARY KEY (`author_id`),
  ADD KEY `authors_submission_id` (`submission_id`);

--
-- Indexes for table `author_settings`
--
ALTER TABLE `author_settings`
  ADD UNIQUE KEY `author_settings_pkey` (`author_id`,`locale`,`setting_name`),
  ADD KEY `author_settings_author_id` (`author_id`);

--
-- Indexes for table `auth_sources`
--
ALTER TABLE `auth_sources`
  ADD PRIMARY KEY (`auth_id`);

--
-- Indexes for table `citations`
--
ALTER TABLE `citations`
  ADD PRIMARY KEY (`citation_id`),
  ADD UNIQUE KEY `citations_submission_seq` (`submission_id`,`seq`),
  ADD KEY `citations_submission` (`submission_id`);

--
-- Indexes for table `citation_settings`
--
ALTER TABLE `citation_settings`
  ADD UNIQUE KEY `citation_settings_pkey` (`citation_id`,`locale`,`setting_name`),
  ADD KEY `citation_settings_citation_id` (`citation_id`);

--
-- Indexes for table `completed_payments`
--
ALTER TABLE `completed_payments`
  ADD PRIMARY KEY (`completed_payment_id`);

--
-- Indexes for table `controlled_vocabs`
--
ALTER TABLE `controlled_vocabs`
  ADD PRIMARY KEY (`controlled_vocab_id`),
  ADD UNIQUE KEY `controlled_vocab_symbolic` (`symbolic`,`assoc_type`,`assoc_id`);

--
-- Indexes for table `controlled_vocab_entries`
--
ALTER TABLE `controlled_vocab_entries`
  ADD PRIMARY KEY (`controlled_vocab_entry_id`),
  ADD KEY `controlled_vocab_entries_cv_id` (`controlled_vocab_id`,`seq`);

--
-- Indexes for table `controlled_vocab_entry_settings`
--
ALTER TABLE `controlled_vocab_entry_settings`
  ADD UNIQUE KEY `c_v_e_s_pkey` (`controlled_vocab_entry_id`,`locale`,`setting_name`),
  ADD KEY `c_v_e_s_entry_id` (`controlled_vocab_entry_id`);

--
-- Indexes for table `custom_issue_orders`
--
ALTER TABLE `custom_issue_orders`
  ADD UNIQUE KEY `custom_issue_orders_pkey` (`issue_id`);

--
-- Indexes for table `custom_section_orders`
--
ALTER TABLE `custom_section_orders`
  ADD UNIQUE KEY `custom_section_orders_pkey` (`issue_id`,`section_id`);

--
-- Indexes for table `data_object_tombstones`
--
ALTER TABLE `data_object_tombstones`
  ADD PRIMARY KEY (`tombstone_id`),
  ADD KEY `data_object_tombstones_data_object_id` (`data_object_id`);

--
-- Indexes for table `data_object_tombstone_oai_set_objects`
--
ALTER TABLE `data_object_tombstone_oai_set_objects`
  ADD PRIMARY KEY (`object_id`),
  ADD KEY `data_object_tombstone_oai_set_objects_tombstone_id` (`tombstone_id`);

--
-- Indexes for table `data_object_tombstone_settings`
--
ALTER TABLE `data_object_tombstone_settings`
  ADD UNIQUE KEY `data_object_tombstone_settings_pkey` (`tombstone_id`,`locale`,`setting_name`),
  ADD KEY `data_object_tombstone_settings_tombstone_id` (`tombstone_id`);

--
-- Indexes for table `edit_decisions`
--
ALTER TABLE `edit_decisions`
  ADD PRIMARY KEY (`edit_decision_id`),
  ADD KEY `edit_decisions_submission_id` (`submission_id`),
  ADD KEY `edit_decisions_editor_id` (`editor_id`);

--
-- Indexes for table `email_log`
--
ALTER TABLE `email_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `email_log_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `email_log_users`
--
ALTER TABLE `email_log_users`
  ADD UNIQUE KEY `email_log_user_id` (`email_log_id`,`user_id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`email_id`),
  ADD UNIQUE KEY `email_templates_email_key` (`email_key`,`assoc_type`,`assoc_id`),
  ADD KEY `email_templates_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `email_templates_data`
--
ALTER TABLE `email_templates_data`
  ADD UNIQUE KEY `email_templates_data_pkey` (`email_key`,`locale`,`assoc_type`,`assoc_id`);

--
-- Indexes for table `email_templates_default`
--
ALTER TABLE `email_templates_default`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `email_templates_default_email_key` (`email_key`);

--
-- Indexes for table `email_templates_default_data`
--
ALTER TABLE `email_templates_default_data`
  ADD UNIQUE KEY `email_templates_default_data_pkey` (`email_key`,`locale`);

--
-- Indexes for table `event_log`
--
ALTER TABLE `event_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `event_log_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `event_log_settings`
--
ALTER TABLE `event_log_settings`
  ADD UNIQUE KEY `event_log_settings_pkey` (`log_id`,`setting_name`),
  ADD KEY `event_log_settings_log_id` (`log_id`);

--
-- Indexes for table `filters`
--
ALTER TABLE `filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `filter_groups`
--
ALTER TABLE `filter_groups`
  ADD PRIMARY KEY (`filter_group_id`),
  ADD UNIQUE KEY `filter_groups_symbolic` (`symbolic`);

--
-- Indexes for table `filter_settings`
--
ALTER TABLE `filter_settings`
  ADD UNIQUE KEY `filter_settings_pkey` (`filter_id`,`locale`,`setting_name`),
  ADD KEY `filter_settings_id` (`filter_id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`genre_id`);

--
-- Indexes for table `genre_settings`
--
ALTER TABLE `genre_settings`
  ADD UNIQUE KEY `genre_settings_pkey` (`genre_id`,`locale`,`setting_name`),
  ADD KEY `genre_settings_genre_id` (`genre_id`);

--
-- Indexes for table `institutional_subscriptions`
--
ALTER TABLE `institutional_subscriptions`
  ADD PRIMARY KEY (`institutional_subscription_id`),
  ADD KEY `institutional_subscriptions_subscription_id` (`subscription_id`),
  ADD KEY `institutional_subscriptions_domain` (`domain`);

--
-- Indexes for table `institutional_subscription_ip`
--
ALTER TABLE `institutional_subscription_ip`
  ADD PRIMARY KEY (`institutional_subscription_ip_id`),
  ADD KEY `institutional_subscription_ip_subscription_id` (`subscription_id`),
  ADD KEY `institutional_subscription_ip_start` (`ip_start`),
  ADD KEY `institutional_subscription_ip_end` (`ip_end`);

--
-- Indexes for table `issues`
--
ALTER TABLE `issues`
  ADD PRIMARY KEY (`issue_id`),
  ADD KEY `issues_journal_id` (`journal_id`);

--
-- Indexes for table `issue_files`
--
ALTER TABLE `issue_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `issue_files_issue_id` (`issue_id`);

--
-- Indexes for table `issue_galleys`
--
ALTER TABLE `issue_galleys`
  ADD PRIMARY KEY (`galley_id`),
  ADD KEY `issue_galleys_issue_id` (`issue_id`);

--
-- Indexes for table `issue_galley_settings`
--
ALTER TABLE `issue_galley_settings`
  ADD UNIQUE KEY `issue_galley_settings_pkey` (`galley_id`,`locale`,`setting_name`),
  ADD KEY `issue_galley_settings_galley_id` (`galley_id`);

--
-- Indexes for table `issue_settings`
--
ALTER TABLE `issue_settings`
  ADD UNIQUE KEY `issue_settings_pkey` (`issue_id`,`locale`,`setting_name`),
  ADD KEY `issue_settings_issue_id` (`issue_id`),
  ADD KEY `issue_settings_name_value` (`setting_name`(50),`setting_value`(150));

--
-- Indexes for table `item_views`
--
ALTER TABLE `item_views`
  ADD UNIQUE KEY `item_views_pkey` (`assoc_type`,`assoc_id`,`user_id`);

--
-- Indexes for table `journals`
--
ALTER TABLE `journals`
  ADD PRIMARY KEY (`journal_id`),
  ADD UNIQUE KEY `journals_path` (`path`);

--
-- Indexes for table `journal_settings`
--
ALTER TABLE `journal_settings`
  ADD UNIQUE KEY `journal_settings_pkey` (`journal_id`,`locale`,`setting_name`),
  ADD KEY `journal_settings_journal_id` (`journal_id`);

--
-- Indexes for table `library_files`
--
ALTER TABLE `library_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `library_files_submission_id` (`submission_id`);

--
-- Indexes for table `library_file_settings`
--
ALTER TABLE `library_file_settings`
  ADD UNIQUE KEY `library_file_settings_pkey` (`file_id`,`locale`,`setting_name`),
  ADD KEY `library_file_settings_id` (`file_id`);

--
-- Indexes for table `metadata_descriptions`
--
ALTER TABLE `metadata_descriptions`
  ADD PRIMARY KEY (`metadata_description_id`),
  ADD KEY `metadata_descriptions_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `metadata_description_settings`
--
ALTER TABLE `metadata_description_settings`
  ADD UNIQUE KEY `metadata_descripton_settings_pkey` (`metadata_description_id`,`locale`,`setting_name`),
  ADD KEY `metadata_description_settings_id` (`metadata_description_id`);

--
-- Indexes for table `metrics`
--
ALTER TABLE `metrics`
  ADD KEY `metrics_load_id` (`load_id`),
  ADD KEY `metrics_metric_type_context_id` (`metric_type`,`context_id`),
  ADD KEY `metrics_metric_type_submission_id_assoc_type` (`metric_type`,`submission_id`,`assoc_type`),
  ADD KEY `metrics_metric_type_submission_id_assoc` (`metric_type`,`context_id`,`assoc_type`,`assoc_id`);

--
-- Indexes for table `navigation_menus`
--
ALTER TABLE `navigation_menus`
  ADD PRIMARY KEY (`navigation_menu_id`);

--
-- Indexes for table `navigation_menu_items`
--
ALTER TABLE `navigation_menu_items`
  ADD PRIMARY KEY (`navigation_menu_item_id`);

--
-- Indexes for table `navigation_menu_item_assignments`
--
ALTER TABLE `navigation_menu_item_assignments`
  ADD PRIMARY KEY (`navigation_menu_item_assignment_id`);

--
-- Indexes for table `navigation_menu_item_assignment_settings`
--
ALTER TABLE `navigation_menu_item_assignment_settings`
  ADD UNIQUE KEY `navigation_menu_item_assignment_settings_pkey` (`navigation_menu_item_assignment_id`,`locale`,`setting_name`),
  ADD KEY `assignment_settings_navigation_menu_item_assignment_id` (`navigation_menu_item_assignment_id`);

--
-- Indexes for table `navigation_menu_item_settings`
--
ALTER TABLE `navigation_menu_item_settings`
  ADD UNIQUE KEY `navigation_menu_item_settings_pkey` (`navigation_menu_item_id`,`locale`,`setting_name`),
  ADD KEY `navigation_menu_item_settings_navigation_menu_id` (`navigation_menu_item_id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`note_id`),
  ADD KEY `notes_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `notifications_context_id_user_id` (`context_id`,`user_id`,`level`),
  ADD KEY `notifications_context_id` (`context_id`,`level`),
  ADD KEY `notifications_assoc` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `notification_mail_list`
--
ALTER TABLE `notification_mail_list`
  ADD PRIMARY KEY (`notification_mail_list_id`),
  ADD UNIQUE KEY `notification_mail_list_email_context` (`email`,`context`);

--
-- Indexes for table `notification_settings`
--
ALTER TABLE `notification_settings`
  ADD UNIQUE KEY `notification_settings_pkey` (`notification_id`,`locale`,`setting_name`);

--
-- Indexes for table `notification_subscription_settings`
--
ALTER TABLE `notification_subscription_settings`
  ADD PRIMARY KEY (`setting_id`);

--
-- Indexes for table `oai_resumption_tokens`
--
ALTER TABLE `oai_resumption_tokens`
  ADD UNIQUE KEY `oai_resumption_tokens_pkey` (`token`);

--
-- Indexes for table `plugin_settings`
--
ALTER TABLE `plugin_settings`
  ADD UNIQUE KEY `plugin_settings_pkey` (`plugin_name`,`context_id`,`setting_name`),
  ADD KEY `plugin_settings_plugin_name` (`plugin_name`);

--
-- Indexes for table `published_submissions`
--
ALTER TABLE `published_submissions`
  ADD PRIMARY KEY (`published_submission_id`),
  ADD UNIQUE KEY `published_submissions_submission_id` (`submission_id`),
  ADD KEY `published_submissions_issue_id` (`issue_id`);

--
-- Indexes for table `queries`
--
ALTER TABLE `queries`
  ADD PRIMARY KEY (`query_id`),
  ADD KEY `queries_assoc_id` (`assoc_type`,`assoc_id`);

--
-- Indexes for table `query_participants`
--
ALTER TABLE `query_participants`
  ADD UNIQUE KEY `query_participants_pkey` (`query_id`,`user_id`);

--
-- Indexes for table `queued_payments`
--
ALTER TABLE `queued_payments`
  ADD PRIMARY KEY (`queued_payment_id`);

--
-- Indexes for table `review_assignments`
--
ALTER TABLE `review_assignments`
  ADD PRIMARY KEY (`review_id`),
  ADD KEY `review_assignments_submission_id` (`submission_id`),
  ADD KEY `review_assignments_reviewer_id` (`reviewer_id`),
  ADD KEY `review_assignments_form_id` (`review_form_id`),
  ADD KEY `review_assignments_reviewer_review` (`reviewer_id`,`review_id`);

--
-- Indexes for table `review_files`
--
ALTER TABLE `review_files`
  ADD UNIQUE KEY `review_files_pkey` (`review_id`,`file_id`),
  ADD KEY `review_files_review_id` (`review_id`);

--
-- Indexes for table `review_forms`
--
ALTER TABLE `review_forms`
  ADD PRIMARY KEY (`review_form_id`);

--
-- Indexes for table `review_form_elements`
--
ALTER TABLE `review_form_elements`
  ADD PRIMARY KEY (`review_form_element_id`),
  ADD KEY `review_form_elements_review_form_id` (`review_form_id`);

--
-- Indexes for table `review_form_element_settings`
--
ALTER TABLE `review_form_element_settings`
  ADD UNIQUE KEY `review_form_element_settings_pkey` (`review_form_element_id`,`locale`,`setting_name`),
  ADD KEY `review_form_element_settings_review_form_element_id` (`review_form_element_id`);

--
-- Indexes for table `review_form_responses`
--
ALTER TABLE `review_form_responses`
  ADD KEY `review_form_responses_pkey` (`review_form_element_id`,`review_id`);

--
-- Indexes for table `review_form_settings`
--
ALTER TABLE `review_form_settings`
  ADD UNIQUE KEY `review_form_settings_pkey` (`review_form_id`,`locale`,`setting_name`),
  ADD KEY `review_form_settings_review_form_id` (`review_form_id`);

--
-- Indexes for table `review_rounds`
--
ALTER TABLE `review_rounds`
  ADD PRIMARY KEY (`review_round_id`),
  ADD UNIQUE KEY `review_rounds_submission_id_stage_id_round_pkey` (`submission_id`,`stage_id`,`round`),
  ADD KEY `review_rounds_submission_id` (`submission_id`);

--
-- Indexes for table `review_round_files`
--
ALTER TABLE `review_round_files`
  ADD UNIQUE KEY `review_round_files_pkey` (`submission_id`,`review_round_id`,`file_id`,`revision`),
  ADD KEY `review_round_files_submission_id` (`submission_id`);

--
-- Indexes for table `rt_contexts`
--
ALTER TABLE `rt_contexts`
  ADD PRIMARY KEY (`context_id`),
  ADD KEY `rt_contexts_version_id` (`version_id`);

--
-- Indexes for table `rt_searches`
--
ALTER TABLE `rt_searches`
  ADD PRIMARY KEY (`search_id`),
  ADD KEY `rt_searches_context_id` (`context_id`);

--
-- Indexes for table `rt_versions`
--
ALTER TABLE `rt_versions`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `rt_versions_journal_id` (`journal_id`),
  ADD KEY `rt_versions_version_key` (`version_key`);

--
-- Indexes for table `scheduled_tasks`
--
ALTER TABLE `scheduled_tasks`
  ADD UNIQUE KEY `scheduled_tasks_pkey` (`class_name`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`),
  ADD KEY `sections_journal_id` (`journal_id`);

--
-- Indexes for table `section_editors`
--
ALTER TABLE `section_editors`
  ADD UNIQUE KEY `section_editors_pkey` (`context_id`,`section_id`,`user_id`),
  ADD KEY `section_editors_context_id` (`context_id`),
  ADD KEY `section_editors_section_id` (`section_id`),
  ADD KEY `section_editors_user_id` (`user_id`);

--
-- Indexes for table `section_settings`
--
ALTER TABLE `section_settings`
  ADD UNIQUE KEY `section_settings_pkey` (`section_id`,`locale`,`setting_name`),
  ADD KEY `section_settings_section_id` (`section_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD UNIQUE KEY `sessions_pkey` (`session_id`),
  ADD KEY `sessions_user_id` (`user_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD UNIQUE KEY `site_settings_pkey` (`setting_name`,`locale`);

--
-- Indexes for table `stage_assignments`
--
ALTER TABLE `stage_assignments`
  ADD PRIMARY KEY (`stage_assignment_id`),
  ADD UNIQUE KEY `stage_assignment` (`submission_id`,`user_group_id`,`user_id`),
  ADD KEY `stage_assignments_submission_id` (`submission_id`),
  ADD KEY `stage_assignments_user_group_id` (`user_group_id`),
  ADD KEY `stage_assignments_user_id` (`user_id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`static_page_id`);

--
-- Indexes for table `static_page_settings`
--
ALTER TABLE `static_page_settings`
  ADD UNIQUE KEY `static_page_settings_pkey` (`static_page_id`,`locale`,`setting_name`),
  ADD KEY `static_page_settings_static_page_id` (`static_page_id`);

--
-- Indexes for table `submissions`
--
ALTER TABLE `submissions`
  ADD PRIMARY KEY (`submission_id`),
  ADD KEY `submissions_section_id` (`section_id`);

--
-- Indexes for table `submission_artwork_files`
--
ALTER TABLE `submission_artwork_files`
  ADD PRIMARY KEY (`file_id`,`revision`);

--
-- Indexes for table `submission_comments`
--
ALTER TABLE `submission_comments`
  ADD PRIMARY KEY (`comment_id`),
  ADD KEY `submission_comments_submission_id` (`submission_id`);

--
-- Indexes for table `submission_files`
--
ALTER TABLE `submission_files`
  ADD PRIMARY KEY (`file_id`,`revision`),
  ADD KEY `submission_files_submission_id` (`submission_id`);

--
-- Indexes for table `submission_file_settings`
--
ALTER TABLE `submission_file_settings`
  ADD UNIQUE KEY `submission_file_settings_pkey` (`file_id`,`locale`,`setting_name`),
  ADD KEY `submission_file_settings_id` (`file_id`);

--
-- Indexes for table `submission_galleys`
--
ALTER TABLE `submission_galleys`
  ADD PRIMARY KEY (`galley_id`),
  ADD KEY `submission_galleys_submission_id` (`submission_id`);

--
-- Indexes for table `submission_galley_settings`
--
ALTER TABLE `submission_galley_settings`
  ADD UNIQUE KEY `submission_galley_settings_pkey` (`galley_id`,`locale`,`setting_name`),
  ADD KEY `submission_galley_settings_galley_id` (`galley_id`),
  ADD KEY `submission_galley_settings_name_value` (`setting_name`(50),`setting_value`(150));

--
-- Indexes for table `submission_search_keyword_list`
--
ALTER TABLE `submission_search_keyword_list`
  ADD PRIMARY KEY (`keyword_id`),
  ADD UNIQUE KEY `submission_search_keyword_text` (`keyword_text`);

--
-- Indexes for table `submission_search_objects`
--
ALTER TABLE `submission_search_objects`
  ADD PRIMARY KEY (`object_id`);

--
-- Indexes for table `submission_search_object_keywords`
--
ALTER TABLE `submission_search_object_keywords`
  ADD UNIQUE KEY `submission_search_object_keywords_pkey` (`object_id`,`pos`),
  ADD KEY `submission_search_object_keywords_keyword_id` (`keyword_id`);

--
-- Indexes for table `submission_settings`
--
ALTER TABLE `submission_settings`
  ADD UNIQUE KEY `submission_settings_pkey` (`submission_id`,`locale`,`setting_name`),
  ADD KEY `submission_settings_submission_id` (`submission_id`),
  ADD KEY `submission_settings_name_value` (`setting_name`(50),`setting_value`(150));

--
-- Indexes for table `submission_supplementary_files`
--
ALTER TABLE `submission_supplementary_files`
  ADD PRIMARY KEY (`file_id`,`revision`);

--
-- Indexes for table `submission_tombstones`
--
ALTER TABLE `submission_tombstones`
  ADD PRIMARY KEY (`tombstone_id`),
  ADD KEY `submission_tombstones_journal_id` (`journal_id`),
  ADD KEY `submission_tombstones_submission_id` (`submission_id`);

--
-- Indexes for table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`subscription_id`);

--
-- Indexes for table `subscription_types`
--
ALTER TABLE `subscription_types`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `subscription_type_settings`
--
ALTER TABLE `subscription_type_settings`
  ADD UNIQUE KEY `subscription_type_settings_pkey` (`type_id`,`locale`,`setting_name`),
  ADD KEY `subscription_type_settings_type_id` (`type_id`);

--
-- Indexes for table `temporary_files`
--
ALTER TABLE `temporary_files`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `temporary_files_user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_username` (`username`),
  ADD UNIQUE KEY `users_email` (`email`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`user_group_id`),
  ADD KEY `user_groups_user_group_id` (`user_group_id`),
  ADD KEY `user_groups_context_id` (`context_id`),
  ADD KEY `user_groups_role_id` (`role_id`);

--
-- Indexes for table `user_group_settings`
--
ALTER TABLE `user_group_settings`
  ADD UNIQUE KEY `user_group_settings_pkey` (`user_group_id`,`locale`,`setting_name`);

--
-- Indexes for table `user_group_stage`
--
ALTER TABLE `user_group_stage`
  ADD UNIQUE KEY `user_group_stage_pkey` (`context_id`,`user_group_id`,`stage_id`),
  ADD KEY `user_group_stage_context_id` (`context_id`),
  ADD KEY `user_group_stage_user_group_id` (`user_group_id`),
  ADD KEY `user_group_stage_stage_id` (`stage_id`);

--
-- Indexes for table `user_interests`
--
ALTER TABLE `user_interests`
  ADD UNIQUE KEY `u_e_pkey` (`user_id`,`controlled_vocab_entry_id`);

--
-- Indexes for table `user_settings`
--
ALTER TABLE `user_settings`
  ADD UNIQUE KEY `user_settings_pkey` (`user_id`,`locale`,`setting_name`,`assoc_type`,`assoc_id`),
  ADD KEY `user_settings_user_id` (`user_id`);

--
-- Indexes for table `user_user_groups`
--
ALTER TABLE `user_user_groups`
  ADD UNIQUE KEY `user_user_groups_pkey` (`user_group_id`,`user_id`),
  ADD KEY `user_user_groups_user_group_id` (`user_group_id`),
  ADD KEY `user_user_groups_user_id` (`user_id`);

--
-- Indexes for table `versions`
--
ALTER TABLE `versions`
  ADD UNIQUE KEY `versions_pkey` (`product_type`,`product`,`major`,`minor`,`revision`,`build`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_keys`
--
ALTER TABLE `access_keys`
  MODIFY `access_key_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `announcement_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `announcement_types`
--
ALTER TABLE `announcement_types`
  MODIFY `type_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `authors`
--
ALTER TABLE `authors`
  MODIFY `author_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `auth_sources`
--
ALTER TABLE `auth_sources`
  MODIFY `auth_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `citations`
--
ALTER TABLE `citations`
  MODIFY `citation_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `completed_payments`
--
ALTER TABLE `completed_payments`
  MODIFY `completed_payment_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `controlled_vocabs`
--
ALTER TABLE `controlled_vocabs`
  MODIFY `controlled_vocab_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `controlled_vocab_entries`
--
ALTER TABLE `controlled_vocab_entries`
  MODIFY `controlled_vocab_entry_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `data_object_tombstones`
--
ALTER TABLE `data_object_tombstones`
  MODIFY `tombstone_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_object_tombstone_oai_set_objects`
--
ALTER TABLE `data_object_tombstone_oai_set_objects`
  MODIFY `object_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `edit_decisions`
--
ALTER TABLE `edit_decisions`
  MODIFY `edit_decision_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `email_log`
--
ALTER TABLE `email_log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `email_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_templates_default`
--
ALTER TABLE `email_templates_default`
  MODIFY `email_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `event_log`
--
ALTER TABLE `event_log`
  MODIFY `log_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `filters`
--
ALTER TABLE `filters`
  MODIFY `filter_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `filter_groups`
--
ALTER TABLE `filter_groups`
  MODIFY `filter_group_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `genre_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `institutional_subscriptions`
--
ALTER TABLE `institutional_subscriptions`
  MODIFY `institutional_subscription_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `institutional_subscription_ip`
--
ALTER TABLE `institutional_subscription_ip`
  MODIFY `institutional_subscription_ip_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `issues`
--
ALTER TABLE `issues`
  MODIFY `issue_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `issue_files`
--
ALTER TABLE `issue_files`
  MODIFY `file_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `issue_galleys`
--
ALTER TABLE `issue_galleys`
  MODIFY `galley_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `journals`
--
ALTER TABLE `journals`
  MODIFY `journal_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `library_files`
--
ALTER TABLE `library_files`
  MODIFY `file_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `metadata_descriptions`
--
ALTER TABLE `metadata_descriptions`
  MODIFY `metadata_description_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `navigation_menus`
--
ALTER TABLE `navigation_menus`
  MODIFY `navigation_menu_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `navigation_menu_items`
--
ALTER TABLE `navigation_menu_items`
  MODIFY `navigation_menu_item_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `navigation_menu_item_assignments`
--
ALTER TABLE `navigation_menu_item_assignments`
  MODIFY `navigation_menu_item_assignment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `note_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT for table `notification_mail_list`
--
ALTER TABLE `notification_mail_list`
  MODIFY `notification_mail_list_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_subscription_settings`
--
ALTER TABLE `notification_subscription_settings`
  MODIFY `setting_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `published_submissions`
--
ALTER TABLE `published_submissions`
  MODIFY `published_submission_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `queries`
--
ALTER TABLE `queries`
  MODIFY `query_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `queued_payments`
--
ALTER TABLE `queued_payments`
  MODIFY `queued_payment_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review_assignments`
--
ALTER TABLE `review_assignments`
  MODIFY `review_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `review_forms`
--
ALTER TABLE `review_forms`
  MODIFY `review_form_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review_form_elements`
--
ALTER TABLE `review_form_elements`
  MODIFY `review_form_element_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `review_rounds`
--
ALTER TABLE `review_rounds`
  MODIFY `review_round_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `rt_contexts`
--
ALTER TABLE `rt_contexts`
  MODIFY `context_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rt_searches`
--
ALTER TABLE `rt_searches`
  MODIFY `search_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rt_versions`
--
ALTER TABLE `rt_versions`
  MODIFY `version_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stage_assignments`
--
ALTER TABLE `stage_assignments`
  MODIFY `stage_assignment_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `static_page_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `submissions`
--
ALTER TABLE `submissions`
  MODIFY `submission_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `submission_comments`
--
ALTER TABLE `submission_comments`
  MODIFY `comment_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `submission_files`
--
ALTER TABLE `submission_files`
  MODIFY `file_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `submission_galleys`
--
ALTER TABLE `submission_galleys`
  MODIFY `galley_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `submission_search_keyword_list`
--
ALTER TABLE `submission_search_keyword_list`
  MODIFY `keyword_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `submission_search_objects`
--
ALTER TABLE `submission_search_objects`
  MODIFY `object_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `submission_tombstones`
--
ALTER TABLE `submission_tombstones`
  MODIFY `tombstone_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `subscription_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscription_types`
--
ALTER TABLE `subscription_types`
  MODIFY `type_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temporary_files`
--
ALTER TABLE `temporary_files`
  MODIFY `file_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `user_group_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
