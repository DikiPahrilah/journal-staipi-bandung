<?php return array (
  'contactEmail' => 
  array (
    'en_US' => 'admin@staipi.com',
  ),
  'contactName' => 
  array (
    'en_US' => 'Open Journal Systems',
  ),
  'showDescription' => true,
  'showThumbnail' => true,
  'showTitle' => true,
  'themePluginPath' => 'default',
); ?>