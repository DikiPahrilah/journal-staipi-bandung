<?php return array (
  'enabled' => true,
  'selectedOrientation' => 'horizontal',
  'selectedPosition' => 'footer',
  'selectedServices' => 
  array (
    0 => 'twitter',
    1 => 'facebook',
    2 => 'googleplus',
    3 => 'linkedin',
    4 => 'pinterest',
    5 => 'xing',
    6 => 'whatsapp',
    7 => 'addthis',
    8 => 'tumblr',
    9 => 'flattr',
    10 => 'diaspora',
    11 => 'reddit',
    12 => 'stumbleupon',
    13 => 'threema',
    14 => 'weibo',
    15 => 'tencent-weibo',
    16 => 'qzone',
    17 => 'mail',
    18 => 'print',
    19 => 'info',
  ),
  'selectedTheme' => 'standard',
); ?>