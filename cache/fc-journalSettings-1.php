<?php return array (
  'agenciesEnabledSubmission' => false,
  'agenciesEnabledWorkflow' => true,
  'agenciesRequired' => false,
  'citationsEnabledSubmission' => false,
  'citationsEnabledWorkflow' => true,
  'citationsRequired' => false,
  'contactEmail' => 'fachrealheart96@gmail.com',
  'contactName' => 'FachrealHeart',
  'contactPhone' => '',
  'copyrightHolderType' => NULL,
  'copyrightNoticeAgree' => false,
  'copyrightYearBasis' => NULL,
  'copySubmissionAckAddress' => '',
  'copySubmissionAckPrimaryContact' => false,
  'coverageEnabledSubmission' => false,
  'coverageEnabledWorkflow' => true,
  'coverageRequired' => false,
  'currency' => 'IDR',
  'defaultReviewMode' => 2,
  'disciplinesEnabledSubmission' => false,
  'disciplinesEnabledWorkflow' => true,
  'disciplinesRequired' => false,
  'emailSignature' => '<p><br> ________________________________________________________________________<br> <a href="{$contextUrl}">{$contextName}</a></p>',
  'enableAnnouncements' => true,
  'enableAnnouncementsHomepage' => true,
  'enableAuthorSelfArchive' => false,
  'envelopeSender' => NULL,
  'itemsPerPage' => 25,
  'keywordsEnabledSubmission' => true,
  'keywordsEnabledWorkflow' => true,
  'keywordsRequired' => false,
  'languagesEnabledSubmission' => false,
  'languagesEnabledWorkflow' => true,
  'languagesRequired' => false,
  'licenseURL' => 'http://creativecommons.org/licenses/by-nc-nd/4.0',
  'mailingAddress' => 'Untuk informasi lebih lanjut, silahkan lihat peta lokasi atau bisa langsung menghubungi pihak terkait.',
  'membershipFee' => 0,
  'navItems' => NULL,
  'numAnnouncementsHomepage' => 2,
  'numDaysBeforeInviteReminder' => 0,
  'numDaysBeforeSubmitReminder' => 0,
  'numPageLinks' => 10,
  'numWeeksPerResponse' => 0,
  'numWeeksPerReview' => 4,
  'onlineIssn' => '',
  'paymentPluginName' => 'ManualPayment',
  'paymentsEnabled' => false,
  'printIssn' => '2339-1162',
  'publicationFee' => 0,
  'publisherInstitution' => 'STAIPI Persis Bandung',
  'purchaseArticleFee' => 0,
  'restrictReviewerFileAccess' => false,
  'reviewerAccessKeysEnabled' => false,
  'reviewerCompetingInterestsRequired' => false,
  'rightsEnabledSubmission' => false,
  'rightsEnabledWorkflow' => false,
  'rightsRequired' => false,
  'rtAbstract' => true,
  'rtAddComment' => true,
  'rtCaptureCite' => true,
  'rtDefineTerms' => true,
  'rtEmailAuthor' => true,
  'rtEmailOthers' => true,
  'rtPrinterFriendly' => true,
  'rtSupplementaryFiles' => true,
  'rtViewMetadata' => true,
  'showEnsuringLink' => false,
  'sourceEnabledSubmission' => false,
  'sourceEnabledWorkflow' => false,
  'sourceRequired' => false,
  'subjectsEnabledSubmission' => false,
  'subjectsEnabledWorkflow' => true,
  'subjectsRequired' => false,
  'supportedFormLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportedLocales' => 
  array (
    0 => 'en_US',
    1 => 'id_ID',
    2 => 'ar_IQ',
  ),
  'supportedSubmissionLocales' => 
  array (
    0 => 'en_US',
  ),
  'supportEmail' => 'admin@staipi.com',
  'supportName' => 'admin',
  'supportPhone' => '(022) 7563521',
  'themePluginPath' => 'bootstrap3',
  'typeEnabledSubmission' => false,
  'typeEnabledWorkflow' => false,
  'typeRequired' => false,
  'abbreviation' => 
  array (
    'en_US' => 'ts',
  ),
  'about' => 
  array (
    'en_US' => '<p>This journal discusses a conceptualization of existing studies in the field of education that use wordless picturebooks with young readers. The main aim of this study is to encourage a more interdisciplinary understanding of meaning-making and persuade educational researchers and mediators to consider investigative approaches that are not based on verbalization but are more in tune with the invitations that wordless picturebooks extend to young readers. The findings showed that wordless picturebooks allow readers have chances from the authority and weight of the words they must continually deal with both in school and elsewhere.</p>',
  ),
  'acronym' => 
  array (
    'en_US' => 'tested',
  ),
  'authorGuidelines' => 
  array (
    'en_US' => '<p>Detail guidline for submiting article can be found in <a href="https://drive.google.com/file/d/1xh4Sc_oh3NlhjRIaKlGFjBNX97K-1fx8/view" target="_blank" rel="noopener">Submission Kit</a>. It contains author guideline as well as template for article</p>',
  ),
  'authorInformation' => 
  array (
    'en_US' => 'Interested in submitting to this journal? We recommend that you review the <a href="http://localhost/journal/index.php/tested/about">About the Journal</a> page for the journal\'s section policies, as well as the <a href="http://localhost/journal/index.php/tested/about/submissions#authorGuidelines">Author Guidelines</a>. Authors need to <a href="http://localhost/journal/index.php/tested/user/register">register</a> with the journal prior to submitting or, if already registered, can simply <a href="http://localhost/journal/index.php/index/login">log in</a> and begin the five-step process.',
  ),
  'authorSelfArchivePolicy' => 
  array (
    'en_US' => '<p>This journal permits and encourages authors to post items submitted to the journal on personal websites or institutional repositories both prior to and after publication, while providing bibliographic details that credit, if applicable, its publication in this journal.</p>',
  ),
  'clockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the CLOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://clockss.org/">More...</a>',
  ),
  'contactAffiliation' => 
  array (
    'en_US' => 'STAIPI Persis bandung',
  ),
  'contactTitle' => 
  array (
    'en_US' => 'fachreal',
  ),
  'copyeditInstructions' => 
  array (
    'en_US' => 'The copyediting stage is intended to improve the flow, clarity, grammar, wording, and formatting of the article. It represents the last chance for the author to make any substantial changes to the text because the next stage is restricted to typos and formatting corrections.

The file to be copyedited is in Word or .rtf format and therefore can easily be edited as a word processing document. The set of instructions displayed here proposes two approaches to copyediting. One is based on Microsoft Word\'s Track Changes feature and requires that the copy editor, editor, and author have access to this program. A second system, which is software independent, has been borrowed, with permission, from the Harvard Educational Review. The journal editor is in a position to modify these instructions, so suggestions can be made to improve the process for this journal.


<h4>Copyediting Systems</h4>

<strong>1. Microsoft Word\'s Track Changes</strong>

Under Tools in the menu bar, the feature Track Changes enables the copy editor to make insertions (text appears in color) and deletions (text appears crossed out in color or in the margins as deleted). The copy editor can posit queries to both the author (Author Queries) and to the editor (Editor Queries) by inserting these queries in square brackets. The copyedited version is then uploaded, and the editor is notified. The editor then reviews the text and notifies the author.

The editor and author should leave those changes with which they are satisfied. If further changes are necessary, the editor and author can make changes to the initial insertions or deletions, as well as make new insertions or deletions elsewhere in the text. Authors and editors should respond to each of the queries addressed to them, with responses placed inside the square brackets.

After the text has been reviewed by editor and author, the copy editor will make a final pass over the text accepting the changes in preparation for the layout and galley stage.


<strong>2. Harvard Educational Review</strong>

<strong>Instructions for Making Electronic Revisions to the Manuscript</strong>

Please follow the following protocol for making electronic revisions to your manuscript:

<strong>Responding to suggested changes.</strong>
&nbsp; For each of the suggested changes that you accept, unbold the text.
&nbsp; For each of the suggested changes that you do not accept, re-enter the original text and <strong>bold</strong> it.

<strong>Making additions and deletions.</strong>
&nbsp; Indicate additions by <strong>bolding</strong> the new text.
&nbsp; Replace deleted sections with: <strong>[deleted text]</strong>.
&nbsp; If you delete one or more sentence, please indicate with a note, e.g., <strong>[deleted 2 sentences]</strong>.

<strong>Responding to Queries to the Author (QAs).</strong>
&nbsp; Keep all QAs intact and bolded within the text. Do not delete them.
&nbsp; To reply to a QA, add a comment after it. Comments should be delimited using:
<strong>[Comment:]</strong>
&nbsp; e.g., <strong>[Comment: Expanded discussion of methodology as you suggested]</strong>.

<strong>Making comments.</strong>
&nbsp; Use comments to explain organizational changes or major revisions
&nbsp; e.g., <strong>[Comment: Moved the above paragraph from p. 5 to p. 7].</strong>
&nbsp; Note: When referring to page numbers, please use the page numbers from the printed copy of the manuscript that was sent to you. This is important since page numbers may change as a document is revised electronically.

<h4>An Illustration of an Electronic Revision</h4>

<ol>
<li><strong>Initial copyedit.</strong> The journal copy editor will edit the text to improve flow, clarity, grammar, wording, and formatting, as well as including author queries as necessary. Once the initial edit is complete, the copy editor will upload the revised document through the journal Web site and notify the author that the edited manuscript is available for review.</li>
<li><strong>Author copyedit.</strong> Before making dramatic departures from the structure and organization of the edited manuscript, authors must check in with the editors who are co-chairing the piece. Authors should accept/reject any changes made during the initial copyediting, as appropriate, and respond to all author queries. When finished with the revisions, authors should rename the file from AuthorNameQA.doc to AuthorNameQAR.doc (e.g., from LeeQA.doc to LeeQAR.doc) and upload the revised document through the journal Web site as directed.</li>
<li><strong>Final copyedit.</strong> The journal copy editor will verify changes made by the author and incorporate the responses to the author queries to create a final manuscript. When finished, the copy editor will upload the final document through the journal Web site and alert the layout editor to complete formatting.</li>
</ol>',
  ),
  'copyrightNotice' => 
  array (
    'en_US' => '<p style="text-align: justify;">The Authors submitting a manuscript do so on the understanding that if accepted for publication, copyright of the article shall be assigned to journal Tested Faculty of Humanities, Universitas STAIPI as publisher of the journal, and the author&nbsp;also holds the copyright without restriction.</p>
<p style="text-align: justify;">Copyright encompasses exclusive rights to reproduce and deliver the article in all form and media, including reprints, photographs, microfilms and any other similar reproductions, as well as translations. The reproduction of any part of this journal, its storage in databases and its transmission by any form or media, such as electronic, electrostatic and mechanical copies, photocopies, recordings, magnetic media, etc. , are allowed with a written permission from journal Tested Faculty of Humanities, Universitas STAIPI.</p>
<p style="text-align: justify;">Jurnal Tested Board, Faculty of Humanities, Universitas STAIPI, the Editors and the Advisory International Editorial Board make every effort to ensure that no wrong or misleading data, opinions or statements be published in the journal. In any way, the contents of the articles and advertisements published in the journal Tested Faculty of Humanities, Universitas STAIPI are sole and exclusive responsibility of their respective authors and advertisers.</p>',
  ),
  'description' => 
  array (
    'en_US' => '<p>&nbsp;</p>
<p><img style="width: 100%; height: 100%;" src="http://stai-persis-bandung.ac.id/images/logo.png" alt="Staipi"></p>
<p style="text-align: justify;">This journal mostly discussed about the important of using narrative in the within language education. According to the researcher, narrative can help to address the field’s changing needs by further democratizing knowledge production and exchange, illuminating subtle yet vital dimensions of classroom interactions, and prompting imaginative interpretations and revisionists. His argument draws together relevant strands of applied linguistics research; narrative theories and research from education, sociology, and the arts. He concluded that incorporate narratives of classroom life, in light of some important dilemmas and cautions.</p>',
  ),
  'editorialTeam' => 
  array (
    'en_US' => '<div id="group">
<h4>Editorial in Chief</h4>
<div class="member">
<ul>
<li class="show"><a>Diki Pahrilah Fat Hamubin</a>, Student Of LPKIA Bandung, Indonesia</li>
</ul>
</div>
</div>
<div id="group">
<h4>Managing Editor</h4>
<div class="member">
<ul>
<li class="show"><a>Admin</a>, Administrator</li>
</ul>
</div>
</div>
<div id="group">
<h4>Editor Board</h4>
<div class="member">
<ul>
<li class="show"><a>Diki Pahrilah Fat Hamubin</a>, Student Of LPKIA Bandung, Indonesia</li>
</ul>
<ul>
<li class="show"><a>Diaz Faizal Malik</a>, Student Of LPKIA Bandung, Indonesia</li>
</ul>
<ul>
<li class="show"><a>Panji Dennis Giantoro</a>, Student Of LPKIA Bandung, Indonesia</li>
</ul>
</div>
</div>',
  ),
  'favicon' => 
  array (
    'en_US' => 
    array (
      'name' => 'logo9.png',
      'uploadName' => 'favicon_en_US.png',
      'width' => 254,
      'height' => 252,
      'dateUploaded' => '2019-02-28 08:20:19',
      'altText' => '',
    ),
  ),
  'journalThumbnail' => 
  array (
    'en_US' => 
    array (
      'name' => 'Cityscape Buildings Movie Poster2.png',
      'uploadName' => 'journalThumbnail_en_US.png',
      'width' => 526,
      'height' => 744,
      'dateUploaded' => '2019-02-28 09:44:41',
      'altText' => '',
    ),
  ),
  'librarianInformation' => 
  array (
    'en_US' => 'We encourage research librarians to list this journal among their library\'s electronic journal holdings. As well, it may be worth noting that this journal\'s open source publishing system is suitable for libraries to host for their faculty members to use with journals they are involved in editing (see <a href="http://pkp.sfu.ca/ojs">Open Journal Systems</a>).',
  ),
  'lockssLicense' => 
  array (
    'en_US' => 'This journal utilizes the LOCKSS system to create a distributed archiving system among participating libraries and permits those libraries to create permanent archives of the journal for purposes of preservation and restoration. <a href="http://www.lockss.org/">More...</a>',
  ),
  'metaCitations' => 
  array (
    'en_US' => '1',
  ),
  'name' => 
  array (
    'en_US' => 'Tested',
  ),
  'openAccessPolicy' => 
  array (
    'en_US' => 'This journal provides immediate open access to its content on the principle that making research freely available to the public supports a greater global exchange of knowledge.',
  ),
  'pageFooter' => 
  array (
    'en_US' => '<p><strong>Jurnal Tested ter Index :</strong></p>
<p><img src="/journal/public/site/images/admin/google-scholar-logo-png-5.png" width="119" height="34"></p>',
  ),
  'pageHeaderLogoImage' => 
  array (
    'en_US' => 
    array (
      'name' => 'logo9.png',
      'uploadName' => 'pageHeaderLogoImage_en_US.png',
      'width' => 254,
      'height' => 252,
      'dateUploaded' => '2019-02-28 07:55:29',
      'altText' => 'STAIPI',
    ),
  ),
  'privacyStatement' => 
  array (
    'en_US' => '<p>The names and email addresses entered in this journal site will be used exclusively for the stated purposes of this journal and will not be made available for any other purpose or to any other party.</p>',
  ),
  'proofInstructions' => 
  array (
    'en_US' => '<p>The proofreading stage is intended to catch any errors in the galley\'s spelling, grammar, and formatting. More substantial changes cannot be made at this stage, unless discussed with the Section Editor. In Layout, click on VIEW PROOF to see the HTML, PDF, and other available file formats used in publishing this item.</p>
	<h4>For Spelling and Grammar Errors</h4>

	<p>Copy the problem word or groups of words and paste them into the Proofreading Corrections box with "CHANGE-TO" instructions to the editor as follows:</p>

	<pre>1. CHANGE...
	then the others
	TO...
	than the others</pre>
	<br />
	<pre>2. CHANGE...
	Malinowsky
	TO...
	Malinowski</pre>
	<br />

	<h4>For Formatting Errors</h4>

	<p>Describe the location and nature of the problem in the Proofreading Corrections box after typing in the title "FORMATTING" as follows:</p>
	<br />
	<pre>3. FORMATTING
	The numbers in Table 3 are not aligned in the third column.</pre>
	<br />
	<pre>4. FORMATTING
	The paragraph that begins "This last topic..." is not indented.</pre>',
  ),
  'readerInformation' => 
  array (
    'en_US' => 'We encourage readers to sign up for the publishing notification service for this journal. Use the <a href="http://localhost/journal/index.php/tested/user/register">Register</a> link at the top of the home page for the journal. This registration will result in the reader receiving the Table of Contents by email for each new issue of the journal. This list also allows the journal to claim a certain level of support or readership. See the journal\'s <a href="http://localhost/journal/index.php/tested/about/submissions#privacyStatement">Privacy Statement</a>, which assures readers that their name and email address will not be used for other purposes.',
  ),
  'refLinkInstructions' => 
  array (
    'en_US' => '<h4>To Add Reference Linking to the Layout Process</h4>
	<p>When turning a submission into HTML or PDF, make sure that all hyperlinks in the submission are active.</p>
	<h4>A. When the Author Provides a Link with the Reference</h4>
	<ol>
	<li>While the submission is still in its word processing format (e.g., Word), add the phrase VIEW ITEM to the end of the reference that has a URL.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>B. Enabling Readers to Search Google Scholar For References</h4>
	<ol>
		<li>While the submission is still in its word processing format (e.g., Word), copy the title of the work referenced in the References list (if it appears to be too common a title—e.g., "Peace"—then copy author and title).</li>
		<li>Paste the reference\'s title between the %22\'s, placing a + between each word: http://scholar.google.com/scholar?q=%22PASTE+TITLE+HERE%22&hl=en&lr=&btnG=Search.</li>

	<li>Add the phrase GS SEARCH to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting it and using Word\'s Insert Hyperlink tool and the URL prepared in #2.</li>
	</ol>
	<h4>C. Enabling Readers to Search for References with a DOI</h4>
	<ol>
	<li>While the submission is still in Word, copy a batch of references into CrossRef Text Query http://www.crossref.org/freeTextQuery/.</li>
	<li>Paste each DOI that the Query provides in the following URL (between = and &): http://www.cmaj.ca/cgi/external_ref?access_num=PASTE DOI#HERE&link_type=DOI.</li>
	<li>Add the phrase CrossRef to the end of each citation in the submission\'s References list.</li>
	<li>Turn that phrase into a hyperlink by highlighting the phrase and using Word\'s Insert Hyperlink tool and the appropriate URL prepared in #2.</li>
	</ol>',
  ),
  'reviewGuidelines' => 
  array (
    'en_US' => '<h1>Peer Review Process</h1>
<p style="text-align: justify;">The research article submitted to this online journal will be peer-reviewed. The accepted research articles will be available online (free download) following the journal peer-reviewing process. The language used in this Journal is English. Detail information about reviewing, download <a href="http://arbitrer.fib.unand.ac.id/index.php/arbitrer/pages/view/review#">Reviewer Kit</a>.</p>
<p style="text-align: justify;">Reviewing a manuscript written by a fellow scientist is a privilege. However, it is a time-consuming responsibility. Hence, Arbitrer Editorial Board, authors, and audiences appreciate your willingness to accept this responsibility and your dedication. Arbitreradheres to a double-blind peer-review process that is rapid and fair, and also ensures a high quality of articles published. In so doing, Arbiter needs reviewers who can provide insightful and helpful comments on submitted manuscripts with a turn around time of about 4 weeks. Maintaining Arbitrer as a scientific journal of high quality depends on reviewers with a high level of expertise and an ability to be objective, fair, and insightful in their evaluation of manuscripts.</p>
<p style="text-align: justify;">If Arbitrer Editor-in-Chief has invited you to review a manuscript, please consider the following:</p>
<ol>
<li style="text-align: justify;">Reviewing manuscript critically, but constructively and preparing detailed comments about the manuscript to help authors improve their work</li>
<li style="text-align: justify;">Reviewing multiple versions of a manuscript as necessary</li>
<li style="text-align: justify;">Providing all required information within established deadlines</li>
<li style="text-align: justify;">Making recommendations to the editor regarding the suitability of the manuscript for publication in the journal</li>
<li style="text-align: justify;">Declaring to the editor any potential conflicts of interest with respect to the authors or the content of a manuscript they are asked to review</li>
<li style="text-align: justify;">Reporting possible research misconducts</li>
<li style="text-align: justify;">Suggesting alternative reviewers in case they cannot review the manuscript for any reasons</li>
<li style="text-align: justify;">Treating the manuscript as a confidential document</li>
<li style="text-align: justify;">Not making any use of the work described in the manuscript</li>
<li style="text-align: justify;">Not communicating directly with authors, if somehow they identify the authors</li>
<li style="text-align: justify;">Not identifying themselves to authors</li>
<li style="text-align: justify;">Not passing on the assigned manuscript to another reviewer</li>
<li style="text-align: justify;">Ensuring that the manuscript is of high quality and original work</li>
<li style="text-align: justify;">Informing the editor if he/she finds the assigned manuscript is under consideration in any other publication to his/her knowledge</li>
<li style="text-align: justify;">Writing review report in English only</li>
<li style="text-align: justify;">Authoring a commentary for publication related to the reviewed manuscript.</li>
</ol>
<p style="text-align: justify;">Here list of items that need to be reviewed:</p>
<ol>
<li><strong>Novelty of the topic</strong></li>
<li><strong>Originality</strong></li>
<li><strong>Scientific reliability</strong></li>
<li><strong>Valuable contribution to the science</strong></li>
<li><strong>Adding new aspects to the existed field of study</strong></li>
<li><strong>Ethical aspects</strong></li>
<li><strong>Structure of the article submitted and its relevance to authors’ guidelines</strong></li>
<li><strong>References provided to substantiate the content</strong></li>
<li><strong>Grammar, punctuation, and spelling</strong></li>
<li style="text-align: justify;"><strong>Scientific misconduct</strong></li>
</ol>',
  ),
  'searchDescription' => 
  array (
    'en_US' => 'Tested Journal',
  ),
  'submissionChecklist' => 
  array (
    'en_US' => 
    array (
      0 => 
      array (
        'content' => 'The submission has not been previously published, nor is it before another journal for consideration (or an explanation has been provided in Comments to the Editor).',
        'order' => '1',
      ),
      1 => 
      array (
        'content' => 'The submission file is in OpenOffice, Microsoft Word, or RTF document file format.',
        'order' => '2',
      ),
      2 => 
      array (
        'content' => 'Where available, URLs for the references have been provided.',
        'order' => '3',
      ),
      3 => 
      array (
        'content' => 'The text is single-spaced; uses a 12-point font; employs italics, rather than underlining (except with URL addresses); and all illustrations, figures, and tables are placed within the text at the appropriate points, rather than at the end.',
        'order' => '4',
      ),
      4 => 
      array (
        'content' => 'The text adheres to the stylistic and bibliographic requirements outlined in the Author Guidelines.',
        'order' => '5',
      ),
    ),
  ),
); ?>