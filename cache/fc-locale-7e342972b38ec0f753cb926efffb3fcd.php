<?php return array (
  'plugins.themes.immersion.name' => 'Immersion Theme',
  'plugins.themes.immersion.description' => 'The Theme For OJS 3',
  'plugins.themes.immersion.colorPick' => 'Color Picker',
  'plugins.themes.immersion.colorPickInstructions' => 'Pick a color for a section to display on the journal\'s front-end. Must be valid HTML hex code (e.g., #ffff00 for yellow).',
  'plugins.themes.immersion.section.coverPage' => 'Section Cover Image',
  'plugins.themes.immersion.annoucements.colorPick' => 'Announcements section colour on the journal\'s home page',
  'editor.issues.sectionArea' => 'Options for issue\'s sections',
  'plugins.themes.immersion.issue.published' => 'Published',
  'plugins.themes.immersion.issue.description' => 'Issue description',
  'plugins.themes.immersion.registration.consent' => 'Consent',
  'plugins.themes.immersion.mainMenu' => 'Main menu',
  'plugins.themes.immersion.language.toggle' => 'Change the language. The current language is:',
  'plugins.themes.immersion.adminMenu' => 'Admin menu',
  'plugins.themes.immersion.issue.fullIssueLink' => 'See the full issue',
  'plugins.themes.immersion.search.parameters' => 'Parameters',
  'plugins.themes.immersion.article.biography' => 'Bio',
  'plugins.themes.immersion.article.figure' => 'Cover image',
  'plugins.themes.immersion.authors.browse' => 'Browse by Author',
  'plugins.themes.immersion.author.details' => 'Author Details',
); ?>